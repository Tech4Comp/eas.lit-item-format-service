#!/bin/bash

COMPOSE_FILE=docker-compose-staging.yml

DIR=$(dirname $0)
cd $DIR

IMAGEID=$(docker-compose -p eal -f $COMPOSE_FILE images -q)
docker-compose -p eal -f $COMPOSE_FILE ps
docker-compose -p eal -f $COMPOSE_FILE pull
docker-compose -p eal -f $COMPOSE_FILE up -d --force-recreate

docker rmi $IMAGEID
