# EAs.LiT Format-Service #

This service provides a REST-API for item conversion between different file formats and can (optionally) request items from the [item-service](https://gitlab.com/Tech4Comp/eas.lit-item-api-service) via their id.

## How to get started
1. Clone the repository via `git clone ...`.
2. Change into the directory and execute the service via `gradle bootRun --info`. It will automatically download all needed dependencies. The started service is accessible via [http://localhost:8080](http://localhost:8080)

Have a look at the gradle files and gradle documentation for more commands.

For usable environment variables, see the file [docker-compose-staging.yml](./docker-compose-staging.yml).

As soon as the service is started, an OpenAPI documentation page (swagger) is created at `/documentation` that may be used to test and experiment with this service.

## Production Environment
Use the included dockerfile to build a docker image and the included docker-compose.yml files to start it up.

Look at the dockerfile and docker-compose.yml files to gain insights into manual deployment.

## Automatic Builds and Deployments
By including `[build]` to a commit message on the main branch, a GitLab CI pipeline is triggered and builds a docker image, which is published on GitLab. Afterwards, a deployment stage is triggered, which deploys this newly built image to the staging environment. Deployment may also be triggered on its own, by including `[deploy]` to a commit message.

## Development
The directory `src/` contains all source code, as well as most configuration files.

## Important Links

* API Documentation at localhost:
[Swagger UI at https://localhost:8080/documentation](https://localhost:8080/documentation)
* API Documentation at the tech4comp instance: [Swagger UI at https://import.easlit.erzw.uni-leipzig.de/documentation](https://import.easlit.erzw.uni-leipzig.de/documentation)
* Excel Plugin at the tech4comp instance: [Excel Plugin at http://import.easlit.erzw.uni-leipzig.de/excel/manifest.xml](http://import.easlit.erzw.uni-leipzig.de/excel/manifest.xml)
* Description of the JSON-Format: [JSON Format](src/main/java/eal/service/format/json/Json_FormatDescription.md)
