package eal.item;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import eal.item.format.InputFormat;
import eal.item.format.OutputFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController(value="KJKL")
//@EnableWebMvc
@Api(description = "REST API for import and export items")
public class Controller {
	
	@ApiIgnore
	@GetMapping(value = "/documentation")
    public String handleDocumentationRequest (HttpServletResponse httpResponse) throws IOException {
		httpResponse.sendRedirect("/swagger-ui.html");
        return null;
    }

	@PostMapping("/convert")
	@ResponseBody
    @ApiOperation(value = "Converts list of items from one format into another.")
	public void convertItemsPOST(
			@ApiParam(required = true, value = "Format of input data", allowableValues = "csv, csv_zip, xlsx, xlsx_zip, json, json_zip, csvjson, ilias, onyx")
			@RequestParam("inputFormat") 
			String inputFormat,
			
			@ApiParam(required = true, value = "Format of output data", allowableValues = "csv, csv_easlit, csv_zip, xlsx_all, xlsx_type, xlsx_all_easlit, xlsx_type_easlit, xlsx_all_zip, xlsx_type_zip, json, json_easlit, json_zip, json_id, csvjson_all, csvjson_type, csvjson_all_easlit, csvjson_type_easlit, ilias, onyx, moodle")
			@RequestParam("outputFormat") 
			String outputFormat,

			@ApiParam(value = "File that contains the items (e.g., from Ilias); this or fileData must be present")
			@RequestParam(value="file", required=false) 
			MultipartFile file,
			
			@ApiParam(value = "String that contains the items (e.g., copy+pasted from JSON file); this or file must be present")
			@RequestParam(value="fileData", required=false) 
			String fileData,
			
			HttpServletResponse response) throws Exception {

		
		InputFormat formatIn;
		OutputFormat formatOut;
		InputStream in;
		
		if ((formatIn = getInputFormat(inputFormat, response)) == null) return;
		if ((formatOut = getOutputFormat(outputFormat, response)) == null) return;
		if ((in = getInputStream(file, fileData, response)) == null) return;
		
		Service.convertItems(formatIn, in, formatOut, response);
	}




	
	@PostMapping("/import")
	@ResponseBody
    @ApiOperation(value = "Adds list of items to item store; returns list of stored items (incl. ID if stored successfully). ")
	public void importItemsPOST(
			@ApiParam(required = true, value = "Format of input data", allowableValues = "csv, csv_zip, xlsx, xlsx_zip, json, json_zip, csvjson, ilias, onyx")
			@RequestParam("inputFormat") 
			String inputFormat,
			
			@ApiParam(value = "File that contains the items (e.g., from Ilias); this or fileData must be present")
			@RequestParam(value="file", required=false) 
			MultipartFile file,
			
			@ApiParam(value = "String that contains the items (e.g., copy+pasted from JSON file); this or file must be present")
			@RequestParam(value="fileData", required=false) 
			String fileData,

			@ApiParam(value = "Project Id")
			@RequestParam(value="project", required=false) 
			String project,

			HttpServletResponse response) throws IOException {

		
		InputFormat format;
		InputStream in;
		
		if ((format = getInputFormat(inputFormat, response)) == null) return;
		if ((in = getInputStream(file, fileData, response)) == null) return;

		Service.importItems (format, in, true, project, response);
	}

	

	
	
	@PutMapping("/import")
	@ResponseBody
    @ApiOperation(value = "Updates list of items to item store (a new item is added if it does not have an ID; otherwise the item is updated); returns list of stored items (incl. ID if stored successfully). ")
	public void importItemsPUT(
			@ApiParam(required = true, value = "Format of input data", allowableValues = "csv, csv_zip, xlsx, xlsx_zip, json, json_zip, csvjson, ilias, onyx")
			@RequestParam("inputFormat") 
			String inputFormat,
			
			@ApiParam(value = "File that contains the items (e.g., from Ilias); this or fileData must be present")
			@RequestParam(value="file", required=false) 
			MultipartFile file,
			
			@ApiParam(value = "String that contains the items (e.g., copy+pasted from JSON file); this or file must be present")
			@RequestParam(value="fileData", required=false) 
			String fileData,

			@ApiParam(value = "Project Id")
			@RequestParam(value="project", required=false) 
			String project,
			
			HttpServletResponse response) throws IOException {

		
		InputFormat format;
		InputStream in;
		
		if ((format = getInputFormat(inputFormat, response)) == null) return;
		if ((in = getInputStream(file, fileData, response)) == null) return;

		Service.importItems (format, in, false, project, response);		
		
	}


	/**
	 * { "items_found" : [ "id1", "id2", ...], "items_not_found" : ["id6", "id7", ...] } 
	 * 
	 * 
	 * 
	 * @param idRange
	 * @param outputFormat
	 * @param response
	 * @throws IOException
	 * @throws Exception
	 */
	
	@GetMapping("/export")
	@ResponseBody
    @ApiOperation(value = "Exports a list of items (defined by their ids) into output format.")
	public void exportItemsGET(
			@ApiParam(value = "Item id or comma-separated list of item ids, e.g., '1,23,19'")
			@RequestParam(value="idList", required = false) 
			String idList,

			@ApiParam(value = "Project Id")
			@RequestParam(value="project", required = false) 
			String project,

			@ApiParam(required = true, value = "Format of output data",	allowableValues = "csv, csv_easlit, csv_zip, xlsx_all, xlsx_type, xlsx_all_easlit, xlsx_type_easlit, xlsx_all_zip, xlsx_type_zip, json, json_easlit, json_zip, csvjson_all, csvjson_type, csvjson_all_easlit, csvjson_type_easlit, ilias, onyx, moodle")
			@RequestParam("outputFormat") 
			String outputFormat,
	
			HttpServletResponse response) throws Exception {

		String[] itemIds = getItemIds (idList);
		OutputFormat format;
		
		if ((itemIds.length==0) && ((project == null) || (project.trim().length()==0))) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "No item ids or project given");
			return;
		}

		if ((format = getOutputFormat(outputFormat, response)) == null) return;

		Service.exportItems (itemIds, project, format, response);
	}	
	
	
	
	
	

	private InputFormat getInputFormat (String inputFormat, HttpServletResponse response) throws IOException {
		try {
			return InputFormat.valueOf(inputFormat);
		} catch (Exception e) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, String.format("Unknown inputFormat %s", inputFormat));
			return null;
		}			
	}
	
	private OutputFormat getOutputFormat (String outputFormat, HttpServletResponse response) throws IOException {
		try {
			return OutputFormat.valueOf(outputFormat);
		} catch (Exception e) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, String.format("Unknown outputFormat %s", outputFormat));
			return null;
		}			
	}	
	
	private InputStream getInputStream (MultipartFile file, String fileData, HttpServletResponse response) throws IOException {
		
		if ((file==null) && (fileData==null)) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "No data. Both file and fileData is null.");
			return null;
		}
		
		try {
			return (file!=null) ? file.getInputStream() : new ByteArrayInputStream(fileData.getBytes(StandardCharsets.UTF_8));
		} catch (Exception e) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, String.format("Can not read data from %s.", (file!=null) ? "file" : "fileData"));
			return null;
		}		
	}
	
	private String[] getItemIds (String idList) {
		
		return Arrays.stream(((idList==null)?"":idList).split(","))
			.map(String::trim)
			.filter(it -> !it.isEmpty())
			.distinct()
			.toArray(String[]::new);
		

	}

}

