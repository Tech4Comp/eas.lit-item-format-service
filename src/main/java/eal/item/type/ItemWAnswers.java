package eal.item.type;

import java.util.ArrayList;
import java.util.List;


public abstract class ItemWAnswers extends Item {

	private class Answer {
		private String text;
		private float[] points;	// [0=negative; 1=positive]

		private Answer(String text) {
			this.text = text;
			this.points = new float[] {0, 0};
		}
	}

	private List<Answer> answers = new ArrayList<Answer>();
	
	
	private boolean addAnswer (Object text) {
		
		if (text==null) return false;
		
		
		if (text instanceof String) {
			String sText = ((String)text).trim(); 
			if (sText.length()>0) {
				this.answers.add(this.new Answer(sText));
				return true;
			}
		}
		
		if (text instanceof Number) {
			Number nText = (Number)text;
			this.answers.add (this.new Answer (nText.toString()));
			return true;
		}
		
		return false;
	}
	
	public boolean addAnswer (Object text, Object pointsPositive, Object pointsNegative) {
		
		if (this.addAnswer(text)) {
			this.setAnswerPoints(this.getNumberOfAnswers()-1, true, pointsPositive);
			this.setAnswerPoints(this.getNumberOfAnswers()-1, false, pointsNegative);
			return true;
		}
		
		return false;
	}
	
	public String getAnswerText(int index) {
		return this.answers.get(index).text;
	}
	
	public float getAnswerPoints(int index, boolean positive) {
		return this.answers.get(index).points[positive?1:0];
	}

	private void setAnswerPoints (int index, boolean positive, float points) {
		this.answers.get(index).points[positive?1:0] = points;
	}
	
	private void setAnswerPoints (int index, boolean positive, Object points) {
		
		if (points == null) {
			this.setAnswerPoints(index, positive, 0);
			return;
		}
		
		if (points instanceof Number) {
			this.setAnswerPoints(index, positive, ((Number)points).floatValue());
			return;
		}
		
		if (points instanceof String) {
			try {
				this.setAnswerPoints(index, positive, Float.valueOf((String)points).floatValue());
				return;
			} catch (NumberFormatException e) {
				 this.setAnswerPoints(index, positive, 0);
				 return;
			}
		}
		
		 this.setAnswerPoints(index, positive, 0);
	}
	
	public int getNumberOfAnswers () {
		return this.answers.size();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		
		if (!super.equals(obj)) return false;
		
		if (!(obj instanceof ItemWAnswers)) return false;
		ItemWAnswers other = (ItemWAnswers) obj;
		
		if (this.getNumberOfAnswers() != other.getNumberOfAnswers()) return false;
		for (int index=0; index<this.getNumberOfAnswers(); index++) {
			if (!this.getAnswerText(index).equals(other.getAnswerText(index))) return false;
			if (this.getAnswerPoints(index, true) != other.getAnswerPoints(index, true)) return false;
			if (this.getAnswerPoints(index, false) != other.getAnswerPoints(index, false)) return false;
		}
		
		return true;
		
	}
	
	@Override
	public int hashCode() {
		return 0;
	}
	
}
