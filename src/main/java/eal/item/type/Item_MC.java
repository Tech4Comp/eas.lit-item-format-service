package eal.item.type;

import eal.item.format.json.Json_Item_MC;

public class Item_MC extends ItemWAnswers {

	public final static String type = "MC";

	private int minNumber = 0;
	private int maxNumber = 0;
	
	@Override
	public String getType() {
		return Item_MC.type;
	}
	
	
	@Override
	public float getPoints() {
		float res = 0;
		for (int index=0; index<this.getNumberOfAnswers(); index++) {
			res += Math.max(this.getAnswerPoints(index, true), this.getAnswerPoints(index, false));
		}
		return res;
	}
	
	
	public int getMinNumber() {
		// must be at least 1
		return Math.max(1, minNumber);
	}

	public int getMaxNumber() {
		// if unspecified (== 0) --> return default (=number of answers)
		return (maxNumber > 0) ? maxNumber : this.getNumberOfAnswers();
	}
	
	public void setMinMaxNumber (Object min, Object max) {
		this.minNumber = getNumber(min);
		this.maxNumber = getNumber(max);
	}
	
	
	private int getNumber(Object val) {

		if (val == null) return 0;
		
		if (val instanceof Number) return ((Number)val).intValue();
		
		if (val instanceof String) {
			try {
				return Double.valueOf((String)val).intValue();
			} catch (NumberFormatException e) {
				return 0;
			}
		}
		
		return 0;
	}




	
	
	@Override
	public boolean equals(Object obj) {
		
		if (!super.equals(obj)) return false;
		
		if (!(obj instanceof Item_MC)) return false;
		Item_MC other = (Item_MC) obj;
		
//		if (this.getMinNumber() != other.getMinNumber()) return false;
		if (this.getMaxNumber() != other.getMaxNumber()) return false;
		
		return true;
		
	}
	
	@Override
	public int hashCode() {
		return 0;
	}
	
	
	@Override
	public String toString() {
		return Json_Item_MC.toJSON(this, imgIdx -> ""+imgIdx  ).toString(4);
	}
	
}
