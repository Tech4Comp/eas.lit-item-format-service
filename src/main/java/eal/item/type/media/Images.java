package eal.item.type.media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.IntFunction;
import java.util.function.UnaryOperator;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import eal.item.store.HTTPHelper;
import eal.item.store.HttpResult;


/**
 * Type
 * - URI_EXTERNAL references external resource
 * - URI_INTERNAL URI references Easlit Image Store
 * - CONTENT content data (either from file or from base64)
 *
 * Output:
 * - URI original: keep URI (if available); store Content at EASLIT Image Store and get URI
 * - URI easlit: keep URI if EASLIT-URI;
 *
 */


public class Images {

	private static String IMAGE_STORE_URI = "https://file.easlit.erzw.uni-leipzig.de/image";
	private static String FILENAME = "filename";

	public IntFunction<String> IMAGES_URI_ORIGINAL = index -> getURI(index, false);
	public IntFunction<String> IMAGES_URI_EASLIT = index -> getURI(index, true);
	public IntFunction<String> IMAGES_BASE64 = index -> new String(org.apache.commons.codec.binary.Base64.encodeBase64(getImage(index)));


	private class Image {

		private String uri;
		private byte[] content;

		private Image(String uri) {
			super();
			this.uri = uri;
			this.content = null;
		}

		private Image(byte[] content) {
			super();
			this.uri = null;
			this.content = content;
		}


		private byte[] getContent ()  {
			return (this.content = Optional.ofNullable(this.content).orElseGet(() -> HTTPHelper.getURIAsByteArray(uri, 3)));
		}


		private String getURI (boolean forceEaslitURL)  {

			if (this.uri != null) {
				if ((!forceEaslitURL) || (this.uri.startsWith(IMAGE_STORE_URI))) return this.uri;
			}

			JSONObject json = HTTPHelper.postPutAsByteArray(IMAGE_STORE_URI, getContent(), 3);

		    String filename = Optional.ofNullable(json.optJSONObject(HttpResult.JSONOBJECT)).orElse(new JSONObject()).optString(FILENAME, null);
		    if (filename != null) {
		    	String res = IMAGE_STORE_URI + "/" + filename;
//		    	System.out.println(String.format("Images.getURI: %s", res));
		    	return res;
		    } else {
//		    	System.out.println(String.format("Images.getURI: ERROR %s", json.toString()));
		    	return IMAGE_STORE_URI + "/<none>";
		    }
		}
	}

	private List<Image> images;



	public String convertImagesToLabel (String html, IntFunction<String> getLabel) {

		if (html == null) return null;

		org.jsoup.nodes.Document doc = Jsoup.parseBodyFragment(html);


		// TODO: doc.select("img").stream()

		for (org.jsoup.nodes.Element img : doc.select("img")) {

			String src = img.attr("src");
			if (src == null) continue;
			if (src.length() == 0) continue;

			try {
				img.attr("src", getLabel.apply(Integer.parseInt(src)));
			} catch (NumberFormatException e) { }
		}

		return doc.body().html();

	}


	public String parseImages (String html, Map<String, byte[]> content) {

		return convertLabelToImages(html, src -> {

			// add new image from BASE64 code
			if ((src.length()>5) && (src.substring(0, 5).equalsIgnoreCase("data:"))) {
//				System.out.println("Replacing base64data ");
				String[] parts = src.split(",", 2);
				byte[] data = null;
				data = Base64.decodeBase64(parts[1].getBytes());
				images.add(new Image(data));
				return String.valueOf((this.getNumberOfImages()-1));
			}

			// look for reference in content
			if (content.containsKey(src)) {
				images.add(new Image(content.get(src)));
				return String.valueOf((this.getNumberOfImages()-1));
			}

			// consider src as URI
			images.add(new Image(src));
			return String.valueOf((this.getNumberOfImages()-1));
		});

	}






	public static String convertLabelToImages (String html, UnaryOperator<String> getImageIndex) {

		if (html == null) return null;

		org.jsoup.nodes.Document doc = Jsoup.parseBodyFragment(html);

		for (org.jsoup.nodes.Element img: doc.select("img")) {

			String src = img.attr("src");
			if (src == null) continue;
			if (src.length() == 0) continue;

			// number == internal reference
			try {
				Integer.parseInt(src);
				continue;
			} catch (NumberFormatException e) {	}

			img.attr("src", getImageIndex.apply(src));
		}

		return doc.body().html();
	}


//	public void addImagesToZipFIle (ZipOutputStream zip, IntFunction<String> getLabel) throws IOException {
//		for (int index=0; index<images.size(); index++) {
//			ZipHelper.addFileToZip(zip, getLabel.apply(index), new ByteArrayInputStream (images.get(index).content));
//		}
//	}


	public Images() {
		images = new ArrayList<Image>();
	}


	public int getNumberOfImages () {
		return this.images.size();
	}

	public void addImage(byte[] content) {
		this.images.add(new Image(content));
	}


	public byte[] getImage(int index) {
		return this.images.get(index).getContent();
	}


	public String getURI(int index, boolean forceEaslitURL) {
		return this.images.get(index).getURI(forceEaslitURL);
	}


	@Override
	public boolean equals(Object obj) {

		if (obj == null) return false;
		if (!(obj instanceof Images)) return false;

		Images other = (Images)obj;

		if (this.getNumberOfImages() != other.getNumberOfImages()) return false;
		for (int index=0; index<this.getNumberOfImages(); index++) {

			// both have the same URI --> ok
			if ((this.images.get(index).uri != null) && (other.images.get(index).uri != null)) {
				if (this.images.get(index).uri.equals(other.images.get(index).uri)) continue;
			}

			// check for content --> might require downloading
			if (!(Arrays.equals(this.getImage(index), other.getImage(index)))) return false;
		}

		return true;
	}


}
