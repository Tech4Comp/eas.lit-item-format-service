package eal.item.type;

public class Item_ParseException extends Item {

	private final String parseExceptionMessage; 
	
	public Item_ParseException(String parseExceptionMessage) {
		this.parseExceptionMessage = parseExceptionMessage;
	}
	
	
	public String getParseExceptionMessage() {
		return parseExceptionMessage;
	}


	@Override
	public float getPoints() {
		return 0;
	}

	@Override
	public String getType() {
		return null;
	}
	
	@Override
	public String toString() {
		return String.format("Item_ParseException: %s", this.parseExceptionMessage);
	}

}
