package eal.item.type;

import eal.item.format.json.Json_Item_FT;

public class Item_FT extends Item {

	public final static String type = "FT";

	private float points = 0;


	@Override
	public String getType() {
		return Item_FT.type;
	}
	
	@Override
	public float getPoints() {
		return this.points;
	}


	public void setPoints(float points) {
		this.points = points;
	}

	public void setPoints(Object points) {
		
		if (points == null) return;
		
		if (points instanceof Number) {
			this.setPoints(((Number)points).floatValue());
		}
		
		if (points instanceof String) {
			try {
				this.setPoints(Float.valueOf((String)points).floatValue());
			} catch (NumberFormatException e) { }
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (!super.equals(obj)) return false;
		
		if (!(obj instanceof Item_FT)) return false;
		Item_FT other = (Item_FT) obj;

		return (this.points == other.points);
	}
	
	@Override
	public int hashCode() {
		return 0;
	}
	
	
	@Override
	public String toString() {
		return Json_Item_FT.toJSON(this, imgIdx -> ""+imgIdx  ).toString(4);
	}
	
}
