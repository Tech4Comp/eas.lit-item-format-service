package eal.item.type;

import eal.item.format.json.Json_Item_SC;

public class Item_SC extends ItemWAnswers {

	public final static String type = "SC";


	@Override
	public String getType() {
		return Item_SC.type;
	}
	
	
	@Override
	public float getPoints() {

		float res = 0;
		for (int index=0; index<this.getNumberOfAnswers(); index++) {
			res = Math.max(res, this.getAnswerPoints(index));
		}
		return res;
	}
	
	
	public float getAnswerPoints(int index) {
		return this.getAnswerPoints(index, true);
	}
	
	public boolean addAnswer (Object text, Object pointsPositive) {
		return this.addAnswer(text, pointsPositive, null);
	}

	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) return false;
		if (!(obj instanceof Item_SC)) return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		return 0;
	}
	
	@Override
	public String toString() {
		return Json_Item_SC.toJSON(this, imgIdx -> ""+imgIdx  ).toString(4);
	}
	
}
