package eal.item.type.bloom;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import eal.item.type.Item;

public enum BloomKnowledgeType { FACTUAL, CONCEPTUAL, PROCEDURAL, METACOGNITIVE;
	
	public static BloomKnowledgeType valueOf(Object val) {
		
		if (val==null) return null;
		if (val instanceof BloomKnowledgeType) return (BloomKnowledgeType)val;
		
		if (val instanceof Number) { // 1..4 
			try { return BloomKnowledgeType.values()[((Number)val).intValue()-1]; } catch (Exception e) { }
		}
		
		if (val instanceof String) {
			
			// String-encoded index (1..4)
			try { return BloomKnowledgeType.values()[Integer.valueOf((String)val)-1]; } catch (Exception e) {}
			
			// by label
			try {
				List<String> labels = Arrays.stream(BloomKnowledgeType.values()).map(it -> it.name().toLowerCase()).collect(Collectors.toList());
				return BloomKnowledgeType.values()[labels.indexOf(((String)val).toLowerCase())];
			} catch (Exception e) {}
			
			// by URI
			try {
				List<String> uris = Arrays.stream(BloomKnowledgeType.values()).map(it -> it.toURI()).collect(Collectors.toList());
				return BloomKnowledgeType.values()[uris.indexOf(((String)val).toLowerCase())];
			} catch (Exception e) {}				
		}
		
		return null;
	}
	
	public String toURI () {
		return Item.URI_PREFIX + this.toString().toLowerCase();
	}

}