package eal.item.type.bloom;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import eal.item.type.Item;

public enum BloomLevel { NONE, REMEMBER, UNDERSTAND, APPLY, ANALYZE, EVALUATE, CREATE; 
	
	/**
	 * @param val can be instance of BloomLevel, a number (0...6), a string number (0..6) or a string (e.g., "remember") 
	 * @return
	 */
	public static BloomLevel valueOf (Object val) {
		
		if (val==null) return BloomLevel.NONE;
		
		if (val instanceof BloomLevel) return (BloomLevel)val;
		
		if (val instanceof Number) {	// 0..6 
			try { return  BloomLevel.values()[((Number)val).intValue()]; } catch (Exception e) { }
		}
		
		if (val instanceof String) {
			
			// String-encoded index (0..6)
			try {  return BloomLevel.values()[Integer.valueOf((String)val)]; } catch (Exception e) {}
			
			// by label
			try {
				List<String> labels = Arrays.stream(BloomLevel.values()).map(it -> it.name().toLowerCase()).collect(Collectors.toList());
				return BloomLevel.values()[labels.indexOf(((String)val).toLowerCase())];
			} catch (Exception e) {}

			// by URI
			try {
				List<String> uris = Arrays.stream(BloomLevel.values()).map(it -> it.toURI()).collect(Collectors.toList());
				return BloomLevel.values()[uris.indexOf(((String)val).toLowerCase())];
			} catch (Exception e) {}				
		}
		
		return BloomLevel.NONE;
	}
	
	public String toURI () {
		return  Item.URI_PREFIX + this.toString().toLowerCase();
	}
	
	
	public static BloomLevel[] init() {
		BloomLevel[] bloom = new BloomLevel[BloomKnowledgeType.values().length];
		for (int index=0; index<BloomKnowledgeType.values().length; index++) {
			bloom[index] = BloomLevel.NONE;
		}
		return bloom;
	}
}