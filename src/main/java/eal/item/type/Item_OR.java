package eal.item.type;

import eal.item.format.json.Json_Item_OR;

public class Item_OR extends ItemWAnswers {

	public final static String type = "OR";
	
	public enum Direction { 
		HORIZONTAL, VERTICAL; 
	
		public String getLabel() {
			return this.name().toLowerCase();
		}
	};
	
	
	private Direction direction = Direction.VERTICAL;

	
	@Override
	public String getType() {
		return Item_OR.type;
	}
	
	
	@Override
	public float getPoints() {
		float res = 0;
		for (int index=0; index<this.getNumberOfAnswers(); index++) {
			res += this.getAnswerPoints(index);
		}
		return res;
	}

	public float getAnswerPoints(int index) {
		return this.getAnswerPoints(index, true);
	}
	
	public boolean addAnswer (Object text, Object pointsPositive) {
		return this.addAnswer(text, pointsPositive, null);
	}

	
	@Override
	public int hashCode() {
		return 0;
	}
	
	
	@Override
	public String toString() {
		return Json_Item_OR.toJSON(this, imgIdx -> ""+imgIdx  ).toString(4);
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public void setDirection(Object direction) {
		if (direction==null) return;
		
		if (direction instanceof String) {
			setDirection((String)direction);
			return;
		}

		if (direction instanceof Direction) {
			setDirection((Direction)direction);
			return;
		}
	}

	public void setDirection (String direction) {
		for (Direction d: Direction.values()) {
			if (d.getLabel().toLowerCase().indexOf(direction.toLowerCase().trim()) == 0) {
				this.direction = d;
			}
		}
	}
	


}
