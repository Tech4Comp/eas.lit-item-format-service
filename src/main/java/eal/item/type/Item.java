package eal.item.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.Jsoup;

import eal.item.type.bloom.BloomKnowledgeType;
import eal.item.type.bloom.BloomLevel;
import eal.item.type.media.Images;

public abstract class Item {

	public final static String URI_PREFIX = "http://tech4comp/eal/";

	private String ealid = null;
	private String title = null;
	private String description = null;
	private String question = null;
	private String note = null;
	private boolean flag = false;
	private String project = null;
	private Integer difficulty = null;
	private Float expectedSolvability = null;
	private ArrayList<String> contributors = new ArrayList<>();
	


	private String status = null;
	private ArrayList<String> topcis = new ArrayList<>();





	//  a revision of Bloom’s Taxonomy "A Taxonomy for Teaching, Learning, and Assessment". 2001
	private BloomLevel[] bloom = null;
	private Images images = null;
		
	public Item() {
		super();
		bloom = BloomLevel.init();
		images = new Images();
	}
	
	
	
	public abstract float getPoints();

	public abstract String getType();
	

	
	public String getEalid() {
		return ealid;
	}

	public void setEalid(Object ealid) {
		this.ealid = getStringValue(ealid);
	}
	
	public Images getImages() {
		return images;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(Object title) {
		this.title = getStringValue(title);
	}

	public String getProject() {
		return project;
	}

	public void setProject(Object project) {
		this.project = getStringValue(project);
	}
	
	
	public String getDescription() {
		return description;
	}

	public void setDescription(Object description) {
		this.description = getStringValue(description);
	}
	
	public String getQuestion() {
		return question;
	}
	
	public void setQuestion(Object question) {
		 this.question = getStringValue(question);
	}
	
	public String getNote() {
		return note;
	}

	public void setNote(Object note) {
		this.note = getStringValue(note);
	}

	public boolean getFlag() {
		return flag;
	}
	
	public void setFlag (Object flag) {
		
		if (flag==null) return;
		
		if (flag instanceof String) {
			String s = (String)flag;
			this.flag = !(s.equals("") || s.equals ("0") || s.equalsIgnoreCase("false"));
		}
		
		if (flag instanceof Number) {
			int n = ((Number)flag).intValue();
			this.flag = n!=0;
		}
		
		if (flag instanceof Boolean) {
			this.flag = ((Boolean)flag).booleanValue();
		}
	}
	

	public Integer getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(Object difficulty) {
		if (difficulty instanceof Number) {
			this.difficulty = ((Number)difficulty).intValue();
		}
	}

	public Float getExpectedSolvability() {
		return expectedSolvability;
	}

	public void setExpectedSolvability(Float expectedSolvability) {
		if (expectedSolvability instanceof Number) {
			this.expectedSolvability = ((Number)expectedSolvability).floatValue();
		}	
	}


	public BloomLevel getBloomLevel(BloomKnowledgeType knowledgeType) {
		return (knowledgeType==null) ? BloomLevel.NONE : bloom[knowledgeType.ordinal()];
	}
	
	public BloomLevel getBloomLevel(Object knowledgeType) {
		return  getBloomLevel(BloomKnowledgeType.valueOf(knowledgeType));
	}

	public void setBloomLevel(BloomKnowledgeType knowledgeType, BloomLevel level) {
		if (knowledgeType!=null) {
			bloom[knowledgeType.ordinal()] = (level==null) ? BloomLevel.NONE : level;
		}
	}
	
	public void setBloomLevel(Object knowledgeType, Object level) {
		setBloomLevel (BloomKnowledgeType.valueOf(knowledgeType), BloomLevel.valueOf(level));
	}
	
	
	public ArrayList<String> getContributors() {
		return contributors;
	}

	public void setContributors(ArrayList<String> contributors) {
		this.contributors = contributors;
	}	
	
	public void addContributor (String contributor) {
		this.contributors.add(contributor);
	}

	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ArrayList<String> getTopcis() {
		return topcis;
	}

	public void setTopcis(ArrayList<String> topcis) {
		this.topcis = topcis;
	}

	public void addTopic (String topic) {
		this.topcis.add(topic);
	}




	protected String getStringValue (Object val) {
		
		if (val==null) return null;
		
		String res = null;
		if (val instanceof String) res = (String) val;
		if (val instanceof Number) res = ((Number)val).toString();
		if (res == null) return null;
		if (res.trim().length()==0) return null;
		
		org.jsoup.nodes.Document doc = Jsoup.parseBodyFragment(res);
		return doc.body().html();
//		return Jsoup.clean(doc.body().html(), "", Whitelist.relaxed());

	}
	
	
	

	
	public void parseImages() {
		parseImages(new HashMap<String, byte[]>());
	}
	
	public void parseImages(Map<String, byte[]> content) {
		question = images.parseImages(question, content);
		description = images.parseImages(description, content);
	}
	
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj == null) return false;
		
		if (!(obj instanceof Item)) return false;
		Item other = (Item) obj;
	
	    if (!(this.ealid == null ? 			other.ealid == null : 			this.ealid.equals(other.ealid))) return false;
	    if (!(this.title == null ? 			other.title == null : 			this.title.equals(other.title))) return false;
	    if (!(this.description == null ?	other.description == null : 	this.description.equals(other.description))) return false;
	    if (!(this.question == null ? 		other.question == null :		this.question.equals(other.question))) return false;
	    if (!(this.note == null ? 			other.note == null : 			this.note.equals(other.note))) return false;
		if (!(this.project == null ? 		other.project == null : 		this.project.equals(other.project))) return false;

	    if (this.flag != other.flag) return false;
		
		for (int index=0; index<bloom.length; index++) {
			if (!(this.bloom[index].equals(other.bloom[index]))) return false;
		}
		
		if (!this.images.equals(other.images)) return false;

		if (!this.contributors.equals(other.contributors)) return false;
			
		return true;
	}

	
	
	
	@Override
	public int hashCode() {
		// TODO: HashCode mit EALID???
		return this.ealid == null ? 0 : this.ealid.hashCode();
	}


	
}
