package eal.item.type;

import java.util.ArrayList;
import java.util.List;

import eal.item.format.json.Json_Item_MA;

public class Item_MA extends Item {

	public final static String type = "MA";

	private List<String> terms = new ArrayList<>();
	private List<String> definitions = new ArrayList<>();

	private class Match {
		int term;
		int definition;
		float points;

		Match (int t, int d, float p) {
			term = t;
			definition = d;
			points = p;
		}
	}

	private List<Match> matches = new ArrayList<>();
	

	private String objToString (Object o) {
		if (o instanceof String) return (String)o;
		if (o instanceof Number) return ((Number)o).toString();
		return null;
	}

	public void addTerm (String t) {
		this.terms.add(t);
	}

	public void addTerm (Object o) {
		addTerm(objToString(o));
	}

	public void addDefinition (String d) {
		this.definitions.add(d);
	}

	public void addDefinition (Object o) {
		addDefinition(objToString(o));
	}

	public void addMatch (Object term, Object definition, Object points) {
		addMatch(objToString(term), objToString(definition), objToString(points));
	}
	
	public void addMatch (String term, String definition, String points) {
		float p = 0;
		try {
			p = Float.valueOf(p).floatValue();
		} catch (NumberFormatException e) { }
		addMatch(term, definition, p);
	}

	public void addMatch (String term, String definition, float points) {
		this.matches.add(new Match (this.terms.indexOf(term), this.definitions.indexOf(definition), points));
	}


	public List<String> getDefinitions() {
		return this.definitions;
	}

	public List<String> getTerms () {
		return this.terms;
	}

	public int getNumberOfMatches () {
		return this.matches.size();
	}

	public String getMatchDefinition (int index) {
		return this.definitions.get(this.matches.get(index).definition);
	}

	public String getMatchTerm (int index) {
		return this.terms.get(this.matches.get(index).term);
	}

	public int getMatchDefinitionId (int index) {
		return this.matches.get(index).definition;
	}

	public int getMatchTermId (int index) {
		return this.matches.get(index).term;
	}

	public float getMatchPoints (int index) {
		return this.matches.get(index).points;
	}
	


	@Override
	public String getType() {
		return Item_MA.type;
	}
	
	@Override
	public float getPoints() {
		return (float) matches.stream().mapToDouble(m -> m.points).sum();
	}


	@Override
	public boolean equals(Object obj) {
		
		if (!super.equals(obj)) return false;
		
		if (!(obj instanceof Item_MA)) return false;
		Item_MA other = (Item_MA) obj;

		return this.definitions.equals(other.definitions) && this.terms.equals(other.terms) && this.matches.equals(other.matches);
	}
	
	@Override
	public int hashCode() {
		return 0;
	}
	
	
	@Override
	public String toString() {
		return Json_Item_MA.toJSON(this, imgIdx -> ""+imgIdx  ).toString(4);
	}
	
}
