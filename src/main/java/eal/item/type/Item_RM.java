package eal.item.type;

import eal.item.format.json.Json_Item_RM;

public class Item_RM extends Item {

	public final static String type = "RM";

	private String url;

	@Override
	public String getType() {
		return Item_RM.type;
	}

	@Override
	public float getPoints() {
		return 0;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(Object url) {
		this.url = getStringValue(url);
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (!super.equals(obj)) return false;
		
		if (!(obj instanceof Item_RM)) return false;
		Item_RM other = (Item_RM) obj;

	    if (!(this.url == null ? 			other.url == null : 			this.url.equals(other.url))) return false;

		return true;
	}
	
	@Override
	public int hashCode() {
		return 0;
	}
	
	
	@Override
	public String toString() {
		return Json_Item_RM.toJSON(this, imgIdx -> ""+imgIdx  ).toString(4);
	}

	
}
