package eal.item.store;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.json.JSONObject;

public class HTTPHelper {

	private static HttpClientBuilder clientBuilder;
	// private static Function<Long, String> timeInSeconds = startTime -> String.format("%.1f", (System.currentTimeMillis() - startTime)/1000.0);
	
	private static enum REQUEST_TYPE { GET, PUT, POST }; 
	
	
	
	public static byte[] getURIAsByteArray (String uri, int maxNumberOfRetries)  {
		HttpResult res = sendRequest(uri, null, false, REQUEST_TYPE.GET, maxNumberOfRetries);
		return (res.getStatusCode()==200) ? res.getContent() : null;
	}
	
	
	public static JSONObject getURIAsJSON (String uri, int maxNumberOfRetries)  {
		return sendRequest(uri, null, true, REQUEST_TYPE.GET, maxNumberOfRetries).toJSON();
	}	
	
	public static JSONObject postPutAsByteArray (String uri, byte[] content, int maxNumberOfRetries) {
		return sendRequest(uri, content, false, REQUEST_TYPE.PUT, maxNumberOfRetries).toJSON();
	}
	
	public static JSONObject postPutOrPostURIAsJSON (String uri, JSONObject json, boolean isPost, int maxNumberOfRetries) {
		return sendRequest(uri, json.toString().getBytes(StandardCharsets.UTF_8), true, isPost ? REQUEST_TYPE.POST : REQUEST_TYPE.PUT, maxNumberOfRetries).toJSON();
	}
	
	

	
	private static HttpResult sendRequest (String uri, byte[] content, boolean contentIsJson, REQUEST_TYPE type, int maxNumberOfRetries) {
		
		int statusCode = -1;
		CloseableHttpClient client = getHttpClient();
		
		for (int retry=0; retry<maxNumberOfRetries; retry++) {
			
			// long startTime = System.currentTimeMillis();
			
			try {
				CloseableHttpResponse response;
				HttpRequestBase httpRequest; 
				
				if (type == REQUEST_TYPE.GET) {
					httpRequest = new HttpGet(uri);
				} else {
					httpRequest = (type==REQUEST_TYPE.POST) ? new HttpPost(uri) : new HttpPut(uri);
				    ((HttpEntityEnclosingRequestBase) httpRequest).setEntity(EntityBuilder.create().setBinary(content).build());
				}

				if (contentIsJson) {
					httpRequest.addHeader("Content-Type", "application/json");
					httpRequest.addHeader("Accept", "application/json");
				}
				
			    response = client.execute(httpRequest);
			    statusCode = response.getStatusLine().getStatusCode();
		    	byte[] res = IOUtils.toByteArray(response.getEntity().getContent());
//				System.out.println(String.format("sendRequest: %s (%s seconds, try=%d)", uri, timeInSeconds.apply(startTime), retry+1));
				try { client.close(); } catch (IOException e) {	}
				return new HttpResult(statusCode, res);
				
			} catch (Exception e) { 
				// we are retrying anyway
			}
		    
//			System.out.println(String.format("sendRequest: %s ERROR(%d) %d of %d (%s seconds)", uri, statusCode, retry+1, maxNumberOfRetries, timeInSeconds.apply(startTime)));
		}
		
		try { client.close(); } catch (IOException e) {	}
		
		return new HttpResult(statusCode, new byte[] {});
	}
	
	
	private static CloseableHttpClient getHttpClient ()  {

		if (clientBuilder == null) {
			try {
				clientBuilder = HttpClients
					.custom()
					.setSSLContext(SSLContextBuilder
						.create()
						.loadTrustMaterial(null, (certificate, authType) -> true)	// accept all SSL certificates
						.build())
					.setSSLHostnameVerifier(new NoopHostnameVerifier());	// accept all hostnames
			} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
				e.printStackTrace();
			}	
		}
		return clientBuilder.build();
	}
	
}
