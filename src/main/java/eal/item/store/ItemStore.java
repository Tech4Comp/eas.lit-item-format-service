package eal.item.store;

import java.lang.*;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.json.JSONArray;
import org.json.JSONObject;

import eal.item.format.json.Json_Item;
import eal.item.type.Item;
import eal.item.type.Item_FT;
import eal.item.type.Item_MC;
import eal.item.type.Item_OR;
import eal.item.type.Item_ParseException;
import eal.item.type.Item_RM;
import eal.item.type.Item_SC;

public class ItemStore {

	// private static final String ITEM_API_URL = "https://item.easlit.erzw.uni-leipzig.de/item";
	// private static final String PROJECT_API_URL = "http://item.easlit.erzw.uni-leipzig.de/project/";
	private static final String ITEM_API_URL = System.getenv("SERVICE_URL_ITEM") + "/item";
	private static final String PROJECT_API_URL = System.getenv("SERVICE_URL_ITEM").replace("https://","http://") + "/project/";

	private static final  Map<String, String> itemRoutes = new HashMap<String, String>() {
		private static final long serialVersionUID = 1L;
		{
			put(Item_SC.type, "/singleChoice");
			put(Item_MC.type, "/multipleChoice");
			put(Item_FT.type, "/freeText");
			put(Item_OR.type, "/arrangement");
			put(Item_RM.type, "/remote");
		}
	};

	public static ItemWithError[] saveItems(Item[] items, boolean createNew, String project) {
		if (project != null) {
			for (Item i: items) {
				i.setProject(PROJECT_API_URL + project);
			}
		}

		return Arrays.stream(items)
				.peek(item -> System.out.println((String.format("ItemStore.saveItems: inputItemId=%s, createNew=%b", item.getEalid(), createNew))))
				.map(item -> uploadItem(item, createNew))
				.peek(itemWError -> {
					System.out.println((String.format("ItemStore.saveItems: hasItem=%b", itemWError.hasItem())));
				})
				.toArray(ItemWithError[]::new);
	}

	public static ItemWithError[] loadItemsById(String[] itemIds) {
		return Arrays.stream(itemIds)
				.peek(id -> System.out.println((String.format("ItemStore.loadItemsById: inputItemId=%s", id))))
				.map(ItemStore::downloadItem)
				.peek(itemWError -> System.out.println((String.format("ItemStore.loadItemsById: hasItem=%b", itemWError.hasItem()))))
				.toArray(ItemWithError[]::new);

	}

	public static ItemWithError[] loadItemsByProject(String project) {

		String uri;
		try {
			uri = ITEM_API_URL + "/*?project=" + URLEncoder.encode(PROJECT_API_URL + project, StandardCharsets.UTF_8.toString()) + "&detailed=true";
		} catch (UnsupportedEncodingException e1) {
			return new ItemWithError[] {};
		}

		JSONObject jsonGet = HTTPHelper.getURIAsJSON(uri, 3);

		if (jsonGet==null) {
	    	return new ItemWithError[] { new ItemWithError(ItemWithError.ERRCODE.NO_RESPONSE, project, null)};
		}

		if (jsonGet.optInt(HttpResult.STATUSCODE, 0) != 200) {
	    	return new ItemWithError[] { new ItemWithError(ItemWithError.ERRCODE.ITEM_NOT_FOUND, project, jsonGet.toString())};
		}

		JSONArray itemJson = jsonGet.optJSONObject(HttpResult.JSONOBJECT).optJSONArray("@graph");
		ItemWithError[] result = new ItemWithError[itemJson.length()];
		for (int i=0; i<itemJson.length(); i++) {
			result[i] = parseItem(itemJson.getJSONObject(i), project);
		}
		return result;
	}


	private static ItemWithError downloadItem(String itemId)  {

		JSONObject jsonGet = HTTPHelper.getURIAsJSON(ITEM_API_URL + "/" + itemId, 3);

		if (jsonGet==null) {
	    	return new ItemWithError(ItemWithError.ERRCODE.NO_RESPONSE, itemId, null);
		}

		if (jsonGet.optInt(HttpResult.STATUSCODE, 0) != 200) {
	    	return new ItemWithError(ItemWithError.ERRCODE.ITEM_NOT_FOUND, itemId, jsonGet.toString());
		}

		return parseItem(jsonGet.optJSONObject(HttpResult.JSONOBJECT), itemId);
	}


	private static ItemWithError parseItem (JSONObject jsonObject, String errorMsg) {
		Item res = Json_Item.parse (jsonObject);
		if (res != null) {
			return new ItemWithError(res);
		} else {
	    	return new ItemWithError(ItemWithError.ERRCODE.ITEM_PARSE, errorMsg, jsonObject.toString());
		}
	}

	private static ItemWithError uploadItem(Item item, boolean createNew)  {

		if (item instanceof Item_ParseException) {
			return new ItemWithError(ItemWithError.ERRCODE.ITEM_PARSE, "Could not be parsed from input document.", ((Item_ParseException) item).getParseExceptionMessage());
		}

		String route = itemRoutes.get(item.getType());
		if (route == null) {
	    	return new ItemWithError(ItemWithError.ERRCODE.ITEM_TYPE_NOT_SUPPORTED, item.getType(), null);
		}

		// ckeck if item exists in data store
		if ((item.getEalid() != null) && !createNew) {

			JSONObject jsonGet = HTTPHelper.getURIAsJSON(item.getEalid(), 3);
			String itemId = Optional.ofNullable(jsonGet.optJSONObject(HttpResult.JSONOBJECT)).orElse(new JSONObject()).optString(Json_Item.ID, null);
			if (itemId==null) {	// we did not get any item
				if (jsonGet.optInt(HttpResult.STATUSCODE, 0) == 404) {
					createNew = true;	// Could not get item --> we ensure that a new item is created
				} else {
			    	return new ItemWithError(ItemWithError.ERRCODE.ITEM_NOT_FOUND, item.getEalid(), jsonGet.toString());
				}
			}

			if (!itemId.equals(item.getEalid())) {
		    	return new ItemWithError(ItemWithError.ERRCODE.DIFFERENT_ITEM_ID, item.getEalid(), itemId);
			}

		}

		JSONObject jsonInput = Json_Item.removeId (Json_Item.toJSON(item, item.getImages().IMAGES_URI_EASLIT));

		JSONObject jsonOutput = HTTPHelper.postPutOrPostURIAsJSON(ITEM_API_URL + route, jsonInput, ((item.getEalid() == null) || createNew), 3);
		if (jsonOutput.optInt(HttpResult.STATUSCODE) == 200) {

			System.out.println(jsonOutput);

			Item res = Json_Item.parse (jsonOutput.optJSONObject(HttpResult.JSONOBJECT));
			if (res != null) {
				res.parseImages();
				return new ItemWithError(res);
			} else {
		    	return new ItemWithError(ItemWithError.ERRCODE.ITEM_PARSE, item.getEalid(), jsonOutput.toString());
			}
		} else {
	    	return new ItemWithError(ItemWithError.ERRCODE.UPLOAD, item.getEalid(), jsonOutput.toString());
		}
	}
}
