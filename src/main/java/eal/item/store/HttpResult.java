package eal.item.store;

import java.nio.charset.StandardCharsets;

import org.json.JSONException;
import org.json.JSONObject;

public class HttpResult  {
	


	private final int statusCode; 
	private final byte[] content;
	
	public static final String STATUSCODE = "statusCode";
	public static final String JSONOBJECT = "jsonObject";
	public static final String TEXT = "text";
	
	public HttpResult(int statusCode, byte[] content) {
		super();
		this.statusCode = statusCode;
		this.content = content;
	}
	
	public JSONObject toJSON () {
		try {
			JSONObject json = new JSONObject (new String (content, StandardCharsets.UTF_8));
			return new JSONObject().put(STATUSCODE, statusCode).put(JSONOBJECT, json); 
		} catch (JSONException e) {
			return new JSONObject().put(STATUSCODE, statusCode).put(TEXT, new String (content, StandardCharsets.UTF_8));
		}
	}
	
	
	public int getStatusCode() {
		return statusCode;
	}

	public byte[] getContent() {
		return content;
	}
	
	
}