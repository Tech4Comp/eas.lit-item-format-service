package eal.item.store;

import org.json.JSONObject;

import eal.item.format.json.Json_Item;
import eal.item.type.Item;

public class ItemWithError {

	public enum ERRCODE { OK, ITEM_TYPE_NOT_SUPPORTED, ITEM_NOT_FOUND, DIFFERENT_ITEM_ID, ITEM_PARSE, UPLOAD, NO_RESPONSE; }
	
	
	private final Item item;
	private final ERRCODE errorCode;
	private final String errorMsg;
	private final String errorCause;
	
	
	public ItemWithError(Item item) {
		this.item = item;
		this.errorCode = ERRCODE.OK;
		this.errorMsg = null;
		this.errorCause = null;
	}
	
	
	public ItemWithError(ERRCODE errorCode, String errorMsg, String errorCause) {
		this.item = null;
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
		this.errorCause = errorCause;
	}
	
	public boolean hasItem() {
		return this.item != null;
	}
	
	public Item getItem() {
		return this.item;
	}
	
	public ERRCODE getErrorCode() {
		return errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public String getErrorCause() {
		return errorCause;
	}
	
	public JSONObject toJSON () {
		
		return (this.item == null) 
			? new JSONObject()
				.put("error", new JSONObject()
					.put ("code", this.errorCode)
					.put ("msg", this.errorMsg)
					.put ("cause", this.errorCause)) 
			: new JSONObject()
				.put ("item", Json_Item.toJSON (this.item, this.item.getImages().IMAGES_URI_ORIGINAL));
	}
	
}
