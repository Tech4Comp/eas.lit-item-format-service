package eal.item;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.json.JSONArray;
import org.json.JSONObject;

import eal.item.format.Exporter;
import eal.item.format.Importer.ImporterInitException;
import eal.item.format.Importer.ParseException;
import eal.item.format.InputFormat;
import eal.item.format.OutputFormat;
import eal.item.store.ItemStore;
import eal.item.store.ItemWithError;
import eal.item.store.ItemWithError.ERRCODE;
import eal.item.type.Item;
import eal.item.type.Item_ParseException;

public class Service {

	private Service() {
	}

	public static void convertItems(InputFormat formatIn, InputStream in, OutputFormat formatOut, HttpServletResponse response) throws Exception {

		Item[] items = parseItems(formatIn, in, response);
		if (items.length == 0) {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}
		
		if (response.getStatus() != HttpServletResponse.SC_NOT_ACCEPTABLE) {
			writeItems(items, formatOut, response);
		}
	}

	public static void importItems(InputFormat format, InputStream in, boolean createNew, String project, HttpServletResponse response) throws IOException {

		Item[] items = parseItems(format, in, response);
		if (items.length == 0) {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
			return;
		}

		ItemWithError[] allItems = ItemStore.saveItems(items, createNew, project);
		long numberOfItemsWithErrors = Arrays.stream(allItems).filter(item -> !item.hasItem()).count();
		if (numberOfItemsWithErrors>0) {
			response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
			response.setHeader("EASLIT-ImportItems", String.format("%d out of %d items could not be imported", numberOfItemsWithErrors, allItems.length));
		}

		response.setCharacterEncoding(StandardCharsets.UTF_8.name());
		response.setHeader("Content-Type", OutputFormat.json.getContentType());
		PrintWriter p = new PrintWriter(
				new OutputStreamWriter(response.getOutputStream(), StandardCharsets.UTF_8.name()), true);
		p.write(new JSONObject()
				.put("result",
						new JSONArray(
								Arrays.stream(allItems).map(ItemWithError::toJSON).collect(Collectors.toList())))
				.toString());
		p.flush();

	}

	public static void exportItems(String[] itemIds, String project, OutputFormat format, HttpServletResponse response) throws Exception {

		ItemWithError[] loadedItems = (itemIds.length > 0) ? ItemStore.loadItemsById(itemIds) : ItemStore.loadItemsByProject(project);

		Item[] items = Arrays.stream(loadedItems).filter(ItemWithError::hasItem).map(ItemWithError::getItem).toArray(Item[]::new);

		if (items.length == 0) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		JSONObject info = new JSONObject();
		for (ItemWithError it : loadedItems) {
			if (it.hasItem()) {
				info.put(it.getItem().getEalid(), ERRCODE.OK.toString());
			} else {
				info.put(it.getErrorMsg(), it.getErrorCode());
			}
		}

		response.addHeader("EASLIT-LoadedItems", info.toString());

		if (items.length < loadedItems.length) {
			response.sendError(HttpServletResponse.SC_PARTIAL_CONTENT, String.format(
					"%d out of %d items could not be loaded", loadedItems.length - items.length, loadedItems.length));
		}

		writeItems(items, format, response);

	}

	private static void writeItems(Item[] items, OutputFormat format, HttpServletResponse response) throws Exception {

		Exporter exporter = format.getExporter();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int numberOfExportedItems = exporter.create(items, out);

		response.setCharacterEncoding("UTF-8");
		response.setHeader("Content-Type", format.getContentType());

		if (format.getContentDisposition() != null) {
			response.setHeader("Content-Disposition", 
				String.format("attachment; filename=\"%s.%s\"", 
					Objects.requireNonNullElse(exporter.getFileName(), String.valueOf(System.currentTimeMillis())), 
					format.getContentDisposition()));
		}

		if (numberOfExportedItems < items.length) {
			response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
			response.setHeader("EASLIT-ExportItems", String.format("%d out of %d items could not be exported", items.length-numberOfExportedItems, items.length));
		}

		response.getOutputStream().write(out.toByteArray());
	}

	
	private static Item[] parseItems (InputFormat formatIn, InputStream in, HttpServletResponse response) throws IOException {
		try {
			Item[] parsedItems = formatIn.getImporter().parse(in);
		
			// check, if all items could be parsed
			Item[] items = Arrays.stream(parsedItems).filter(item -> !(item instanceof Item_ParseException)).toArray(Item[]::new);
			if (items.length < parsedItems.length) {
				response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
				response.setHeader("EASLIT-ParseItems", String.format("%d out of %d items could not be parsed", parsedItems.length-items.length, parsedItems.length));
			}
			
			return items;
		} catch (ImporterInitException | ParseException e) {
			response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, e.getMessage());
			return new Item[0];
		}
	}





}
