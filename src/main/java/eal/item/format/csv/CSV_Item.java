package eal.item.format.csv;

import java.util.HashMap;
import java.util.Map;
import java.util.function.IntFunction;

import eal.item.type.Item;
import eal.item.type.Item_FT;
import eal.item.type.Item_MA;
import eal.item.type.Item_MC;
import eal.item.type.Item_OR;
import eal.item.type.Item_ParseException;
import eal.item.type.Item_RM;
import eal.item.type.Item_SC;
import eal.item.type.bloom.BloomKnowledgeType;
import eal.item.type.bloom.BloomLevel;

public abstract class CSV_Item {

	
	enum Column {
		TYPE, EALID, TITLE, DESCRIPTION, QUESTION, POINTS, LEVEL_FK, LEVEL_CK, LEVEL_PK, LEVEL_MK, NOTE, FLAG;

		public String getLabel() {
			return this.name().toLowerCase();
		}
	}

	
	public static Map<String, Object> toCSV (Item item, IntFunction<String> imgReference) {
		
		if (item instanceof Item_SC) {
			return CSV_Item_SC.toCSV((Item_SC) item, imgReference);
		}
		if (item instanceof Item_MC) {
			return CSV_Item_MC.toCSV((Item_MC) item, imgReference);
		}
		if (item instanceof Item_FT) {
			return CSV_Item_FT.toCSV((Item_FT) item, imgReference);
		}
		if (item instanceof Item_OR) {
			return CSV_Item_OR.toCSV((Item_OR) item, imgReference);
		}
		if (item instanceof Item_RM) {
			return CSV_Item_RM.toCSV((Item_RM) item, imgReference);
		}
		if (item instanceof Item_MA) {
			return CSV_Item_MA.toCSV((Item_MA) item, imgReference);
		}		
		return null;
	}
	
	
	
	public static Map<String, Object> toCSV (Item item, String type, IntFunction<String> imgReference) {
		
		Map<String, Object> kvMap = new HashMap<String, Object>();
		
		kvMap.put(Column.TYPE.getLabel(), type);
		
		if (item.getEalid() != null) {
			kvMap.put(Column.EALID.getLabel(), item.getEalid());
		}
		
		kvMap.put(Column.TITLE.getLabel(), item.getTitle());
		kvMap.put(Column.DESCRIPTION.getLabel(), item.getImages().convertImagesToLabel(item.getDescription(), imgReference));
		kvMap.put(Column.QUESTION.getLabel(), item.getImages().convertImagesToLabel(item.getQuestion(), imgReference));
		kvMap.put(Column.POINTS.getLabel(), item.getPoints());
		
		
		if (item.getBloomLevel(BloomKnowledgeType.FACTUAL) != BloomLevel.NONE) {
			kvMap.put(Column.LEVEL_FK.getLabel(), item.getBloomLevel(BloomKnowledgeType.FACTUAL).ordinal());
		}
		if (item.getBloomLevel(BloomKnowledgeType.CONCEPTUAL) != BloomLevel.NONE) {
			kvMap.put(Column.LEVEL_CK.getLabel(), item.getBloomLevel(BloomKnowledgeType.CONCEPTUAL).ordinal());
		}
		if (item.getBloomLevel(BloomKnowledgeType.PROCEDURAL) != BloomLevel.NONE) {
			kvMap.put(Column.LEVEL_PK.getLabel(), item.getBloomLevel(BloomKnowledgeType.PROCEDURAL).ordinal());
		}
		if (item.getBloomLevel(BloomKnowledgeType.METACOGNITIVE) != BloomLevel.NONE) {
			kvMap.put(Column.LEVEL_MK.getLabel(), item.getBloomLevel(BloomKnowledgeType.METACOGNITIVE).ordinal());
		}
		
		kvMap.put(Column.NOTE.getLabel(), item.getNote());
		kvMap.put(Column.FLAG.getLabel(), item.getFlag() ? "X" : "");
		
		return kvMap;
	}
	
	

	public static Item parse (Map<String, Object> kvMap) {
		
		
		String type = (String) kvMap.get(Column.TYPE.getLabel());
		switch (type) {
		case CSV_Item_SC.type:
			return CSV_Item_SC.parse(kvMap);
		case CSV_Item_MC.type:
			return CSV_Item_MC.parse(kvMap);
		case CSV_Item_FT.type:
			return CSV_Item_FT.parse(kvMap);
		case CSV_Item_OR.type:
			return CSV_Item_OR.parse(kvMap);
		case CSV_Item_RM.type:
			return CSV_Item_RM.parse(kvMap);		
		case CSV_Item_MA.type:
			return CSV_Item_MA.parse(kvMap);
		default:
			return new Item_ParseException("CSV: unknown type " + type);
		}
	}
	
	

	public static void parse (Map<String, Object> kvMap, Item item) {
		
		// we do not set type (defined by sub-class)
		item.setEalid(kvMap.get(Column.EALID.getLabel()));;
		item.setTitle(kvMap.get(Column.TITLE.getLabel()));
		item.setDescription(kvMap.get(Column.DESCRIPTION.getLabel()));
		item.setQuestion(kvMap.get(Column.QUESTION.getLabel()));
		// we do not set points (virtual property; function of answer points)
		
		item.setBloomLevel(BloomKnowledgeType.FACTUAL, kvMap.get(Column.LEVEL_FK.getLabel()));
		item.setBloomLevel(BloomKnowledgeType.CONCEPTUAL, kvMap.get(Column.LEVEL_CK.getLabel()));
		item.setBloomLevel(BloomKnowledgeType.PROCEDURAL, kvMap.get(Column.LEVEL_PK.getLabel()));
		item.setBloomLevel(BloomKnowledgeType.METACOGNITIVE, kvMap.get(Column.LEVEL_MK.getLabel()));
		
		item.setNote(kvMap.get(Column.NOTE.getLabel()));
		item.setFlag(kvMap.get(Column.FLAG.getLabel()));
		
	}
	
	

}

