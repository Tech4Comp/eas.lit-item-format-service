package eal.item.format.csv;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import eal.item.type.Item;
import eal.item.type.ItemWAnswers;
import eal.item.type.Item_OR;

public class CSV_Table {

	private String[] header;
	private Item[] items;

	
	public String[] getHeader() {
		return header;
	}

	public Item[] getItems () {
		return this.items;
	}

	public CSV_Table(List<Object[]> dataString) {
		this (dataString, null);
	}
	
	public CSV_Table(List<Object[]> dataString, String itemType) {

		// get header (first line)
		this.header = Arrays.stream(dataString.remove(0)).map(it -> ((String)it).toLowerCase()).toArray(String[]::new);

		// convert rows from String[] to Object[] to kvMap to Item
		this.items = dataString.stream()
			.map(row -> {
				Map<String, Object> kvMap = new HashMap<String, Object>();
				for (int index = 0; index < header.length; index++) {
					kvMap.put(header[index], row[index]);
				}
				
				// overwrite itemType if set
				if (itemType != null) {
					kvMap.put (CSV_Item.Column.TYPE.getLabel(), itemType);
				}

				return CSV_Item.parse(kvMap);
			})
			.toArray(size -> new Item[size]);
	}

	
	public CSV_Table(Item[] items) {

		// FIXME warum allKeysSorted nochmal ... da muss ja jeder Typ alles geben?
		
		this.items = items;

		// all items + merge of all their keys (columns)
		List<String> itemKeys = Stream.of(items)
				.map(item -> CSV_Item.toCSV(item, index -> ""))	// do not need parsing of image references because we only want to get the columns keys 
				.map(it -> it.keySet())
				.flatMap(it -> it.stream())
				.distinct()
				.collect(Collectors.toList());

		
		// sorted list of all possible keys (columns)
		List<String> allKeysSorted = Arrays.asList(CSV_Item.Column.values()).stream()
				.map(col -> col.getLabel())
				.collect(Collectors.toList());
		
		allKeysSorted.addAll(Arrays.asList(CSV_Item_MC.Column.MINNUMBER, CSV_Item_MC.Column.MAXNUMBER).stream()
				.map(col -> col.getLabel()).collect(Collectors.toList()));

		allKeysSorted.addAll(Arrays.asList(CSV_Item_RM.Column.URL).stream()
				.map(col -> col.getLabel()).collect(Collectors.toList()));
		
		// add answer options
		int maxNumberOfAnswerOptions = Arrays.stream(items)
			.filter(item -> item instanceof ItemWAnswers)
			.mapToInt(item -> ((ItemWAnswers)item).getNumberOfAnswers())
			.max().orElse(0);
		
		AtomicInteger indexAnswer = new AtomicInteger(0);
		while (indexAnswer.incrementAndGet() <= maxNumberOfAnswerOptions) {
			allKeysSorted.addAll(
					Arrays.asList(CSV_Item_MC.Column.ANSWER, CSV_Item_MC.Column.POINTS, CSV_Item_MC.Column.POINTSNOT)
							.stream()
							.map(col -> col.getLabel())
							.map(label -> label + indexAnswer) // append index value to label
							.collect(Collectors.toList()));
		}
		

		if (Arrays.stream(items).filter(item -> item instanceof Item_OR).count() > 0) {
			allKeysSorted.add(CSV_Item_OR.Column.DIRECTION.getLabel());
		}
		// header = sorted list of all item keys
		this.header = allKeysSorted.stream().filter(it -> itemKeys.contains(it)).toArray(String[]::new);

	}

}
