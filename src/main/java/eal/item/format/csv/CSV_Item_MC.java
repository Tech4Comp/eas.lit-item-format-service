package eal.item.format.csv;

import java.util.Map;
import java.util.function.IntFunction;

import eal.item.type.Item_MC;

public class CSV_Item_MC {

	public final static String type = "MC";

	enum Column {
		ANSWER, POINTS, POINTSNOT, MINNUMBER, MAXNUMBER;

		public String getLabel() {
			return this.name().toLowerCase();
		}
	}
	
	
	public static Map<String, Object> toCSV (Item_MC item, IntFunction<String> imgReference) {
		
		Map<String, Object> kvMap = CSV_Item.toCSV (item, CSV_Item_MC.type, imgReference);
		
		for (int index=0; index<item.getNumberOfAnswers(); index++) {
			kvMap.put(Column.ANSWER.getLabel() + (index+1), item.getAnswerText(index));
			kvMap.put(Column.POINTS.getLabel() + (index+1), item.getAnswerPoints(index, true));
			kvMap.put(Column.POINTSNOT.getLabel() + (index+1), item.getAnswerPoints(index, false));
		}
		
		kvMap.put(Column.MINNUMBER.getLabel(), item.getMinNumber());
		kvMap.put(Column.MAXNUMBER.getLabel(), item.getMaxNumber());
		
		return kvMap;

	}
	
	
	public static Item_MC parse(Map<String, Object> kvMap) {
		
		Item_MC item = new Item_MC();
		CSV_Item.parse(kvMap, item);
		
		int index=0;
		while (kvMap.get(CSV_Item_MC.Column.ANSWER.getLabel() + (++index)) != null) {
						
			item.addAnswer(
				kvMap.get(CSV_Item_MC.Column.ANSWER.getLabel() + index), 
				kvMap.get(CSV_Item_MC.Column.POINTS.getLabel() + index), 
				kvMap.get(CSV_Item_MC.Column.POINTSNOT.getLabel() + index));
		}
		
		item.setMinMaxNumber(kvMap.get(Column.MINNUMBER.getLabel()), kvMap.get(Column.MAXNUMBER.getLabel()));
		
		return item;

	}
	
}
