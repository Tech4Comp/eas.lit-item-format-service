package eal.item.format.csv;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntFunction;
import java.util.zip.ZipOutputStream;

import com.opencsv.CSVWriter;

import eal.item.format.Exporter;
import eal.item.type.Item;

public class CSV_Export_Zip extends Exporter {


	static String MAIN_FILE = "items.csv";
	private static IntFunction<IntFunction<String>> IMAGES_FILENAME = imgOffset -> imgIndex -> String.format("images/ealimg_%d", imgOffset+imgIndex);


	@Override
	public int create(Item[] items, OutputStream out) throws Exception {
		
		AtomicInteger result = new AtomicInteger();
		ZipOutputStream zip = new ZipOutputStream(out);

		CSV_Table tableData = new CSV_Table(items);
		ByteArrayOutputStream csvOut = new ByteArrayOutputStream();
		CSVWriter csv = new CSVWriter(new OutputStreamWriter(csvOut, "UTF-8"));
		csv.writeNext(tableData.getHeader());
		
		AtomicInteger imgOffset = new AtomicInteger(0);
		Arrays.stream(tableData.getItems())
			.map(item -> {
				addAllImages(zip, item, IMAGES_FILENAME.apply (imgOffset.get()));
				return CSV_Item.toCSV(item, IMAGES_FILENAME.apply(imgOffset.getAndAdd(item.getImages().getNumberOfImages())));
			})
			.filter(Objects::nonNull)
			.map(kvMap -> Arrays.stream(tableData.getHeader()).map(key -> kvMap.get(key)).toArray(Object[]::new))
			.forEach(row ->	{
				// create Array of normalized strings for each line 
				csv.writeNext(Arrays.stream(row)
					.map(val -> CSV_Export.getNormalizedStringValue(val))
					.map(val -> val.replaceAll("\\r\\n|\\r|\\n", ""))	// replace line breaks by space
					.toArray(String[]::new));
				
				// count number of rows = number of exported items --> return value
				result.incrementAndGet();
			});	    	
		
		csv.close();
		
		addFileToZip(zip, MAIN_FILE, new ByteArrayInputStream(csvOut.toByteArray()));
		zip.close();
		
		return result.get();
	}
	

}
