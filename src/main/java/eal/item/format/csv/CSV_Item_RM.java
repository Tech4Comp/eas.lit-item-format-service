package eal.item.format.csv;

import java.util.Map;
import java.util.function.IntFunction;

import eal.item.type.Item_RM;

public class CSV_Item_RM {

	public final static String type = "RM";
	
	enum Column {
		URL;

		public String getLabel() {
			return this.name().toLowerCase();
		}
	}
	
	public static Map<String, Object> toCSV (Item_RM item, IntFunction<String> imgReference) {
		
		Map<String, Object> kvMap = CSV_Item.toCSV (item, CSV_Item_RM.type, imgReference);
		kvMap.put(Column.URL.getLabel(), item.getUrl());
		return kvMap;
	}
	
	public static Item_RM parse(Map<String, Object> kvMap) {
		
		Item_RM item = new Item_RM();
		CSV_Item.parse(kvMap, item);
		item.setUrl(kvMap.get(CSV_Item_RM.Column.URL.getLabel()));
		return item;

	}
}
