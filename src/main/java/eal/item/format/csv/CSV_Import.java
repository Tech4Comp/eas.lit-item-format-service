package eal.item.format.csv;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.RFC4180Parser;
import com.opencsv.RFC4180ParserBuilder;

import eal.item.format.Importer;
import eal.item.type.Item;

public class CSV_Import extends Importer {

	private boolean isZipFile;

	public CSV_Import(boolean isZipFile) {
		super();
		this.isZipFile = isZipFile;
	}

	@Override
	public Item[] parse(InputStream in) throws ParseException {
		
		Map<String, byte[]> content; 
		List<String[]> dataString;
		
		try {
			content = isZipFile ? getZipContent(in) : new HashMap<String, byte[]>();
			Reader read = isZipFile ? new StringReader(new String(content.get(CSV_Export_Zip.MAIN_FILE))) : new InputStreamReader(in, "UTF-8");
	
			// need to use this parser for proper handling of "\\", e.g., in SQL-LIKE queries
			// see https://dzone.com/articles/properly-handling-backslashes-using-opencsv
			RFC4180Parser rfc4180Parser = new RFC4180ParserBuilder().build();		
			CSVReaderBuilder csvReaderBuilder = new CSVReaderBuilder(read).withCSVParser(rfc4180Parser);
			CSVReader csv = csvReaderBuilder.build();
			dataString = csv.readAll();
			csv.close();
			
		} catch (IOException e) {
			throw new ParseException("CSV", e);
		}
		
		
		Item[] items = new CSV_Table(dataString.stream()
				.map(it -> Arrays.stream(it).toArray(Object[]::new))
				.collect (Collectors.toList()))
			.getItems();
		
		for (Item item: items) {
			item.parseImages(content);
		}
		
		return items;
	}

	

	

}
