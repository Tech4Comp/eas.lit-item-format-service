package eal.item.format.csv;

import java.util.Map;
import java.util.function.IntFunction;

import eal.item.type.Item_MA;

public class CSV_Item_MA {

	public final static String type = "MA";

	enum Column {
		DEFINITION, TERM, MATCHDEF, MATCHTERM, MATCHPOINTS;

		public String getLabel() {
			return this.name().toLowerCase();
		}
	}
	
	
	public static Map<String, Object> toCSV (Item_MA item, IntFunction<String> imgReference) {
		
		Map<String, Object> kvMap = CSV_Item.toCSV (item, CSV_Item_MA.type, imgReference);
		
		for (int index=0; index < item.getDefinitions().size(); index++) {
			kvMap.put(Column.DEFINITION.getLabel() + (index+1), item.getDefinitions().get(index));
		}
		for (int index=0; index < item.getTerms().size(); index++) {
			kvMap.put(Column.TERM.getLabel() + (index+1), item.getTerms().get(index));
		}
		for (int index=0; index < item.getNumberOfMatches(); index++) {
			kvMap.put(Column.MATCHDEF.getLabel() + (index+1), item.getMatchDefinition(index));
			kvMap.put(Column.MATCHTERM.getLabel() + (index+1), item.getMatchTerm(index));
			kvMap.put(Column.MATCHPOINTS.getLabel() + (index+1), item.getMatchPoints(index));
		}

		return kvMap;

	}
	
	
	public static Item_MA parse(Map<String, Object> kvMap) {
		
		Item_MA item = new Item_MA();
		CSV_Item.parse(kvMap, item);
		
		int index=0;
		while (kvMap.get(Column.DEFINITION.getLabel() + (++index)) != null) {
			item.addDefinition(kvMap.get(Column.DEFINITION.getLabel() + index));
		}

		index=0;
		while (kvMap.get(Column.TERM.getLabel() + (++index)) != null) {
			item.addTerm(kvMap.get(Column.TERM.getLabel() + index));
		}
		
		index=0;
		while (kvMap.get(Column.MATCHDEF.getLabel() + (++index)) != null) {
			item.addMatch(
				kvMap.get(Column.MATCHDEF.getLabel() + index), 
				kvMap.get(Column.MATCHTERM.getLabel() + index),
				kvMap.get(Column.MATCHPOINTS.getLabel() + index)
			);
		}

		return item;

	}
	
}
