package eal.item.format.csv;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import com.opencsv.CSVWriter;

import eal.item.format.Exporter;
import eal.item.type.Item;

public class CSV_Export extends Exporter {


	private boolean forceEaslitURL;
	
	public CSV_Export(boolean forceEaslitURL) {
		super();
		this.forceEaslitURL = forceEaslitURL;
	}
	

	@Override
	public int create(Item[] items, OutputStream out) throws Exception {
		
		AtomicInteger result = new AtomicInteger();

		CSV_Table tableData = new CSV_Table(items);

		ByteArrayOutputStream csvOut = new ByteArrayOutputStream();
		CSVWriter csv = new CSVWriter(new OutputStreamWriter(csvOut, "UTF-8"));
		
		csv.writeNext(tableData.getHeader());

		Arrays.stream(tableData.getItems())
			.map(item -> CSV_Item.toCSV(item, forceEaslitURL ? item.getImages().IMAGES_URI_EASLIT : item.getImages().IMAGES_URI_ORIGINAL))
			.filter(Objects::nonNull)
			.map(kvMap -> Arrays.stream(tableData.getHeader()).map(key -> kvMap.get(key)).toArray(Object[]::new))
			.forEach(row -> {
				csv.writeNext(Arrays.stream(row)
					.map(val -> getNormalizedStringValue(val))
					.map(val -> val.replaceAll("\\r\\n|\\r|\\n", ""))	// replace line breaks by space
					.toArray(String[]::new));

				// count number of rows = number of exported items --> return value
				result.incrementAndGet();					
			});
		
		csv.close();
		
		PrintWriter p = new PrintWriter(out);
		p.write(csvOut.toString("UTF-8"));
		p.close();

		return result.get();
	}
	
	protected static String getNormalizedStringValue(Object val) {

		if (val == null) return "";
		if (val instanceof String) return (String) val;

		// removes ".0" for double values that are actually integer/long values
		if (val instanceof Number) {
			long val_l = ((Number) val).longValue();
			double val_d = ((Number) val).doubleValue();
			return ((double) val_l == val_d) ? String.format("%d", val_l) : String.format("%s", val_d);
		}

		return val.toString();
	}
}
