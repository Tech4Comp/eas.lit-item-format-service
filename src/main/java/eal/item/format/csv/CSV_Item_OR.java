package eal.item.format.csv;

import java.util.Map;
import java.util.function.IntFunction;

import eal.item.type.Item_OR;

public class CSV_Item_OR {

	public final static String type = "OR";

	enum Column {
		DIRECTION;

		public String getLabel() {
			return this.name().toLowerCase();
		}
	}
	
	
	public static Map<String, Object> toCSV (Item_OR item, IntFunction<String> imgReference) {
		
		Map<String, Object> kvMap = CSV_Item.toCSV (item, CSV_Item_OR.type, imgReference);
		
		for (int index=0; index<item.getNumberOfAnswers(); index++) {
			kvMap.put(CSV_Item_MC.Column.ANSWER.getLabel() + (index+1), item.getAnswerText(index));
			kvMap.put(CSV_Item_MC.Column.POINTS.getLabel() + (index+1), item.getAnswerPoints(index));
		}
		
		kvMap.put(Column.DIRECTION.getLabel(), item.getDirection().getLabel());

		return kvMap;
	}
	
	
	public static Item_OR parse(Map<String, Object> kvMap) {
		
		Item_OR item = new Item_OR();
		CSV_Item.parse(kvMap, item);
		
		int index=0;
		while (kvMap.get(CSV_Item_MC.Column.ANSWER.getLabel() + (++index)) != null) {
						
			item.addAnswer(
				kvMap.get(CSV_Item_MC.Column.ANSWER.getLabel() + index), 
				kvMap.get(CSV_Item_MC.Column.POINTS.getLabel() + index)); 
		}
		
		item.setDirection(kvMap.get(Column.DIRECTION.getLabel()));
		return item;
	}
	
	
}
