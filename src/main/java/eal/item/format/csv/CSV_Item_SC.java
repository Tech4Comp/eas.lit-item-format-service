package eal.item.format.csv;

import java.util.Map;
import java.util.function.IntFunction;

import eal.item.type.Item_SC;

public class CSV_Item_SC  {

	public final static String type = "SC";

	
	
	public static Map<String, Object> toCSV (Item_SC item, IntFunction<String> imgReference) {
		
		Map<String, Object> kvMap = CSV_Item.toCSV (item, CSV_Item_SC.type, imgReference);
		
		for (int index=0; index<item.getNumberOfAnswers(); index++) {
			kvMap.put(CSV_Item_MC.Column.ANSWER.getLabel() + (index+1), item.getAnswerText(index));
			kvMap.put(CSV_Item_MC.Column.POINTS.getLabel() + (index+1), item.getAnswerPoints(index));
		}
		
		return kvMap;
	}
	
	public static Item_SC parse(Map<String, Object> kvMap) {
		
		Item_SC item = new Item_SC();
		CSV_Item.parse(kvMap, item);
		
		int index=0;
		while (kvMap.get(CSV_Item_MC.Column.ANSWER.getLabel() + (++index)) != null) {
						
			item.addAnswer(
				kvMap.get(CSV_Item_MC.Column.ANSWER.getLabel() + index), 
				kvMap.get(CSV_Item_MC.Column.POINTS.getLabel() + index), 
				null);
		}
		
		return item;
		
	}
	

	
}
