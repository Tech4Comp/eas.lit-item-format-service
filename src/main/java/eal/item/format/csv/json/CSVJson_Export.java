package eal.item.format.csv.json;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;

import eal.item.format.Exporter;
import eal.item.format.csv.CSV_Item;
import eal.item.format.csv.CSV_Table;
import eal.item.type.Item;

public class CSVJson_Export extends Exporter {

	private boolean byType;	
	private boolean forceEaslitURL;

	public CSVJson_Export(boolean byType, boolean forceEaslitURL) {
		this.byType = byType;
		this.forceEaslitURL = forceEaslitURL;
	}	
	
	@Override
	public int create(Item[] items, OutputStream out) throws Exception {

		AtomicInteger result = new AtomicInteger();

        JSONObject json = new JSONObject();
        
        Arrays.stream(items)
	    	.collect(Collectors.groupingBy(byType ? Item::getType : item -> "All"))
	    	.entrySet()
	    	.forEach(entry -> {	// entry: key=itemType; value=Array of items
				CSV_Table tableData = new CSV_Table(entry.getValue().toArray (new Item[entry.getValue().size()]));
				JSONArray res = new JSONArray();
				res.put (new JSONArray(tableData.getHeader()));

				Arrays.stream(tableData.getItems())
					.map(item -> CSV_Item.toCSV(item, forceEaslitURL ? item.getImages().IMAGES_URI_EASLIT : item.getImages().IMAGES_URI_ORIGINAL))
					.filter(Objects::nonNull)
					.map(kvMap -> Arrays.stream(tableData.getHeader()).map(key -> kvMap.get(key)).toArray(Object[]::new))
					.forEach (it -> {
						res.put (new JSONArray(it));
						result.incrementAndGet();
					});
				
				json.put(entry.getKey(), res);
	    	});
        
        PrintWriter p = new PrintWriter (new OutputStreamWriter(out, "UTF-8"), true);
		p.write(json.toString());
		p.flush();

		return result.get();
	}


}
