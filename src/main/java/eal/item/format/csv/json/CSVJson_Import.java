package eal.item.format.csv.json;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import eal.item.format.Importer;
import eal.item.format.csv.CSV_Item_FT;
import eal.item.format.csv.CSV_Item_MC;
import eal.item.format.csv.CSV_Item_OR;
import eal.item.format.csv.CSV_Item_RM;
import eal.item.format.csv.CSV_Item_SC;
import eal.item.format.csv.CSV_Table;
import eal.item.type.Item;

public class CSVJson_Import extends Importer {

	
	@Override
	public Item[] parse(InputStream in) throws ParseException {

		JSONObject json;
		try {
			json = new JSONObject(IOUtils.toString(in, "UTF-8"));
		} catch (IOException e) {
			throw new ParseException("CSVJson", e);
		}
		
		String[] types = (json.optJSONArray("All") != null) ? new String[] { "All" } :  new String[] { CSV_Item_SC.type, CSV_Item_MC.type, CSV_Item_FT.type, CSV_Item_OR.type, CSV_Item_RM.type };
		
		return Arrays.stream(types)
			.filter(type -> json.optJSONArray(type) != null)
			.map(type -> new CSV_Table(getDataFromJSONArray(json.optJSONArray(type)), type).getItems())
			.flatMap(items -> Arrays.stream(items))
			.peek(item -> item.parseImages())
			.toArray (size -> new Item[size]);
	}
	
	public List<Object[]> getDataFromJSONArray (JSONArray ja) {
		
		return StreamSupport
				.stream(ja.spliterator(), false)
				.map(values -> StreamSupport
					.stream(((JSONArray)values).spliterator(), false)
					.toArray())
				.collect(Collectors.toList());
	}
	
}
