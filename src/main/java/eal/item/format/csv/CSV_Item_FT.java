package eal.item.format.csv;

import java.util.Map;
import java.util.function.IntFunction;

import eal.item.type.Item_FT;

public class CSV_Item_FT {

	public final static String type = "FT";
	
	
	public static Map<String, Object> toCSV (Item_FT item, IntFunction<String> imgReference) {
		
		Map<String, Object> kvMap = CSV_Item.toCSV (item, CSV_Item_FT.type, imgReference);
		return kvMap;
	}
	
	public static Item_FT parse(Map<String, Object> kvMap) {
		
		Item_FT item = new Item_FT();
		CSV_Item.parse(kvMap, item);
		
		item.setPoints(kvMap.get(CSV_Item.Column.POINTS.getLabel()));
		return item;

	}
}
