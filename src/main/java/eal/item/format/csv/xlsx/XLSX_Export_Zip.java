package eal.item.format.csv.xlsx;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import eal.item.format.Exporter;
import eal.item.format.csv.CSV_Item;
import eal.item.format.csv.CSV_Table;
import eal.item.type.Item;

public class XLSX_Export_Zip extends Exporter {

	static String MAIN_FILE = "items.xlsx";
	private static IntFunction<IntFunction<String>> IMAGES_FILENAME = imgOffset -> imgIndex -> String.format("images/ealimg_%d", imgOffset+imgIndex);

	private boolean byType;

	public XLSX_Export_Zip(boolean byType) {
		this.byType = byType;
	}
	
	@Override
	public int create(Item[] items, OutputStream out) throws Exception {

		AtomicInteger result = new AtomicInteger();

        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file
		ZipOutputStream zip = new ZipOutputStream(out); 

		AtomicInteger imgOffset = new AtomicInteger(0);
        Arrays.stream(items)
	    	.collect(Collectors.groupingBy(byType ? Item::getType : item -> "All"))
	    	.entrySet()
	    	.forEach(entry -> {	// entry: key=itemType; value=Array of items
	    		Sheet sheet = workbook.createSheet (entry.getKey());
	    		CSV_Table tableData = new CSV_Table(entry.getValue().toArray (new Item[entry.getValue().size()]));
	    		XLSX_Export.writeHeader(sheet, tableData.getHeader());
	    		
	    		AtomicInteger rowNum = new AtomicInteger(0);
				Arrays.stream(tableData.getItems())
					.map(item -> { 
						addAllImages(zip, item, IMAGES_FILENAME.apply (imgOffset.get()));
						return CSV_Item.toCSV(item, IMAGES_FILENAME.apply(imgOffset.getAndAdd(item.getImages().getNumberOfImages()))); 
					})
					.filter(Objects::nonNull)
					.map(kvMap -> Arrays.stream(tableData.getHeader()).map(key -> kvMap.get(key)).toArray(Object[]::new))
					.forEach(values -> XLSX_Export.writeNextLine(sheet, values, rowNum.incrementAndGet())); 
				result.addAndGet(rowNum.get());
	    	});
    
		ByteArrayOutputStream xlsxOut = new ByteArrayOutputStream();
		workbook.write(xlsxOut);
		workbook.close();
		addFileToZip(zip, MAIN_FILE, new ByteArrayInputStream(xlsxOut.toByteArray()));
		zip.close();

		return result.get();
	}

}
