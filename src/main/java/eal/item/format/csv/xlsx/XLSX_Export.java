package eal.item.format.csv.xlsx;

import java.io.OutputStream;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import eal.item.format.Exporter;
import eal.item.format.csv.CSV_Item;
import eal.item.format.csv.CSV_Table;
import eal.item.type.Item;

public class XLSX_Export extends Exporter {

	private boolean byType;	
	private boolean forceEaslitURL;

	public XLSX_Export(boolean byType, boolean forceEaslitURL) {
		this.byType = byType;
		this.forceEaslitURL = forceEaslitURL;
	}
	
	@Override
	public int create(Item[] items, OutputStream out) throws Exception {

		AtomicInteger result = new AtomicInteger();

        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file
        
        Arrays.stream(items)
        	.collect(Collectors.groupingBy(byType ? Item::getType : item -> "All"))
        	.entrySet()
        	.forEach(entry -> {	// entry: key=itemType; value=Array of items
        		Sheet sheet = workbook.createSheet (entry.getKey());
        		CSV_Table tableData = new CSV_Table(entry.getValue().toArray (new Item[entry.getValue().size()]));
        		writeHeader(sheet, tableData.getHeader());
        		
        		AtomicInteger rowNum = new AtomicInteger(0);
    			Arrays.stream(tableData.getItems())
    				.map(item -> CSV_Item.toCSV(item, forceEaslitURL ? item.getImages().IMAGES_URI_EASLIT : item.getImages().IMAGES_URI_ORIGINAL))
					.filter(Objects::nonNull)
    				.map(kvMap -> Arrays.stream(tableData.getHeader()).map(key -> kvMap.get(key)).toArray(Object[]::new))
    				.forEach(values -> writeNextLine(sheet, values, rowNum.incrementAndGet())); 
				result.addAndGet(rowNum.get());
        	});
        
		workbook.write(out);
        workbook.close();

		return result.get();
	}

	

	static void writeHeader (Sheet sheet, String[] header) {
        Row headerRow = sheet.createRow(0);
        for(int index = 0; index <header.length; index++) {
            Cell cell = headerRow.createCell(index);
            cell.setCellValue(header[index]);
        }
	}
	
	static void writeNextLine (Sheet sheet, Object values[], int rowNum) {
		Row row = sheet.createRow(rowNum);
        for(int i = 0; i < values.length; i++) {
            Cell cell = row.createCell(i);
            
            if (values[i] == null) continue;
            
            if (values[i] instanceof Number) {
            	cell.setCellValue (((Number) values[i]).doubleValue());
            	continue;
            }

			String v = (String) values[i];
			if (v.length()>32000) v = v.substring(0, 32000);	// The maximum length of cell contents (text) is 32,767 characters
            cell.setCellValue (v);	


        }
	}
	
}
