package eal.item.format.csv.xlsx;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import eal.item.format.Importer;
import eal.item.format.csv.CSV_Item_FT;
import eal.item.format.csv.CSV_Item_MC;
import eal.item.format.csv.CSV_Item_OR;
import eal.item.format.csv.CSV_Item_RM;
import eal.item.format.csv.CSV_Item_SC;
import eal.item.format.csv.CSV_Table;
import eal.item.type.Item;

public class XLSX_Import extends Importer {

	private boolean isZipFile;

	
	public XLSX_Import(boolean isZipFile) {
		super();
		this.isZipFile = isZipFile;
	}
	
	
	@Override
	public Item[] parse(InputStream in) throws ParseException {

		Item[] result;
		
		try {
			Map<String, byte[]> content = isZipFile ? getZipContent(in) : new HashMap<String, byte[]>();
			InputStream xlsxIn = isZipFile ? new ByteArrayInputStream(content.get(XLSX_Export_Zip.MAIN_FILE)) : in;
	
			Workbook workbook = new XSSFWorkbook(xlsxIn);
			String[] sheetNames = (workbook.getSheet("All") != null) ? new String[] { "All" } :  new String[] { CSV_Item_SC.type, CSV_Item_MC.type, CSV_Item_FT.type, CSV_Item_OR.type, CSV_Item_RM.type };
	
			result = Arrays.stream(sheetNames)
				.filter(name -> workbook.getSheet(name) != null)
				.map(name -> new CSV_Table(getDataFromSheet(workbook.getSheet(name)), name.equals("All")?null:name).getItems())
				.flatMap(items -> Arrays.stream(items))
				.peek(item -> item.parseImages(content))
				.toArray (size -> new Item[size]);
			
			workbook.close();
			
		} catch (IOException e) {
			throw new ParseException("XLSX", e);
		}
		
		
		return result;				

	}

	

	private List<Object[]> getDataFromSheet(Sheet sheet) {

		int noOfColumns = sheet.getRow(0).getLastCellNum();
		
		return StreamSupport.stream(sheet.spliterator(), false)
				.map(row -> IntStream.range(0, noOfColumns)
								.mapToObj(index -> row.getCell(index))
								.map(cell -> getCellValue(cell))
								.toArray(size -> new Object[size]))
				.collect(Collectors.toList());
	}

	
	private Object getCellValue(Cell cell) {

		if (cell==null) return null;
		
		switch (cell.getCellType()) {
		case STRING:
			return cell.getRichStringCellValue().getString();
		case NUMERIC:
			if (DateUtil.isCellDateFormatted(cell)) {
				return cell.getDateCellValue();
			} else {
				return cell.getNumericCellValue();
			}
		default:
			return null;
		}

	}

}
