
SELECT 
	"MC" AS type, 
	I.id AS ealid, 
	I.title AS title, 
	I.description AS description, 
	I.question AS question, 
	I.points AS points,
	I.note AS note, 
	I.flag AS flag, 
	I.level_FW AS level_FK, 
	I.level_KW AS level_CK, 
	I.level_PW AS level_PK,
	P.post_modified_gmt,
	M1.answer AS answer1, M1.positive AS points1, M1.negative AS pointsnot1, 
	M2.answer AS answer2, M2.positive AS points2, M2.negative AS pointsnot2,
	M3.answer AS answer3, M3.positive AS points3, M3.negative AS pointsnot3,
	M4.answer AS answer4, M4.positive AS points4, M4.negative AS pointsnot4,
	M5.answer AS answer5, M5.positive AS points5, M5.negative AS pointsnot5, 
	M6.answer AS answer6, M6.positive AS points6, M6.negative AS pointsnot6,
	M7.answer AS answer7, M7.positive AS points7, M7.negative AS pointsnot7, 
	M8.answer AS answer8, M8.positive AS points8, M8.negative AS pointsnot8,
	I.minnumber AS minnumber,
	I.maxnumber AS maxnumber
FROM wp_eal_item I
JOIN wp_posts P ON (P.ID = I.ID)
LEFT OUTER JOIN wp_eal_itemmc M1 ON (M1.item_id = I.ID AND M1.id = 1)
LEFT OUTER JOIN wp_eal_itemmc M2 ON (M2.item_id = I.ID AND M2.id = 2)
LEFT OUTER JOIN wp_eal_itemmc M3 ON (M3.item_id = I.ID AND M3.id = 3)
LEFT OUTER JOIN wp_eal_itemmc M4 ON (M4.item_id = I.ID AND M4.id = 4)
LEFT OUTER JOIN wp_eal_itemmc M5 ON (M5.item_id = I.ID AND M5.id = 5)
LEFT OUTER JOIN wp_eal_itemmc M6 ON (M6.item_id = I.ID AND M6.id = 6)
LEFT OUTER JOIN wp_eal_itemmc M7 ON (M7.item_id = I.ID AND M7.id = 7)
LEFT OUTER JOIN wp_eal_itemmc M8 ON (M8.item_id = I.ID AND M8.id = 8)
WHERE P.post_type = 'itemmc'
AND (P.post_status = 'publish' OR P.post_status = 'pending' OR P.post_status = 'draft')
AND I.domain = 'paedagogik'