
SELECT 
	"SC" AS type, 
	I.id AS ealid, 
	I.title AS title, 
	I.description AS description, 
	I.question AS question, 
	I.points AS points,
	I.note AS note, 
	I.flag AS flag, 
	I.level_FW AS level_FK, 
	I.level_KW AS level_CK, 
	I.level_PW AS level_PK,
	P.post_modified_gmt,
	S1.answer AS answer1, S1.points AS points1, 
	S2.answer AS answer2, S2.points AS points2, 
	S3.answer AS answer3, S3.points AS points3, 
	S4.answer AS answer4, S4.points AS points4, 
	S5.answer AS answer5, S5.points AS points5, 
	S6.answer AS answer6, S6.points AS points6 
FROM wp_eal_item I
JOIN wp_posts P ON (P.ID = I.ID)
LEFT OUTER JOIN wp_eal_itemsc S1 ON (S1.item_id = I.ID AND S1.id = 1)
LEFT OUTER JOIN wp_eal_itemsc S2 ON (S2.item_id = I.ID AND S2.id = 2)
LEFT OUTER JOIN wp_eal_itemsc S3 ON (S3.item_id = I.ID AND S3.id = 3)
LEFT OUTER JOIN wp_eal_itemsc S4 ON (S4.item_id = I.ID AND S4.id = 4)
LEFT OUTER JOIN wp_eal_itemsc S5 ON (S5.item_id = I.ID AND S5.id = 5)
LEFT OUTER JOIN wp_eal_itemsc S6 ON (S6.item_id = I.ID AND S6.id = 6)
WHERE P.post_type = 'itemsc'
AND (P.post_status = 'publish' OR P.post_status = 'pending' OR P.post_status = 'draft')
AND I.domain = 'paedagogik'