package eal.item.format.json;

import java.util.function.IntFunction;

import org.json.JSONObject;

import eal.item.type.Item_RM;

public class Json_Item_RM  {

	public final static String TYPE = "http://tech4comp/eal/RemoteItem";
	
	final static String URL = "remoteItemURL";

	public static Item_RM parse (JSONObject jsonObject) {

		Item_RM item = new Item_RM();
		Json_Item.parse(jsonObject, item);
		item.setUrl(jsonObject.optString(Json_Item_RM.URL, null));
		return item;
	}


	public static JSONObject toJSON(Item_RM item, IntFunction<String> imgReference) {
		return Json_Item.toJSON(item, Json_Item_RM.TYPE, imgReference);
	}
	
}
