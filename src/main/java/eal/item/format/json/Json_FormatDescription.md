# JSON Format Description

JSONOBJECT mit folgenden Attributen

* **@id** wenn keine angegeben, wird beim Speichern im Item Store eine angelegt
* **@type** *required*; beschreibt den Typ des Items, mögliche Werte:
    * ``http://tech4comp/eal/SingleChoiceItem``
    * ``http://tech4comp/eal/MultipleChoiceItem``
    * ``http://tech4comp/eal/OrderingItem``
    * ``http://tech4comp/eal/FreeTextItem``
* **title**
* **description**
* **task**
* **points** Anzahl der Punkte; *required* bei FreeTextItem, bei den anderen ergibt es sich aus den Antwort-Punkten
* **images** JSONARRAY of Strings, die die in title und description referenzierten Bilder identifizieren
* **difficulty** Schwierigkeitsgrad (int, 0...1000)
* **expectedSolvability** Erwartungswert (float, 0...1)


* **author** Autor
* **learningoutcome** Id des Learning Outcomes
* **project** *required?*


* **annotationa** JSONOBJECT

Das **annotation** Attribut hat folgenden Aufbau

* **note** Notiz-String
* **flagged** boolean
* **bloom** JSONARRAY von JSONObjekten, jeweils mit Attributen
    * **knowledgeDimension**: mögliche Werte
      * ``http://tech4comp/eal/factual``
      * ``http://tech4comp/eal/conceptual``
      * ``http://tech4comp/eal/procedural``
      * ``http://tech4comp/eal/metagconitive``
    * **performanceLevel**: mögliche Werte
      * ``http://tech4comp/eal/none``
      * ``http://tech4comp/eal/remember``
      * ``http://tech4comp/eal/understand``
      * ``http://tech4comp/eal/apply``
      * ``http://tech4comp/eal/analyze``
      * ``http://tech4comp/eal/evaluate``
      * ``http://tech4comp/eal/ceate``


## Single-Choice-Item

Zusätzlich das Attribut **answers** JSONARRAY, welches aus einer geordneten Menge voN JSONOBJECTS besteht mit folgendem Aufbau:

* **text** Text der Antwortoption
* **points**  Punktzahl, wenn Antwortoption ausgewählt wird


## Multiple-Choice-Item

Zusätzlich das Attribut **answers** JSONARRAY, welches aus einer geordneten Menge voN JSONOBJECTS besteht mit folgendem Aufbau:

* **text** Text der Antwortoption
* **points**  Punktzahl, wenn Antwortoption ausgewählt wird
* **pointsNotSelected** Punktzahl, wenn Antwortoption nicht ausgewählt wird

Zusätzlich die Attribute

* **minumumSelectableAnswers** Minimale Anzahl an korrekten Antwortoptionen
* **maximumSelectableAnswers** Maximale Anzahl an korrekten Antwortoptionen


## Ordering-Item

Zusätzlich das Attribut **answers** JSONARRAY, welches aus einer geordneten Menge voN JSONOBJECTS besteht mit folgendem Aufbau:

 * **text** Text der Antwortoption
 * **points**  Punktzahl, wenn Antwortoption an korrekte Stelle gesetzt wird

## Free-Text-Item

keine weiteren Attribute



Bilder
* refernziert als URL (http:// oder file:./images/jhj)
* oder inline als base64
-- ohne images-Element in JSONObject


https://item.easlit.erzw.uni-leipzig.de/documentation#!/item/

https://file.easlit.erzw.uni-leipzig.de/documentation#!/image/putImage
