package eal.item.format.json;

import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import org.json.JSONObject;

import eal.item.type.Item_OR;

public class Json_Item_OR {

	public final static String TYPE = "http://tech4comp/eal/ArrangementItem"; // "http://tech4comp/eal/OrderingItem";
	public final static String SEQUENCESTEP = "sequenceStep";
	public final static String DIRECTION = "direction";

	public static JSONObject toJSON(Item_OR item, IntFunction<String> imgReference) {

		return Json_Item
				.toJSON(item, Json_Item_OR.TYPE, imgReference)
				.put(DIRECTION, item.getDirection().getLabel())
				.put(Json_Item_MC.ANSWERS, IntStream.range(0, item.getNumberOfAnswers())
						.mapToObj(index -> new JSONObject()
								.put(Json_Item_MC.TEXT, item.getAnswerText(index))
								.put(Json_Item_MC.POINTS, item.getAnswerPoints(index))
								.put(Json_Item_OR.SEQUENCESTEP, index+1))
						.collect(Collectors.toList())
		);
	}
	
	public static Item_OR parse (JSONObject jsonObject) {

		Item_OR item = new Item_OR();
		
		Json_Item.parse(jsonObject, item);

		item.setDirection(jsonObject.optString(DIRECTION));
		StreamSupport.stream(jsonObject.getJSONArray(Json_Item_MC.ANSWERS).spliterator(), false)
			.map(o -> (JSONObject) o)
			.sorted((a, b) -> a.optInt(Json_Item_OR.SEQUENCESTEP)-b.optInt(Json_Item_OR.SEQUENCESTEP))
			.forEach(o -> 
				item.addAnswer(
					((JSONObject)o).getString(Json_Item_MC.TEXT), 
					((JSONObject)o).optFloat(Json_Item_MC.POINTS, 0)));
		
		return item;
	}
	

	
}
