package eal.item.format.json;

import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.json.JSONObject;

import eal.item.type.Item_MA;

public class Json_Item_MA  {

	public final static String TYPE = "http://tech4comp/eal/MatchItem";
	
	/* JSON KEYS */
	final static String DEFINITIONS = "definitions";
	final static String TERMS = "terms";
	final static String MATCHES = "macthes";
	final static String DEFINITION = "definition";
	final static String TERM = "term";
	final static String POINTS = "points";

	public static Item_MA parse (JSONObject jsonObject) {

		Item_MA item = new Item_MA();
		Json_Item.parse(jsonObject, item);

		jsonObject.getJSONArray(DEFINITIONS).forEach(o -> item.addDefinition(o.toString()));
		jsonObject.getJSONArray(TERMS).forEach(o -> item.addTerm(o.toString()));
		jsonObject.getJSONArray(MATCHES).forEach(o -> 
			item.addMatch(
				((JSONObject)o).getString(TERM), 
				((JSONObject)o).getString(DEFINITION), 
				((JSONObject)o).optFloat(POINTS, 0)));

		return item;
	}


	public static JSONObject toJSON(Item_MA item, IntFunction<String> imgReference) {
		return Json_Item.toJSON(item, Json_Item_MA.TYPE, imgReference)
			.put(DEFINITIONS, item.getDefinitions())
			.put(TERMS, item.getTerms())
			.put(MATCHES, IntStream.range(0, item.getNumberOfMatches())
			.mapToObj(index -> new JSONObject()
				.put(TERM, item.getMatchTerm(index))
				.put(DEFINITION, item.getMatchDefinition(index))
				.put(POINTS, item.getMatchPoints(index)))
			.collect(Collectors.toList()));
	}
	
}
