package eal.item.format.json;

import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.json.JSONObject;

import eal.item.type.Item_SC;

public class Json_Item_SC  {

	public final static String TYPE = "http://tech4comp/eal/SingleChoiceItem";

	public static JSONObject toJSON(Item_SC item, IntFunction<String> imgReference) {

		return Json_Item.toJSON(item, Json_Item_SC.TYPE, imgReference).put(Json_Item_MC.ANSWERS, 
			IntStream.range(0, item.getNumberOfAnswers())
				.mapToObj(index -> new JSONObject()
									.put(Json_Item_MC.TEXT, item.getAnswerText(index))
									.put(Json_Item_MC.POINTS, item.getAnswerPoints(index)))
				.collect(Collectors.toList())
		);
	}
	
	public static Item_SC parse (JSONObject jsonObject) {

		Item_SC item = new Item_SC();
		Json_Item.parse(jsonObject, item);
		jsonObject.getJSONArray(Json_Item_MC.ANSWERS).forEach(o -> 
			item.addAnswer(
				((JSONObject)o).getString(Json_Item_MC.TEXT), 
				((JSONObject)o).optFloat(Json_Item_MC.POINTS, 0), null));
		
		return item;
	}
	

	
}
