package eal.item.format.json;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.StreamSupport;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import eal.item.format.Importer;
import eal.item.type.Item;

public class Json_Import extends Importer {

	private boolean isZipFile;

	
	public Json_Import(boolean isZipFile) {
		super();
		this.isZipFile = isZipFile;
	}

	@Override
	public Item[] parse(InputStream in) throws ParseException   {

		Map<String, byte[]> content;
		String json; 
		
		try {
			content = isZipFile ? getZipContent(in) : new HashMap<String, byte[]>();
			json = isZipFile ? new String(content.get(Json_Export_Zip.JSON_FILENAME), "UTF-8") : IOUtils.toString(in, "UTF-8");
		} catch (IOException e) {
			throw new ParseException("Json", e); 
		}
		
		JSONObject jobj = new JSONObject(json);
		String itemsKey = (jobj.optJSONArray(Json_Export.ITEMS) != null) ? Json_Export.ITEMS : ((jobj.optJSONArray(Json_Export.GRAPH) != null) ? Json_Export.GRAPH : null);

		if (itemsKey == null) {
			throw new ParseException(String.format("Json: Could not find items (neither key %s nor %s found)", Json_Export.ITEMS, Json_Export.GRAPH), null);
		}
		
		
		return StreamSupport.stream(jobj.getJSONArray(itemsKey).spliterator(), false)
			.map(o -> Json_Item.parse((JSONObject) o))
			.peek(item -> item.parseImages(content))
			.toArray (size -> new Item[size]);
	}



}
