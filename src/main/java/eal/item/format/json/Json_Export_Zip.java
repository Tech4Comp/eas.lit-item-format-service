package eal.item.format.json;

import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipOutputStream;

import org.json.JSONArray;
import org.json.JSONObject;

import eal.item.format.Exporter;
import eal.item.type.Item;

public class Json_Export_Zip extends Exporter {

	static String JSON_FILENAME = "items.json";
	private static IntFunction<IntFunction<String>> IMAGES_FILENAME = imgOffset -> imgIndex -> String.format("images/ealimg_%d", imgOffset+imgIndex);
	


	@Override
	public int create(Item[] items, OutputStream out) throws Exception {

		ZipOutputStream zip = new ZipOutputStream(out);
		
		AtomicInteger imgOffset = new AtomicInteger(0);
		
		JSONArray createdItems = new JSONArray(Stream.of(items)
			.map(item -> { 
				addAllImages(zip, item, IMAGES_FILENAME.apply (imgOffset.get()));
				return Json_Item.toJSON(item, IMAGES_FILENAME.apply(imgOffset.getAndAdd(item.getImages().getNumberOfImages()))); 
			})
			.filter(Objects::nonNull)
			.collect(Collectors.toList())
		);
		
		JSONObject res = new JSONObject().put(Json_Export.ITEMS, createdItems);
		addFileToZip(zip, JSON_FILENAME, new ByteArrayInputStream(res.toString().getBytes()));
		zip.close();

		return createdItems.length();
	}	



	
}
