package eal.item.format.json;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.json.JSONArray;
import org.json.JSONObject;

import eal.item.format.Exporter;
import eal.item.type.Item;

public class Json_Export extends Exporter {

	private boolean forceEaslitURL;

	final static String ITEMS = "items";
	final static String GRAPH = "@graph";

	public Json_Export(boolean forceEaslitURL) {
		super();
		this.forceEaslitURL = forceEaslitURL;
	}

	@Override
	public int create(Item[] items, OutputStream out) throws Exception {

		List<JSONObject> createdItems = Stream.of(items)
			.map(item -> Json_Item.toJSON(item,	forceEaslitURL ? item.getImages().IMAGES_URI_EASLIT : item.getImages().IMAGES_URI_ORIGINAL))
			.filter(Objects::nonNull)
			.collect(Collectors.toList());

		PrintWriter p = new PrintWriter(new OutputStreamWriter(out, "UTF-8"), true);
		p.write(new JSONObject().put(ITEMS, new JSONArray(createdItems)).toString());
		p.flush();

		return createdItems.size();
	}	


	
}
