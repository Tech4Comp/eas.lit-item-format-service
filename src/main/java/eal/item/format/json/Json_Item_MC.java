package eal.item.format.json;

import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.json.JSONObject;

import eal.item.type.Item_MC;

public class Json_Item_MC  {

	public final static String TYPE = "http://tech4comp/eal/MultipleChoiceItem";

	/* JSON keys */
	final static String ANSWERS = "answers";
	final static String TEXT = "text";
	final static String POINTS = "points";
	final static String POINTSNOT = "pointsNotSelected";
	final static String MINNUMBER = "minimumSelectableAnswers";
	final static String MAXNUMBER = "maximumSelectableAnswers";
	
	public static JSONObject toJSON(Item_MC item, IntFunction<String> imgReference) {
		
		return Json_Item.toJSON(item, Json_Item_MC.TYPE, imgReference)
				.put(ANSWERS, IntStream.range(0, item.getNumberOfAnswers())
					.mapToObj(index -> new JSONObject()
						.put(TEXT, item.getAnswerText(index))
						.put(POINTS, item.getAnswerPoints(index, true))
						.put(POINTSNOT, item.getAnswerPoints(index, false)))
					.collect(Collectors.toList()))
				
				.put(MINNUMBER, item.getMinNumber())
				.put(MAXNUMBER, item.getMaxNumber());

	}
	
	public static Item_MC parse (JSONObject jsonObject) {

		Item_MC item = new Item_MC();
		Json_Item.parse(jsonObject, item);
		
		jsonObject.getJSONArray(ANSWERS).forEach(o -> 
			item.addAnswer(
				((JSONObject)o).getString(TEXT), 
				((JSONObject)o).optFloat(POINTS, 0), 
				((JSONObject)o).optFloat(POINTSNOT, 0)));

		item.setMinMaxNumber(
			jsonObject.optInt(MINNUMBER, 0), 
			jsonObject.optInt(MAXNUMBER, item.getNumberOfAnswers()));
		
		return item;
	}	
	
	

}
