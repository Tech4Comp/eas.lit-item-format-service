package eal.item.format.json;

import java.util.function.IntFunction;

import org.json.JSONObject;

import eal.item.type.Item_FT;

public class Json_Item_FT  {

	public final static String TYPE = "http://tech4comp/eal/FreeTextItem";
	
	public static Item_FT parse (JSONObject jsonObject) {

		Item_FT item = new Item_FT();
		Json_Item.parse(jsonObject, item);
		item.setPoints(jsonObject.optFloat(Json_Item.POINTS, 0f));
		return item;
	}


	public static JSONObject toJSON(Item_FT item, IntFunction<String> imgReference) {
		return Json_Item.toJSON(item, Json_Item_FT.TYPE, imgReference);
	}
	
}
