package eal.item.format.json;

import java.util.function.IntFunction;

import org.json.JSONArray;
import org.json.JSONObject;

import eal.item.type.Item;
import eal.item.type.Item_FT;
import eal.item.type.Item_MA;
import eal.item.type.Item_RM;
import eal.item.type.Item_MC;
import eal.item.type.Item_OR;
import eal.item.type.Item_ParseException;
import eal.item.type.Item_SC;
import eal.item.type.bloom.BloomKnowledgeType;
import eal.item.type.bloom.BloomLevel;

public abstract class Json_Item {

	public  final static String TYPE = "@type";
	public  final static String ID = "@id";
	private final static String TITLE = "title";
	private final static String PROJECT = "project";
	private final static String DESCRIPTION = "description";
	private final static String QUESTION = "task";
	        final static String POINTS = "points";
	private final static String ANNOTATION = "annotations";
	private final static String BLOOM = "bloom";
	private final static String BLOOM_LEVEL = "performanceLevel";
	private final static String BLOOM_KNOWLEDGETYPE = "knowledgeDimension";
	private final static String NOTE = "note";
	private final static String FLAG = "flagged";
	private final static String DIFFICULTY = "difficulty";
	private final static String SOLVABILITY = "expectedSolvability";
	private final static String CONTRIBUTORS = "contributors";
	private final static String STATUS = "status";
	private final static String TOPICS = "topics";

	
	public static JSONObject toJSON (Item item, IntFunction<String> imgReference) {
		if (item instanceof Item_SC) return Json_Item_SC.toJSON((Item_SC) item, imgReference);
		if (item instanceof Item_MC) return Json_Item_MC.toJSON((Item_MC) item, imgReference);
		if (item instanceof Item_FT) return Json_Item_FT.toJSON((Item_FT) item, imgReference);
		if (item instanceof Item_OR) return Json_Item_OR.toJSON((Item_OR) item, imgReference);
		if (item instanceof Item_RM) return Json_Item_RM.toJSON((Item_RM) item, imgReference);
		if (item instanceof Item_MA) return Json_Item_MA.toJSON((Item_MA) item, imgReference);
		return null;
	}
	
	/**
	 * generate an item-JSON object with ealid and annotation only 
	 * @param item
	 * @return
	 */
	public static JSONObject toJSON_AnnotationOnly (Item item) {
		
		String type = null;
		if (item instanceof Item_SC) type = Json_Item_SC.TYPE;
		if (item instanceof Item_MC) type = Json_Item_MC.TYPE;
		if (item instanceof Item_FT) type = Json_Item_FT.TYPE;
		if (item instanceof Item_OR) type = Json_Item_OR.TYPE;
		if (item instanceof Item_RM) type = Json_Item_RM.TYPE;
		if (item instanceof Item_MA) type = Json_Item_MA.TYPE;
		if (type == null) return null;
		
		JSONObject result = toJSON(item, type, index -> "");
		result.remove(TITLE);
		result.remove(DESCRIPTION);
		result.remove(QUESTION);
		result.remove(POINTS);
		return result;
	}

	
	public static JSONObject toJSON (Item item, String type, IntFunction<String> imgReference) {
				
		JSONObject result = new JSONObject()
			.put(TYPE, type)
			.put(TITLE, item.getTitle())
			.put(PROJECT, item.getProject())
			.put(DESCRIPTION, item.getImages().convertImagesToLabel(item.getDescription(), imgReference))
			.put(QUESTION, item.getImages().convertImagesToLabel(item.getQuestion(), imgReference))
			.put(POINTS, item.getPoints())
			.put(DIFFICULTY, item.getDifficulty())
			.put(SOLVABILITY, item.getExpectedSolvability())
			.put(CONTRIBUTORS, new JSONArray(item.getContributors()));
		


		if (item.getEalid() != null) {
			result.put(ID, item.getEalid());
		}

		JSONArray bloom = new JSONArray();
		for (BloomKnowledgeType knowType: BloomKnowledgeType.values()) {
			if (item.getBloomLevel(knowType) != BloomLevel.NONE) {
				bloom.put (new JSONObject()
						.put(BLOOM_KNOWLEDGETYPE, knowType.toURI())
						.put(BLOOM_LEVEL, item.getBloomLevel(knowType).toURI()));
			}
		}

		JSONObject annotation = new JSONObject()
				.put(BLOOM, bloom)
				.put(NOTE, item.getNote())
				.put(FLAG, item.getFlag())
				.put(STATUS, item.getStatus())
				.put(TOPICS, new JSONArray(item.getTopcis()));

		result.put(ANNOTATION, annotation);
		return result;
	}


	public static JSONObject removeId (JSONObject json) {
		json.remove(ID);
		return json;
	}
	
	
	public static Item parse(JSONObject jsonObject) {

		switch (jsonObject.getString(Json_Item.TYPE)) {
		case Json_Item_SC.TYPE: return Json_Item_SC.parse(jsonObject);
		case Json_Item_MC.TYPE: return Json_Item_MC.parse(jsonObject);
		case Json_Item_FT.TYPE:	return Json_Item_FT.parse(jsonObject);
		case Json_Item_OR.TYPE:	return Json_Item_OR.parse(jsonObject);
		case Json_Item_RM.TYPE:	return Json_Item_RM.parse(jsonObject);
		case Json_Item_MA.TYPE:	return Json_Item_MA.parse(jsonObject);
		default: return new Item_ParseException("Json: unsupported type " + jsonObject.getString(Json_Item.TYPE));
		}

	}
	
	
	public static void parse (JSONObject jsonObject, Item item) {
		
		item.setEalid(jsonObject.optString(ID, null));
		item.setTitle(jsonObject.optString(TITLE, null));
		item.setProject(jsonObject.optString(PROJECT, null));
		item.setDescription(jsonObject.optString(DESCRIPTION, null));
		item.setQuestion(jsonObject.optString(QUESTION, null));
		item.setDifficulty(jsonObject.optInt(DIFFICULTY, -1) == -1 ? null : jsonObject.optInt(DIFFICULTY, -1));
		item.setExpectedSolvability(jsonObject.optFloat(SOLVABILITY, -1) == -1 ? null : jsonObject.optFloat(SOLVABILITY, -1));
		
		JSONArray contributors = jsonObject.optJSONArray(CONTRIBUTORS);
		if (contributors != null) {
			contributors.forEach(c -> item.addContributor((String)c));
		}

		/* points will be computed by list of answers; type is represented by class type */
		JSONObject annotation = jsonObject.optJSONObject(ANNOTATION); 
		if (annotation != null) {
			item.setNote(annotation.optString(NOTE, null));
			item.setFlag(annotation.optBoolean(FLAG, false));
			item.setStatus(annotation.optString(STATUS, null));

			JSONArray topics = annotation.optJSONArray(TOPICS);
			if (topics != null) {
				topics.forEach(t -> item.addTopic((String) t));
			}

			JSONArray bloom = annotation.optJSONArray(BLOOM);
			if (bloom != null) {
				bloom.forEach(o -> item.setBloomLevel (BloomKnowledgeType.valueOf(((JSONObject)o).opt(BLOOM_KNOWLEDGETYPE)), BloomLevel.valueOf(((JSONObject)o).opt(BLOOM_LEVEL))));
			}
		}
	}
}
