package eal.item.format;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.attribute.FileTime;
import java.util.function.IntFunction;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import eal.item.type.Item;




public abstract class Exporter {

	/**
	 * @return number of created (i.e. exported) items
	 */
	public abstract int create (Item[] items, OutputStream out) throws Exception;
	
	public String getFileName () {
		return null;
	}


	protected void addAllImages (ZipOutputStream zip, Item item, IntFunction<String> imgReference) {
		for (int imgIndex=0; imgIndex<item.getImages().getNumberOfImages(); imgIndex++) {
			try {
				addFileToZip(zip, imgReference.apply(imgIndex), new ByteArrayInputStream(item.getImages().getImage(imgIndex)));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	protected static void addFileToZip (ZipOutputStream zip, String name, InputStream is) throws IOException {
		
		ZipEntry qplZip = new ZipEntry(name);
		qplZip.setCreationTime(FileTime.fromMillis(java.lang.System.currentTimeMillis()));
		zip.putNextEntry(qplZip);

		byte[] readBuffer = new byte[2048];
		int amountRead;
		
		while ((amountRead = is.read(readBuffer)) > 0) {
			zip.write(readBuffer, 0, amountRead);
		}
		zip.closeEntry();
		
	}
	
	protected static InputStream getStreamFromDocument (Document doc) throws TransformerConfigurationException, TransformerException, TransformerFactoryConfigurationError, UnsupportedEncodingException, IOException {

		Transformer transformer = TransformerFactory.newInstance().newTransformer();
	    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
	    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
	    transformer.setOutputProperty(OutputKeys.INDENT, "no");
	    
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	    OutputStreamWriter osw = new OutputStreamWriter(outputStream, "UTF-8");
	    transformer.transform(new DOMSource(doc), new StreamResult(osw));
	    
		return new ByteArrayInputStream(outputStream.toByteArray());
	}



	
}
