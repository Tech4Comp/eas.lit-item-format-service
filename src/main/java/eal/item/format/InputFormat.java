package eal.item.format;

import eal.item.format.Importer.ImporterInitException;
import eal.item.format.csv.CSV_Import;
import eal.item.format.csv.json.CSVJson_Import;
import eal.item.format.csv.xlsx.XLSX_Import;
import eal.item.format.ilias.Ilias_Import;
import eal.item.format.json.Json_Import;
import eal.item.format.onyx.Onyx_Import;

public enum InputFormat { csv, csv_zip, xlsx, xlsx_zip, json, json_zip, csvjson, ilias, onyx; 
	
	public Importer getImporter() throws ImporterInitException {
		switch (this) {
		case csv: 		return new CSV_Import(false);
		case csv_zip: 	return new CSV_Import(true);
		case xlsx: 		return new XLSX_Import(false);
		case xlsx_zip:	return new XLSX_Import(true);
		case json:		return new Json_Import(false);
		case json_zip:	return new Json_Import(true);
		case csvjson:	return new CSVJson_Import();
		case ilias:		return new Ilias_Import();	
		case onyx:		return new Onyx_Import();
		default:		return null;
		}
		
		
		
		
	}

}