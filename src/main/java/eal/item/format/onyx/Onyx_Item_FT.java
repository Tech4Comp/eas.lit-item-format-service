package eal.item.format.onyx;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import eal.item.type.Item_FT;

public class Onyx_Item_FT  {

	public final static String interactionType = "extendedTextInteraction";
	public final static String cardinality = "single"; 
	public final static String baseType = "string";

	public static Item_FT parseItemElement (Element xmlNode) throws XPathExpressionException {

		Item_FT item = new Item_FT();
		Onyx_Item.parseItemElement(xmlNode, item);

		XPath xpath = XPathFactory.newInstance().newXPath();
		item.setPoints((String) xpath.evaluate("./outcomeDeclaration[@identifier='MAXSCORE']/defaultValue/value/text()", xmlNode, XPathConstants.STRING));

		return item;
	}

	public static Document createManifestDoc(Item_FT item, String itemIdentifier) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		return Onyx_Item.createManifestDoc(item, itemIdentifier, interactionType);
	}
	
	public static Document createItemDoc(Item_FT item, String itemIdentifier) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		return Onyx_Item.createItemDoc(item, itemIdentifier, cardinality, baseType, "");
	}
	
}
