package eal.item.format.onyx;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import eal.item.type.Item_MC;


public class Onyx_Item_MC {

	public final static String interactionType = "choiceInteraction";
	public final static String cardinality = "multiple"; 
	public final static String baseType = "identifier"; 


	public static Item_MC parseItemElement (Element xmlNode) throws XPathExpressionException {

		Item_MC item = new Item_MC();
		Onyx_ItemWAnswers.parseItemElement(xmlNode, item);
		
		XPath xpath = XPathFactory.newInstance().newXPath();

		String maxChoices = (String) xpath.evaluate("./outcomeDeclaration[@identifier='MAXCHOICES']/defaultValue/value/text()", xmlNode, XPathConstants.STRING);
		item.setMinMaxNumber(0, maxChoices);
		
		return item;
	}
	
	public static Document createManifestDoc(Item_MC item, String itemIdentifier) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		return Onyx_Item.createManifestDoc(item, itemIdentifier, interactionType);
	}
	


	
	public static Document createItemDoc(Item_MC item, String itemIdentifier) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		
		String responseProcessing = "<responseProcessing> <responseCondition> <responseIf> <not> <isNull> <variable identifier=\"RESPONSE_1\"/> </isNull> </not> <setOutcomeValue identifier=\"SCORE\"> <sum> <variable identifier=\"SCORE\"/> <mapResponse identifier=\"RESPONSE_1\"/> </sum> </setOutcomeValue> </responseIf> </responseCondition> <responseCondition> <responseIf> <gt> <containerSize> <variable identifier=\"RESPONSE_1\"/> </containerSize> <variable identifier=\"MAXCHOICES\"/> </gt> <setOutcomeValue identifier=\"SCORE\"> <variable identifier=\"MINSCORE\"/> </setOutcomeValue> </responseIf> </responseCondition> <responseCondition> <responseIf> <gt> <variable identifier=\"SCORE\"/> <variable identifier=\"MAXSCORE\"/> </gt> <setOutcomeValue identifier=\"SCORE\"> <variable identifier=\"MAXSCORE\"/> </setOutcomeValue> </responseIf> </responseCondition> <responseCondition> <responseIf> <lt> <variable identifier=\"SCORE\"/> <variable identifier=\"MINSCORE\"/> </lt> <setOutcomeValue identifier=\"SCORE\"> <variable identifier=\"MINSCORE\"/> </setOutcomeValue> </responseIf> </responseCondition> </responseProcessing>";
		
		Document doc = Onyx_ItemWAnswers.createItemDoc(item, itemIdentifier, cardinality, baseType, responseProcessing);
		XPath xpath = XPathFactory.newInstance().newXPath();
		
		Element mapping = doc.createElement("mapping");
		mapping.setAttribute("defaultValue", "0");
		for (int index=0; index<item.getNumberOfAnswers(); index++) {
			Element mapEntry = doc.createElement("mapEntry");
			mapEntry.setAttribute("mapKey", ""+index);
			mapEntry.setAttribute("mappedValue", String.valueOf(item.getAnswerPoints(index, true)-item.getAnswerPoints(index, false)));
			mapping.appendChild(mapEntry);
		}
		Element responseDeclaration = (Element) xpath.evaluate("./responseDeclaration", doc.getDocumentElement(), XPathConstants.NODE);
		responseDeclaration.appendChild(mapping);

		// MAXCHOICES
		Element value = (Element) xpath.evaluate("./outcomeDeclaration[@identifier=\"MAXCHOICES\"]/defaultValue/value", doc.getDocumentElement(), XPathConstants.NODE);
		value.setTextContent(String.valueOf(item.getMaxNumber()));
		
		return doc;
	}
	
}
