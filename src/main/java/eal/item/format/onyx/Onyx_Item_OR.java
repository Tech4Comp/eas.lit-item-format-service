package eal.item.format.onyx;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import eal.item.type.Item_OR;


public class Onyx_Item_OR  {

	public final static String interactionType = "matchInteraction";
	public final static String cardinality = "multiple"; 
	public final static String baseType = "directedPair";


	public static Item_OR parseItemElement (Element xmlNode) throws XPathExpressionException {

		Item_OR item = new Item_OR();


		return item;
	}
	
	public static Document createManifestDoc(Item_OR item, String itemIdentifier) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		return Onyx_Item.createManifestDoc(item, itemIdentifier, interactionType);
	}
	
	public static Document createItemDoc(Item_OR item, String itemIdentifier) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		String responseProcessing = "<responseProcessing> <responseCondition> <responseIf> <isNull> <variable identifier=\"RESPONSE_1\"/> </isNull> </responseIf> <responseElseIf> <match> <variable identifier=\"RESPONSE_1\"/> <correct identifier=\"RESPONSE_1\"/> </match> <setOutcomeValue identifier=\"SCORE\"> <sum> <variable identifier=\"SCORE\"/> <variable identifier=\"MAXSCORE\"/> </sum> </setOutcomeValue> </responseElseIf> </responseCondition> <responseCondition> <responseIf> <gt> <variable identifier=\"SCORE\"/> <variable identifier=\"MAXSCORE\"/> </gt> <setOutcomeValue identifier=\"SCORE\"> <variable identifier=\"MAXSCORE\"/> </setOutcomeValue> </responseIf> </responseCondition> <responseCondition> <responseIf> <lt> <variable identifier=\"SCORE\"/> <variable identifier=\"MINSCORE\"/> </lt> <setOutcomeValue identifier=\"SCORE\"> <variable identifier=\"MINSCORE\"/> </setOutcomeValue> </responseIf> </responseCondition> </responseProcessing>";
		
		return Onyx_Item.createItemDoc(item, itemIdentifier, cardinality, baseType, responseProcessing);
	}
	
}
