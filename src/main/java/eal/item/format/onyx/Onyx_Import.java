package eal.item.format.onyx;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import eal.item.format.Importer;
import eal.item.type.Item;
import eal.item.type.Item_ParseException;

public class Onyx_Import extends Importer {

	private class Manifest {

		private static final String XMLFILENAME = "imsmanifest.xml";
		private String directory = null;
		private Document xml = null;

		public Manifest(String name, byte[] value) {
			this.directory = name.substring(0, name.length() - XMLFILENAME.length());
			try {
				this.xml = builder.parse(new ByteArrayInputStream(value));
			} catch (SAXException | IOException e) {
			}
		}

	}

	private Map<String, byte[]> content;
	private DocumentBuilder builder;
	private XPath xpath;

	public Onyx_Import() throws ImporterInitException {
		try {
			this.builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			this.builder.setEntityResolver(new EntityResolver() {
				public InputSource resolveEntity(String publicId, String systemId) {
					return new InputSource(new ByteArrayInputStream(new byte[0]));
				}
			});
			this.xpath = XPathFactory.newInstance().newXPath();
		} catch (ParserConfigurationException e) {
			throw new ImporterInitException("Onyx", e);
		}

	}

	@Override
	public Item[] parse(InputStream in) throws ParseException {

		try {
			this.content = getZipContent(in);
		} catch (IOException e) {
			throw new ParseException("Onyx", e);
		}

		// get all manifest xml files and parse each of them
		return this.content.entrySet().stream()
				.filter(entry -> entry.getKey().endsWith("/" + Manifest.XMLFILENAME)
						|| entry.getKey().equals(Manifest.XMLFILENAME))
				.map(entry -> new Manifest(entry.getKey(), entry.getValue())).filter(manifest -> manifest.xml != null)
				.flatMap(manifest -> parseManifest(manifest)).toArray(Item[]::new);
	}

	private Stream<Item> parseManifest(Manifest manifest) {

		NodeList itemResources;
		try {
			// get /manifest/resources/resource[@type='imsqti_item_xmlv2p1']
			itemResources = (NodeList) xpath.evaluate("/manifest/resources/resource[@type='imsqti_item_xmlv2p1']",
					manifest.xml, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			return Stream.empty();
		}

		return IntStream.range(0, itemResources.getLength()).mapToObj(index -> itemResources.item(index))
				.map(node -> (Element) node)
				.map(element -> parseItem(this.content.get(manifest.directory + element.getAttribute("href")),
						manifest.directory));
	}

	private Item parseItem(byte[] value, String baseDir) {

		try {
			Element xmlItem = builder.parse(new ByteArrayInputStream(value)).getDocumentElement();
			return Onyx_Item.parseItemElement(xmlItem);
		} catch (SAXException | IOException e) {
			return new Item_ParseException("Onyx_Import.parseItemElement: " + e.getMessage());
		}
	}


	public static String nodeToString(Node node)  {

		try {
			StringWriter sw = new StringWriter();
			Transformer t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.transform(new DOMSource(node), new StreamResult(sw));
			return sw.toString();
		} catch (TransformerException e) {
			return null;
		}
		
	}
}
