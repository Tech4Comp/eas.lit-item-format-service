package eal.item.format.onyx;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.jsoup.Jsoup;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eal.item.type.ItemWAnswers;
import eal.item.type.Item_MC;

public abstract class Onyx_ItemWAnswers {

	
	

	public static void parseItemElement (Element xmlNode, ItemWAnswers item) throws XPathExpressionException {

		Onyx_Item.parseItemElement(xmlNode, item);

		List<String> answerIds = new ArrayList<String>();
		List<String> correctAnswerIds = new ArrayList<String>();
		
		Map<String, String> answerText = new HashMap<String, String>();
		Map<String, String> answerPointsPos = new HashMap<String, String>();
		Map<String, String> answerPointsNeg = new HashMap<String, String>();

		XPath xpath = XPathFactory.newInstance().newXPath();

		NodeList xmlSimpleChoice = (NodeList) xpath.evaluate("./itemBody/choiceInteraction/simpleChoice", xmlNode, XPathConstants.NODESET);
		for (int index = 0; index < xmlSimpleChoice.getLength(); index++) {
			Node xmlElement = xmlSimpleChoice.item(index);
			String id = (String) xpath.evaluate("./@identifier", xmlElement, XPathConstants.STRING);
			String text = (String) xpath.evaluate("./text()", xmlElement, XPathConstants.STRING);
			// String text = Onyx_Import.nodeToString((Node) xpath.evaluate("./*[1]", xmlElement, XPathConstants.NODE));
			answerIds.add(id);
			answerText.put(id, text);
		}
		
		NodeList xmlCorrectResponse = (NodeList) xpath.evaluate("./responseDeclaration/correctResponse/value", xmlNode, XPathConstants.NODESET);
		for (int index = 0; index < xmlCorrectResponse.getLength(); index++) {
			Node xmlElement = xmlCorrectResponse.item(index);
			String id = (String) xpath.evaluate("./text()", xmlElement, XPathConstants.STRING);
			correctAnswerIds.add(id);
		}

		NodeList xmlMapping = (NodeList) xpath.evaluate("./responseDeclaration/mapping/mapEntry", xmlNode, XPathConstants.NODESET);
		if (xmlMapping.getLength() == 0) {
			
			String maxScore= (String) xpath.evaluate("./outcomeDeclaration[@identifier='MAXSCORE']/defaultValue/value/text()", xmlNode, XPathConstants.STRING);
			
			float avgScorePerAnswer = Float.valueOf(maxScore).floatValue();
			if (item instanceof Item_MC) avgScorePerAnswer /= answerIds.size();
			
			for (String id: answerIds) {
				if (correctAnswerIds.contains(id)) {
					answerPointsPos.put(id, String.valueOf(avgScorePerAnswer));
					answerPointsNeg.put(id, "0");
				} else {
					answerPointsPos.put(id, "0");
					answerPointsNeg.put(id, String.valueOf(avgScorePerAnswer));
				}
			}
			
		} else {
			for (int index = 0; index < xmlMapping.getLength(); index++) {
				Node xmlElement = xmlMapping.item(index);
				String id = (String) xpath.evaluate("@mapKey", xmlElement, XPathConstants.STRING);
				String points = (String) xpath.evaluate("@mappedValue", xmlElement, XPathConstants.STRING);
				String defaultValue = (String) xpath.evaluate("../@defaultValue", xmlElement, XPathConstants.STRING);
				answerPointsPos.put(id,  points);
				answerPointsNeg.put(id,  defaultValue);
			}
		}

		for (String id: answerIds) {
			item.addAnswer(answerText.get(id), answerPointsPos.get(id), answerPointsNeg.get(id));
		}
	}	
	
	
	
	public static Document createItemDoc(ItemWAnswers item, String itemIdentifier, String cardinality, String baseType, String responseProcessing) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		
		Document doc = Onyx_Item.createItemDoc(item, itemIdentifier, cardinality, baseType, responseProcessing);
		XPath xpath = XPathFactory.newInstance().newXPath();
		
		// correct responses als value-Elements in correctResponse
		Element correctResponse = (Element) xpath.evaluate("./responseDeclaration/correctResponse", doc.getDocumentElement(), XPathConstants.NODE);

		for (int index=0; index<item.getNumberOfAnswers(); index++) {
			if (item.getAnswerPoints(index, true) >= item.getAnswerPoints(index, false)) {
				Element value = doc.createElement("value");
				value.setTextContent(""+index);
				correctResponse.appendChild(value);
				
			}
		}
		
	/*	
		
		<choiceInteraction responseIdentifier="RESPONSE_1" shuffle="true" maxChoices="0">
		<simpleChoice identifier="idfca61fc8-3cc4-4312-9bd3-539a32063bbf">
		<p>Neue Antwortmöglichkeit korrekt</p>
		</simpleChoice>
		<simpleChoice identifier="id60779c57-039f-46e9-b62d-d32b8a03d76c">
		<p>Neue Antwortmöglichkeit falsch</p>
		</simpleChoice>
		<simpleChoice identifier="id47272899-690b-4929-9a05-4907c85089c0">
		<p>Neue Antwortmöglichkeit korrekt</p>
		</simpleChoice>
		</choiceInteraction>
*/		
		
		Element choiceInteraction = doc.createElement("choiceInteraction");
		choiceInteraction.setAttribute("responseIdentifier", "RESPONSE_1");
		choiceInteraction.setAttribute("shuffle", "true");
		choiceInteraction.setAttribute("maxChoices", "0");
		
		for (int index=0; index<item.getNumberOfAnswers(); index++) {
		
			org.jsoup.nodes.Document html = Jsoup.parseBodyFragment(item.getAnswerText(index));
			html.outputSettings().syntax(org.jsoup.nodes.Document.OutputSettings.Syntax.xml);    
			
			Element simpleChoice = doc.createElement("simpleChoice");
			simpleChoice.setAttribute("identifier", String.valueOf(index));
			simpleChoice.setTextContent(html.body().html());
			choiceInteraction.appendChild(simpleChoice);
		}
		
		Element itemBody = (Element) xpath.evaluate("./itemBody", doc.getDocumentElement(), XPathConstants.NODE);
		itemBody.appendChild(choiceInteraction);

		
		return doc;
	}

	


}
