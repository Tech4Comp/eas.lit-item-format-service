package eal.item.format.onyx;

import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import eal.item.format.ilias.Ilias_Item;
import eal.item.type.Item;
import eal.item.type.Item_FT;
import eal.item.type.Item_MC;
import eal.item.type.Item_OR;
import eal.item.type.Item_ParseException;
import eal.item.type.Item_SC;

public abstract class Onyx_Item {

	
	public static Item parseItemElement (Element xmlItem) {

		try {
			XPath xpath = XPathFactory.newInstance().newXPath();
			String cardinality = (String) xpath.evaluate("/assessmentItem/responseDeclaration/@cardinality", xmlItem, XPathConstants.STRING);
			String baseType = (String) xpath.evaluate("/assessmentItem/responseDeclaration/@baseType", xmlItem, XPathConstants.STRING);

			String[] type = new String[] {cardinality, baseType};
			
			if (Arrays.equals(type, new String[] {Onyx_Item_SC.cardinality, Onyx_Item_SC.baseType})) return Onyx_Item_SC.parseItemElement(xmlItem);
			if (Arrays.equals(type, new String[] {Onyx_Item_MC.cardinality, Onyx_Item_MC.baseType})) return Onyx_Item_MC.parseItemElement(xmlItem);
			if (Arrays.equals(type, new String[] {Onyx_Item_FT.cardinality, Onyx_Item_FT.baseType})) return Onyx_Item_FT.parseItemElement(xmlItem);
			if (Arrays.equals(type, new String[] {Onyx_Item_OR.cardinality, Onyx_Item_OR.baseType})) return Onyx_Item_OR.parseItemElement(xmlItem);

			return new Item_ParseException("Onyx_Item.parseItemElement: Unknown type " + Arrays.toString(type));
			
		} catch (XPathExpressionException e) {
			return new Item_ParseException("Onyx_Item.parseItemElement: " + e.getMessage());
		}
	}
	
	
	static void parseItemElement (Element xmlNode, Item item) throws XPathExpressionException {

		XPath xpath = XPathFactory.newInstance().newXPath();

		// get task/question == first child of itemBody element and parse question, description, and annotation 
		StringBuffer itemBody = new StringBuffer();


		NodeList itemBodyElements = (NodeList) xpath.evaluate("./itemBody/*[name()!='choiceInteraction' and name()!='extendedTextInteraction']", xmlNode, XPathConstants.NODESET);
		for (int index=0; index<itemBodyElements.getLength(); index++) {
			itemBody.append(Onyx_Import.nodeToString(itemBodyElements.item(index)));
		}
		Ilias_Item.parseHtmlQuestion(itemBody.toString(), item);
		
		// get title
		String s = (String) xpath.evaluate("./@title", xmlNode, XPathConstants.STRING);
		item.setTitle(s);

	}

	
	protected static Document createItemDoc(Item item, String itemIdentifier) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		
		if (item instanceof Item_SC) return Onyx_Item_SC.createItemDoc((Item_SC) item, itemIdentifier);
		if (item instanceof Item_MC) return Onyx_Item_MC.createItemDoc((Item_MC) item, itemIdentifier);
		if (item instanceof Item_FT) return Onyx_Item_FT.createItemDoc((Item_FT) item, itemIdentifier);
		if (item instanceof Item_OR) return Onyx_Item_OR.createItemDoc((Item_OR) item, itemIdentifier);
		return null;
	}
	
	protected static Document createItemDoc(Item item, String itemIdentifier, String cardinality, String baseType, String responseProcessing) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
// <![CDATA[%1$s]]>
		String xml = String.format (
			"<assessmentItem xmlns=\"http://www.imsglobal.org/xsd/imsqti_v2p1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.imsglobal.org/xsd/imsqti_v2p1 http://www.imsglobal.org/xsd/qti/qtiv2p1/imsqti_v2p1p1.xsd http://www.w3.org/1998/Math/MathML http://www.w3.org/Math/XMLSchema/mathml2/mathml2.xsd\" adaptive=\"false\" timeDependent=\"false\"><responseDeclaration identifier=\"RESPONSE_1\"><correctResponse></correctResponse></responseDeclaration><outcomeDeclaration identifier=\"MAXCHOICES\" cardinality=\"single\" baseType=\"float\"><defaultValue><value>1</value></defaultValue></outcomeDeclaration><outcomeDeclaration identifier=\"SCORE\" cardinality=\"single\" baseType=\"float\"><defaultValue><value>0</value></defaultValue></outcomeDeclaration><outcomeDeclaration identifier=\"MAXSCORE\" cardinality=\"single\" baseType=\"float\"><defaultValue><value></value></defaultValue></outcomeDeclaration><outcomeDeclaration identifier=\"MINSCORE\" cardinality=\"single\" baseType=\"float\" view=\"testConstructor\"><defaultValue><value>0</value></defaultValue></outcomeDeclaration><itemBody>%1$s</itemBody>%2$s</assessmentItem>", 
			Ilias_Item.createHtmlQuestion(item.getDescription(), item.getQuestion(), new JSONObject()),
			responseProcessing
		);

		System.out.println("Item with id = " + item.getEalid());
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = builder.parse(new InputSource(new StringReader(xml)));
		XPath xpath = XPathFactory.newInstance().newXPath();

		// title und identifier als Attribute von assessmentItem
		Element assessmentItem = doc.getDocumentElement();
		assessmentItem.setAttribute("title", item.getTitle());
		assessmentItem.setAttribute("identifier", itemIdentifier);
		
		// cardinality und baseType als Attribute von responseDeclaration
		Element responseDeclaration = (Element) xpath.evaluate("./responseDeclaration", assessmentItem, XPathConstants.NODE);
		responseDeclaration.setAttribute("cardinality", cardinality);
		responseDeclaration.setAttribute("baseType", baseType);
		
		// score
		Element value = (Element) xpath.evaluate("./outcomeDeclaration[@identifier=\"MAXSCORE\"]/defaultValue/value", assessmentItem, XPathConstants.NODE);
		value.setTextContent(String.valueOf(item.getPoints()));
		
		return doc;
	}
	
	
	protected static Document createManifestDoc(Item item, String itemIdentifier) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
	
		if (item instanceof Item_SC) return Onyx_Item_SC.createManifestDoc((Item_SC) item, itemIdentifier);
		if (item instanceof Item_MC) return Onyx_Item_MC.createManifestDoc((Item_MC) item, itemIdentifier);
		if (item instanceof Item_FT) return Onyx_Item_FT.createManifestDoc((Item_FT) item, itemIdentifier);
		if (item instanceof Item_OR) return Onyx_Item_OR.createManifestDoc((Item_OR) item, itemIdentifier);
		return null;
	}
	
	protected static Document createManifestDoc(Item item, String itemIdentifier, String interactionType) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		
	
		Date d = new Date();
		String xml = String.format (
				"<manifest xmlns=\"http://www.imsglobal.org/xsd/imscp_v1p1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.imsglobal.org/xsd/imscp_v1p1 http://www.imsglobal.org/xsd/qti/qtiv2p1/qtiv2p1_imscpv1p2_v1p0.xsd http://www.imsglobal.org/xsd/imsqti_v2p1 http://www.imsglobal.org/xsd/qti/qtiv2p1/imsqti_v2p1p1.xsd http://www.imsglobal.org/xsd/imsqti_metadata_v2p1 http://www.imsglobal.org/xsd/qti/qtiv2p1/imsqti_metadata_v2p1p1.xsd http://ltsc.ieee.org/xsd/LOM http://www.imsglobal.org/xsd/imsmd_loose_v1p3p2.xsd\" identifier=\"manifestID\"><metadata><schema>QTIv2.1 Package</schema><schemaversion>1.0.0</schemaversion></metadata><organizations/><resources><resource identifier=\"%2$s\" type=\"imsqti_item_xmlv2p1\" href=\"%1$s.xml\"> <metadata> <lom xmlns=\"http://ltsc.ieee.org/xsd/LOM\"> <general> <identifier> <entry>%1$s</entry> </identifier> <title> <string language=\"de\"></string> </title> <description> <string language=\"de\"/> </description> </general> <lifeCycle> <contribute> <date> <dateTime>%3$s</dateTime> </date> <role> <source>LOMv1.0</source> <value>author</value> </role> <entity> BEGIN:VCARD VERSION:4.0 N;LANGUAGE=de:Thor;Andreas;;; FN:Andreas Thor EMAIL:andreas.thor@htwk-leipzig.de PRODID:ONYX Editor 8.9.0.1 END:VCARD </entity> </contribute> </lifeCycle> <educational> <learningResourceType> <source>QTIv2.1</source> <value>AssessmentItem</value> </learningResourceType> </educational> <qtiMetadata xmlns=\"http://www.imsglobal.org/xsd/imsqti_metadata_v2p1\"> <interactionType>%4$s</interactionType> </qtiMetadata> </lom> </metadata> <file href=\"%1$s.xml\"/> </resource></resources></manifest>", 	
			itemIdentifier,
			itemIdentifier.replaceAll("-", "_"),
			new SimpleDateFormat("yyyy-MM-dd").format(d) + "T" + new SimpleDateFormat("HH:mm:ss").format(d),
			interactionType
		);
		
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = builder.parse(new InputSource(new StringReader(xml)));
		
		XPath xpath = XPathFactory.newInstance().newXPath();

		NodeList title = (NodeList) xpath.evaluate("/manifest/resources/resource/metadata/lom/general/title/string", doc.getDocumentElement(), XPathConstants.NODESET);
		if (title.getLength()>0) {
			title.item(0).setTextContent(item.getTitle());
		}
		
		return doc;		
	}
	
	
}
