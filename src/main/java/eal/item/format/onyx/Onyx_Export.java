package eal.item.format.onyx;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.UUID;
import java.util.zip.ZipOutputStream;

import org.w3c.dom.Document;

import eal.item.format.Exporter;
import eal.item.type.Item;

public class Onyx_Export extends Exporter {

	@Override
	public int create(Item[] items, OutputStream out) throws Exception {

		int noOfCreatedItems = 0;
		ZipOutputStream zip = new ZipOutputStream(out);

		for (Item item: items) {
			String itemIdentifier = UUID.randomUUID().toString();

			Document maniDoc = Onyx_Item.createManifestDoc(item, itemIdentifier);
			if (maniDoc==null) continue;
			
			Document itemDoc = Onyx_Item.createItemDoc(item, itemIdentifier);
			if (itemDoc==null) continue;

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ZipOutputStream zipItem = new ZipOutputStream(bos);
			addFileToZip(zipItem, "imsmanifest.xml", getStreamFromDocument(maniDoc));
			addFileToZip(zipItem, itemIdentifier + ".xml", getStreamFromDocument(itemDoc));
			zipItem.close();

			addFileToZip(zip, noOfCreatedItems + ".zip", new ByteArrayInputStream(bos.toByteArray()));
			noOfCreatedItems++;
		}

		zip.close();
		return noOfCreatedItems;
	}

}
