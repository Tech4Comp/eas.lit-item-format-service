package eal.item.format.onyx;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import eal.item.type.Item_SC;

public class Onyx_Item_SC {

	public final static String interactionType = "choiceInteraction";
	public final static String cardinality = "single"; 
	public final static String baseType = "identifier";
	
	public static Item_SC parseItemElement (Element xmlNode) throws XPathExpressionException {

		Item_SC item = new Item_SC();
		Onyx_ItemWAnswers.parseItemElement(xmlNode, item);
		return item;
	}

	public static Document createManifestDoc(Item_SC item, String itemIdentifier) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		return Onyx_Item.createManifestDoc(item, itemIdentifier, interactionType);
	}

	public static Document createItemDoc(Item_SC item, String itemIdentifier) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
		
		String responseProcessing = "<responseProcessing> <responseCondition> <responseIf> <isNull> <variable identifier=\"RESPONSE_1\"/> </isNull> </responseIf> <responseElseIf> <match> <variable identifier=\"RESPONSE_1\"/> <correct identifier=\"RESPONSE_1\"/> </match> <setOutcomeValue identifier=\"SCORE\"> <sum> <variable identifier=\"SCORE\"/> <variable identifier=\"MAXSCORE\"/> </sum> </setOutcomeValue> </responseElseIf> </responseCondition> <responseCondition> <responseIf> <gt> <variable identifier=\"SCORE\"/> <variable identifier=\"MAXSCORE\"/> </gt> <setOutcomeValue identifier=\"SCORE\"> <variable identifier=\"MAXSCORE\"/> </setOutcomeValue> </responseIf> </responseCondition> <responseCondition> <responseIf> <lt> <variable identifier=\"SCORE\"/> <variable identifier=\"MINSCORE\"/> </lt> <setOutcomeValue identifier=\"SCORE\"> <variable identifier=\"MINSCORE\"/> </setOutcomeValue> </responseIf> </responseCondition> </responseProcessing>";
		
		return Onyx_ItemWAnswers.createItemDoc(item, itemIdentifier, cardinality, baseType, responseProcessing);
	}


}
