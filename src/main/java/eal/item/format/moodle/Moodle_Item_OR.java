package eal.item.format.moodle;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import eal.item.type.Item_OR;


public class Moodle_Item_OR  {


	


	public static Item_OR parseItemElement (Element xmlNode) throws XPathExpressionException {
	
		return null;
	}
	
	
	public static void createItemElement(Item_OR item, Document doc, Element question)	throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {

		Element defaultgrade = doc.createElement("defaultgrade");
		defaultgrade.setTextContent(new DecimalFormat("#.#####", new DecimalFormatSymbols(Locale.US)).format(item.getPoints()));
		question.appendChild(defaultgrade);

		Element penalty = doc.createElement("penalty");
		penalty.setTextContent(new DecimalFormat("#.#####", new DecimalFormatSymbols(Locale.US)).format(1f/item.getPoints()));
		question.appendChild(penalty);
		
		for (int index=0; index<item.getNumberOfAnswers(); index++) {
			Element subquestion = doc.createElement("subquestion");
			subquestion.setAttribute("format", "html");	
			Element text  = doc.createElement("text");
			text.appendChild(doc.createCDATASection(item.getAnswerText(index)));
			subquestion.appendChild(text);

			Element answer = doc.createElement("answer");
			Element text2 = doc.createElement("text");
			text2.setTextContent(String.valueOf(index+1));
			answer.appendChild(text2);
			subquestion.appendChild(answer);

			question.appendChild(subquestion);
		}


	}
	
}
