package eal.item.format.moodle;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import eal.item.type.Item_FT;

public class Moodle_Item_FT  {


	public static Item_FT parseItemElement (Element xmlNode) throws XPathExpressionException {

		Item_FT item = new Item_FT();

		return item;
	}



	
	public static void createItemElement(Item_FT item, Document doc, Element question)	throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {

		Element answer = doc.createElement("answer");
		answer.setAttribute("fraction", "0");
		answer.appendChild(doc.createElement("text"));
		question.appendChild(answer);
	}

}
