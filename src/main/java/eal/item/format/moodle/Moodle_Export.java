package eal.item.format.moodle;

import java.io.OutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import eal.item.format.Exporter;
import eal.item.type.Item;

public class Moodle_Export extends Exporter {

	public Moodle_Export() {
	}

	@Override
	public int create(Item[] items, OutputStream out) throws Exception {

		int noOfCreatedItems = 0;

		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document quizDoc = builder.newDocument();
		quizDoc.appendChild(quizDoc.createElement("quiz"));

		for (int itemIndex = 0; itemIndex < items.length; itemIndex++) {
			Element e = Moodle_Item.createItemElement(items[itemIndex], quizDoc);
			if (e==null) continue;

			quizDoc.getDocumentElement().appendChild(e);
			noOfCreatedItems++;
		}
		
		getStreamFromDocument(quizDoc).transferTo(out);
		return noOfCreatedItems;
	}
	
}
