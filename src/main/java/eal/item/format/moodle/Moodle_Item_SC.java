package eal.item.format.moodle;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import eal.item.type.Item_SC;

public class Moodle_Item_SC {


	public static Item_SC parseItemElement (Element xmlNode) throws XPathExpressionException {

		Item_SC item = new Item_SC();

		return item;
	}

	
	public static void createItemElement(Item_SC item, Document doc, Element question)	throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {

		// Locale.US to enforce decimal points
		Element defaultgrade = doc.createElement("defaultgrade");
		defaultgrade.setTextContent(new DecimalFormat("#.#####", new DecimalFormatSymbols(Locale.US)).format(item.getPoints()));
		question.appendChild(defaultgrade);

		Element single = doc.createElement("single");
		single.setTextContent("true");
		question.appendChild(single);

		Element shuffleanswers = doc.createElement("shuffleanswers");
		shuffleanswers.setTextContent("1");
		question.appendChild(shuffleanswers);

		Element answernumbering = doc.createElement("answernumbering");
		answernumbering.setTextContent("abc");
		question.appendChild(answernumbering);


		for (int index=0; index<item.getNumberOfAnswers(); index++) {
			float fraction = item.getAnswerPoints(index)*100.0f/item.getPoints();
			Element answer  = doc.createElement("answer");
			// answer.setAttribute("fraction", String.format(Locale.US, "%.##f", fraction));	
			answer.setAttribute("fraction", new DecimalFormat("#.#####", new DecimalFormatSymbols(Locale.US)).format(fraction));	

			Element text  = doc.createElement("text");
			text.appendChild(doc.createCDATASection(item.getAnswerText(index)));
			answer.appendChild(text);
			question.appendChild(answer);
		}

	}

	

}
