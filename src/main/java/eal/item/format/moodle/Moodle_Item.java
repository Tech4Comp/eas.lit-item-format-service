package eal.item.format.moodle;

import java.io.IOException;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import eal.item.format.json.Json_Item;
import eal.item.type.Item;
import eal.item.type.Item_FT;
import eal.item.type.Item_MC;
import eal.item.type.Item_OR;
import eal.item.type.Item_SC;

public abstract class Moodle_Item {

	public static String DESCRIPTION_QUESTION_SEPARATOR = " EAL ";
	private static String ANNOTATION_PREFIX = "EAL_ANNOTATION:";
	

	
	public static Item parseItemElement(Element xmlItem) {

		return null;
	}	 
	

	static void parseItemElement (Element xmlNode, Item item) throws XPathExpressionException {

	}


	/**
	 * Parse HTML question
	 * - may contain JSON data within an HTML comment 
	 * - may be separated into (vignette) description and actual question 
	 * @param htmlQuestion
	 * @param item
	 */
	public static void parseHtmlQuestion (String htmlQuestion, Item item) {
	}
	
	
	public static Element createItemElement(Item item, Document doc) throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {

		Element question = doc.createElement("question");

		// set Question Type; if not available --> exit
		String questionType = null;
		if (item instanceof Item_SC) questionType = "multichoice";
		if (item instanceof Item_MC) questionType = "multichoice";
		if (item instanceof Item_FT) questionType = "essay";
		if (item instanceof Item_OR) questionType = "order";	// TODO: korrekt?
		if (questionType == null) return null;
		question.setAttribute("type", questionType);

		// Item Title
		Element name = doc.createElement("name");
		Element text2 = doc.createElement("text");
		text2.setTextContent(item.getTitle());
		name.appendChild(text2);
		question.appendChild(name);

		// Item Description & Question + metadata as JSON comment
		Element questionText = doc.createElement("questiontext");
		questionText.setAttribute("format", "html");
		Element text = doc.createElement("text");
		text.appendChild(doc.createCDATASection(createHtmlQuestion(item.getDescription(), item.getQuestion(), Json_Item.toJSON_AnnotationOnly(item))));
		questionText.appendChild(text);
		question.appendChild(questionText);

		if (item instanceof Item_SC) Moodle_Item_SC.createItemElement((Item_SC) item, doc, question);
		if (item instanceof Item_MC) Moodle_Item_MC.createItemElement((Item_MC) item, doc, question);
		if (item instanceof Item_FT) Moodle_Item_FT.createItemElement((Item_FT) item, doc, question);
		if (item instanceof Item_OR) Moodle_Item_OR.createItemElement((Item_OR) item, doc, question);
		
		return question;
	}

	

	public static String createHtmlQuestion(String description, String question, JSONObject annotation) {
		
		org.jsoup.nodes.Document html = Jsoup.parseBodyFragment("");
		html.body().appendChild(new Comment(ANNOTATION_PREFIX + annotation.toString()));
		html.body().append(Optional.ofNullable(description).orElse(""));
		html.body().appendChild(new Comment(DESCRIPTION_QUESTION_SEPARATOR));
		html.body().append(Optional.ofNullable(question).orElse(""));
		html.outputSettings().syntax(org.jsoup.nodes.Document.OutputSettings.Syntax.xml);    
		return html.body().html();
	}

	


}
