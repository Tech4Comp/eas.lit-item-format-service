package eal.item.format.moodle;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import eal.item.type.Item_MC;


public class Moodle_Item_MC {


	public static Item_MC parseItemElement (Element xmlNode) throws XPathExpressionException {

		Item_MC item = new Item_MC();

		
		return item;
	
	}
	

	public static void createItemElement(Item_MC item, Document doc, Element question) throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {
		
		Element defaultgrade = doc.createElement("defaultgrade");
		defaultgrade.setTextContent(new DecimalFormat("#.#####", new DecimalFormatSymbols(Locale.US)).format(item.getPoints()));
		question.appendChild(defaultgrade);

		Element single = doc.createElement("single");
		single.setTextContent("false");
		question.appendChild(single);

		Element shuffleanswers = doc.createElement("shuffleanswers");
		shuffleanswers.setTextContent("1");
		question.appendChild(shuffleanswers);

		Element answernumbering = doc.createElement("answernumbering");
		answernumbering.setTextContent("abc");
		question.appendChild(answernumbering);

		float sumPositive = 0;
		for (int index=0; index<item.getNumberOfAnswers(); index++) {
			if (item.getAnswerPoints(index, true)>item.getAnswerPoints(index, false)) {
				sumPositive += item.getAnswerPoints(index, true)-item.getAnswerPoints(index, false);
			}
		}

		for (int index=0; index<item.getNumberOfAnswers(); index++) {
			float fraction = (item.getAnswerPoints(index, true)-item.getAnswerPoints(index, false))/sumPositive*100;
			Element answer  = doc.createElement("answer");
			answer.setAttribute("fraction", new DecimalFormat("#.#####", new DecimalFormatSymbols(Locale.US)).format(fraction));	
			Element text  = doc.createElement("text");
			text.appendChild(doc.createCDATASection(item.getAnswerText(index)));
			answer.appendChild(text);
			question.appendChild(answer);
		}



	}

	
	
}
