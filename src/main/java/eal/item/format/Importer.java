package eal.item.format;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import eal.item.type.Item;

public abstract class Importer {

	
	public class ImporterInitException extends Exception { 
		private static final long serialVersionUID = 1L;
		public ImporterInitException(String errorMessage, Throwable err) { super(errorMessage, err); }
	}
	
	
	public class ParseException extends Exception { 
		private static final long serialVersionUID = 1L;
		public ParseException(String errorMessage, Throwable err) { super(errorMessage, err); }
	}	
	
	
	public abstract Item[] parse (InputStream in) throws ParseException;

	
	protected static Map<String, byte[]> getZipContent(InputStream in) throws IOException {

		Map<String, byte[]> result = new HashMap<String, byte[]>();
		readZip(in, "", result);
		return result;
	}		
	
	
	
	private static void readZip (InputStream in, String dirPrefix, Map<String, byte[]> content) throws IOException {
		
		// create a buffer to improve copy performance later.
		byte[] buffer = new byte[2048];

		ZipInputStream stream = new ZipInputStream(in, StandardCharsets.UTF_8);
		ZipEntry entry;
		
		
		while ((entry = stream.getNextEntry()) != null) {

			// get the entry's content
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			int len = 0;
			while ((len = stream.read(buffer)) > 0) {
				os.write(buffer, 0, len);
			}
			byte[] entryContent = os.toByteArray();
			
			// check, if content is zipped (file extension = .zip & file content is zip file) --> if yes: recursion
			try {
				if (entry.getName().endsWith(".zip")) {	// we check for extension because xlsx-files are also zip files but should not be unzipped here
					ZipInputStream entryStream = new ZipInputStream(new ByteArrayInputStream(entryContent), StandardCharsets.UTF_8);
					if (entryStream.getNextEntry() != null) {
						readZip (new ByteArrayInputStream(entryContent), dirPrefix + entry.getName() + "/", content);
						continue;
					}
				}
			} catch (IOException e) { }
			
			// not zipped --> add to result
			content.put(dirPrefix + entry.getName(), os.toByteArray());
		}
		stream.close();		
		
	}
	
	
}
