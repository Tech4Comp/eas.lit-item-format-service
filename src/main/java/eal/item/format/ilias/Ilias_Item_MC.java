package eal.item.format.ilias;

import java.io.IOException;
import java.util.function.IntFunction;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import eal.item.type.ItemWAnswers;
import eal.item.type.Item_MC;


public class Ilias_Item_MC {

	public final static String type = "MULTIPLE CHOICE QUESTION";


	public static Item_MC parseItemElement (Element xmlNode) throws XPathExpressionException {

		Item_MC item = new Item_MC();
		Ilias_ItemWAnswers.parseItemElement(xmlNode, item);

		XPath xpath = XPathFactory.newInstance().newXPath();
		String min = (String) xpath.evaluate("./presentation/flow/response_lid/render_choice/@minnumber", xmlNode, XPathConstants.STRING);
		String max = (String) xpath.evaluate("./presentation/flow/response_lid/render_choice/@maxnumber", xmlNode, XPathConstants.STRING);
		item.setMinMaxNumber(min, max);
		
		return item;
	
	}
	
	public static Element createItemElement(Item_MC item, Document doc, String xmlIdent, IntFunction<String> imgReference, IntFunction<String> imgLabel) throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {
		
		Element result = Ilias_Item.createItemElement(item, Ilias_Item_MC.type, doc, xmlIdent, imgReference, imgLabel);
		
		XPath xpath = XPathFactory.newInstance().newXPath();

		Node flow = (Node) xpath.evaluate("./presentation/flow", result, XPathConstants.NODE);
		flow.appendChild(createResponseElement(doc, xmlIdent, item, "Multiple"));

		
		Element render_choice = (Element) xpath.evaluate("./presentation/flow/response_lid/render_choice", result, XPathConstants.NODE);
		render_choice.setAttribute("minnumber", String.valueOf(item.getMinNumber()));
		render_choice.setAttribute("maxnumber", String.valueOf(item.getMaxNumber()));
		
		result.appendChild(createResprocessingElement(doc, xmlIdent, item, true));
		
		return result;
		
	}

	
	public static Element createResprocessingElement (Document doc, String xmlIdent, ItemWAnswers itemWAnsers, boolean includeNOT) {
		
		Element resprocessing = doc.createElement("resprocessing");
		
		Element outcomes = doc.createElement("outcomes");
		Element decvar = doc.createElement("decvar");
		outcomes.appendChild (decvar);
		resprocessing.appendChild(outcomes);
		
		for (int index=0; index<itemWAnsers.getNumberOfAnswers(); index++) {
		
			for (boolean checked: (includeNOT ? new boolean[]{true, false} : new boolean[] {true})) {
				
				Element respcondition = doc.createElement("respcondition");
				respcondition.setAttribute ("continue", "Yes");
				
				Element conditionvar = doc.createElement("conditionvar");
				Element not = doc.createElement("not");
				Element varequal = doc.createElement("varequal");
				varequal.setTextContent(String.valueOf(index));
				varequal.setAttribute ("respident", xmlIdent);
				varequal.setAttribute ("index", String.valueOf(index));			

				
				if (checked) {
					conditionvar.appendChild (varequal);
				} else {
					not.appendChild (varequal);
					conditionvar.appendChild (not);
				}
				respcondition.appendChild (conditionvar);
				
				Element setvar = doc.createElement("setvar");
				// setvar.setTextContent( String.valueOf (itemWAnsers.getAnswerPoints(index, checked)));
				setvar.setTextContent( String.format("%.2f", itemWAnsers.getAnswerPoints(index, checked)));
				
				setvar.setAttribute("action", "Add");
				
				respcondition.appendChild (setvar);
				resprocessing.appendChild (respcondition);
				
			}
			
		}
		
		return resprocessing;	
	}


	public static Element createResponseElement(Document doc, String xmlIdent, ItemWAnswers itemWAnsers, String rcardinality) {
		
		Element render_choice = doc.createElement("render_choice");
		render_choice.setAttribute("shuffle", "Yes");

		
		for (int index=0; index<itemWAnsers.getNumberOfAnswers(); index++) {
			
			Element mattext = doc.createElement("mattext");
			mattext.setTextContent(itemWAnsers.getAnswerText(index));
			mattext.setAttribute("texttype", "text/html");

			Element material = doc.createElement("material");
			material.appendChild(mattext);
			
			Element response_label = doc.createElement("response_label");
			response_label.setAttribute("ident", String.valueOf(index));
			response_label.appendChild(material);
			
			render_choice.appendChild (response_label);
		}

		Element response_lid = doc.createElement("response_lid");
		response_lid.setAttribute("ident", xmlIdent);
		response_lid.setAttribute("rcardinality", rcardinality);
		response_lid.appendChild (render_choice);
		return response_lid;		
	
		
	}

	
}
