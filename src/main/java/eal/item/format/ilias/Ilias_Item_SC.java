package eal.item.format.ilias;

import java.io.IOException;
import java.util.function.IntFunction;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import eal.item.type.Item_SC;

public class Ilias_Item_SC {

	public final static String type = "SINGLE CHOICE QUESTION";

	
	public static Element createItemElement(Item_SC item, Document doc, String xmlIdent, IntFunction<String> imgReference, IntFunction<String> imgLabel)	throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {

		Element result = Ilias_Item.createItemElement(item, Ilias_Item_SC.type, doc, xmlIdent, imgReference, imgLabel);

		XPath xpath = XPathFactory.newInstance().newXPath();

		Node flow = (Node) xpath.evaluate("./presentation/flow", result, XPathConstants.NODE);
		flow.appendChild(Ilias_Item_MC.createResponseElement(doc, xmlIdent, item, "Single"));

		result.appendChild(Ilias_Item_MC.createResprocessingElement(doc, xmlIdent, item, true));

		return result;
	}

	
	// TODO: eigenes parsing
	public static Item_SC parseItemElement (Element xmlNode) throws XPathExpressionException {

		Item_SC item = new Item_SC();
		Ilias_ItemWAnswers.parseItemElement(xmlNode, item);
		return item;
	}
}
