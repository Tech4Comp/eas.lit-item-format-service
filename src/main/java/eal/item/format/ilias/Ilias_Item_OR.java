package eal.item.format.ilias;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.IntFunction;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eal.item.type.ItemWAnswers;
import eal.item.type.Item_OR;


public class Ilias_Item_OR  {

	public final static String type_VERTCIAL = "ORDERING QUESTION";
	public final static String type_HORIZONTAL = "assOrderingHorizontal";


	


	public static Item_OR parseItemElement (Element xmlNode, Item_OR.Direction direction) throws XPathExpressionException {

		Item_OR item = new Item_OR();
		item.setDirection(direction);
		Ilias_Item.parseItemElement(xmlNode, item);

		XPath xpath = XPathFactory.newInstance().newXPath();

		// TODO: answerdepth?
		List<String> answerText = new ArrayList<String>();

		if (direction == Item_OR.Direction.VERTICAL) {
			NodeList xmlLabels = (NodeList) xpath.evaluate("./presentation/flow//response_label", xmlNode, XPathConstants.NODESET);
			for (int index = 0; index < xmlLabels.getLength(); index++) {
				Node xmlLabel = xmlLabels.item(index);
				String text = (String) xpath.evaluate("./material/material/mattext/text()", xmlLabel, XPathConstants.STRING);
				answerText.add(text);
			}

			NodeList xmlResps = (NodeList) xpath.evaluate("./resprocessing/respcondition", xmlNode, XPathConstants.NODESET);
			for (int index = 0; index < xmlResps.getLength(); index++) {
				Node xmlResp = xmlResps.item(index);
	
				String points = (String) xpath.evaluate("./setvar[@action=\"Add\"]/text()", xmlResp, XPathConstants.STRING);
				int idIndex = ((Double) xpath.evaluate("./conditionvar/varequal/text()", xmlResp, XPathConstants.NUMBER)).intValue();
				item.addAnswer(answerText.get(idIndex), points, null);
			}

		} else {	// Horizontal: All answers are in a splittable string
			String text = (String) xpath.evaluate(".//qtimetadatafield[./fieldlabel='ordertext']/fieldentry", xmlNode, XPathConstants.STRING);
			String split = (String) xpath.evaluate(".//qtimetadatafield[./fieldlabel='separator']/fieldentry", xmlNode, XPathConstants.STRING);
			if (!text.contains(split)) split = " ";	// default spliterator is white space but ILIAS puts :: in XML file usually
			String[] answers = text.split(split);
			
			String points = (String) xpath.evaluate(".//qtimetadatafield[./fieldlabel='points']/fieldentry", xmlNode, XPathConstants.STRING);
			float overallPoints = Float.valueOf(points).floatValue();

			for (String a: answers) {
				item.addAnswer(a, overallPoints/answers.length);
			}
		}
		
		return item;
	}
	
	
	public static Element createItemElement(Item_OR item, Document doc, String xmlIdent, IntFunction<String> imgReference, IntFunction<String> imgLabel)	throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {

		Element result;


		if (item.getDirection()==Item_OR.Direction.VERTICAL) {

			result = Ilias_Item.createItemElement(item, Ilias_Item_OR.type_VERTCIAL, doc, xmlIdent, imgReference, imgLabel);
			Element response_lid = createResponseElement(doc, "OQT", item, "Ordered");
			response_lid.setAttribute("output", "javascript");

			XPath xpath = XPathFactory.newInstance().newXPath();
			Node flow = (Node) xpath.evaluate("./presentation/flow", result, XPathConstants.NODE);
			flow.appendChild(response_lid);
			
			result.appendChild(Ilias_Item_MC.createResprocessingElement(doc, "OQT", item, false));

		} else {	// HORIZONTAL

			String orderText = "";
			for (int i=0; i<item.getNumberOfAnswers(); i++) {
				if (i>0) orderText += "::";
				orderText += item.getAnswerText(i);
			}

			Map<String, String> addMetaData = new HashMap<String, String>();
			addMetaData.put("points", String.valueOf(Math.round(item.getPoints())));
			addMetaData.put("ordertext", orderText);
			addMetaData.put("separator", "::");
			addMetaData.put("textsize", "");
			addMetaData.put("feedback_setting", null);

			result = Ilias_Item.createItemElement(item, Ilias_Item_OR.type_HORIZONTAL, doc, xmlIdent, addMetaData ,imgReference, imgLabel);

			for (int index=0; index<item.getNumberOfAnswers(); index++) {
				result.appendChild(createItemFeedbackElement(doc, index));
			}

			
		}

		
		/* ident values for odering questions (OQ)
		 * OQP: OQ_PICTURES
		 * OQNP: OQ_NESTED_PICTURES
		 * OQT: OQ_TERMS
		 * OQNT: OQ_NESTED_TERMS
		 */
		
		return result;
		
	}
	
	public static Element createItemFeedbackElement (Document doc, int answerIndex) {

		Element mattext = doc.createElement("mattext");
		mattext.setAttribute("texttype", "text/plain");

		Element material = doc.createElement("material");
		material.appendChild(mattext);

		Element flow_mat = doc.createElement("flow_mat");
		flow_mat.appendChild(material);

		Element itemfeedback = doc.createElement("itemfeedback");
		itemfeedback.setAttribute("ident", "response_" + answerIndex);
		itemfeedback.setAttribute("view", "All");
		itemfeedback.appendChild(flow_mat);

		return itemfeedback;
	}


	public static Element createResponseElement(Document doc, String xmlIdent, ItemWAnswers itemWAnsers, String rcardinality) {
		
		Element render_choice = doc.createElement("render_choice");
		render_choice.setAttribute("shuffle", "Yes");

		
		for (int index=0; index<itemWAnsers.getNumberOfAnswers(); index++) {
			
			Element mattext = doc.createElement("mattext");
			mattext.setTextContent(itemWAnsers.getAnswerText(index));
			mattext.setAttribute("texttype", "text/plain");

			Element material2 = doc.createElement("material");
			material2.appendChild(mattext);

			Element mattext3 = doc.createElement("mattext");
			mattext3.setTextContent("0");
			mattext3.setAttribute("label", "answerdepth");

			Element material3 = doc.createElement("material");
			material3.appendChild(mattext3);
			
			Element material = doc.createElement("material");
			material.appendChild(material2);
			material.appendChild(material3);
			
			Element response_label = doc.createElement("response_label");
			response_label.setAttribute("ident", String.valueOf(index));
			response_label.appendChild(material);
			
			render_choice.appendChild (response_label);
		}

		Element response_lid = doc.createElement("response_lid");
		response_lid.setAttribute("ident", xmlIdent);
		response_lid.setAttribute("rcardinality", rcardinality);
		response_lid.appendChild (render_choice);
		return response_lid;		
	
		
	}
	
}
