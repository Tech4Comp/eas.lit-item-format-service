package eal.item.format.ilias;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.stream.IntStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import eal.item.format.Importer;
import eal.item.type.Item;

public class Ilias_Import extends Importer {

	private String name;
	private Map<String, byte[]> content;
	private DocumentBuilder builder;
	private XPath xpath;

	
	
	public Ilias_Import() throws ImporterInitException {

		try {
			this.builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			this.builder.setEntityResolver(new EntityResolver() {
				public InputSource resolveEntity(String publicId, String systemId) {
					return new InputSource(new ByteArrayInputStream(new byte[0]));
				}
			});
			this.xpath = XPathFactory.newInstance().newXPath();
		} catch (ParserConfigurationException e) {
			throw new ImporterInitException ("Ilias", e);
		}

	}

	@Override
	public Item[] parse(InputStream in) throws ParseException {
		
		NodeList items; 
		
		try {
			this.content = getZipContent(in);
			this.name = this.content.keySet().iterator().next().split("/")[0];
			
			Document QTI = getQTIDocument();
			
			// get all images and their labels ... 
			NodeList matimage = (NodeList) xpath.evaluate("//item/presentation/flow/material/matimage", QTI, XPathConstants.NODESET);
			for (int index=0; index<matimage.getLength(); index++) {
				Element img = (Element) matimage.item(index);
				String filename = name + "/" + img.getAttribute("uri");
				String label = img.getAttribute("label");
				// ... and replace filename by label in the content
				content.put(label, content.get(filename));
				content.remove(filename);
			}
			
			items = (NodeList) xpath.evaluate("//item", QTI, XPathConstants.NODESET);
			
		} catch (IOException | XPathExpressionException | SAXException e) {
			throw new ParseException("Ilias", e);
		}
		
		
		return IntStream.range(0, items.getLength())
			.mapToObj(index -> Ilias_Item.parseItemElement(((Element) items.item(index))))
			.peek(item -> item.parseImages(content))
			.toArray(Item[]::new);
	}
	
	


	private Document getQTIDocument() throws SAXException, IOException {

		for (String type : new String[] { "_qpl_", "_tst_" }) {
			if (this.name.contains(type)) {
				String nameQTIFile = this.name + "/" + this.name.replaceAll(type, "_qti_") + ".xml";
				return builder.parse(new ByteArrayInputStream(this.content.get(nameQTIFile)/*.toString().getBytes(StandardCharsets.UTF_8)*/));
			}
		}
		throw new IOException("Could not find qti XML file. File " + this.name + " is neither qpl nor tst!");
	}

	




	
}
