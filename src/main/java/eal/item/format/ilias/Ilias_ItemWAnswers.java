package eal.item.format.ilias;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eal.item.type.ItemWAnswers;

public abstract class Ilias_ItemWAnswers {


	
	
	
	public static void parseItemElement (Element xmlNode, ItemWAnswers item) throws XPathExpressionException {

		Ilias_Item.parseItemElement(xmlNode, item);

		List<String> answerIds = new ArrayList<String>();
		Map<String, String> answerText = new HashMap<String, String>();
		Map<String, String> answerPointsPos = new HashMap<String, String>();
		Map<String, String> answerPointsNeg = new HashMap<String, String>();

		XPath xpath = XPathFactory.newInstance().newXPath();

		NodeList xmlLabels = (NodeList) xpath.evaluate("./presentation/flow//response_label", xmlNode, XPathConstants.NODESET);
		for (int index = 0; index < xmlLabels.getLength(); index++) {
			Node xmlLabel = xmlLabels.item(index);
			String id = (String) xpath.evaluate("./@ident", xmlLabel, XPathConstants.STRING);
			String text = (String) xpath.evaluate("./material/mattext/text()", xmlLabel, XPathConstants.STRING);
			answerIds.add(id);
			answerText.put(id, text);
		}

		NodeList xmlResps = (NodeList) xpath.evaluate("./resprocessing/respcondition", xmlNode, XPathConstants.NODESET);
		for (int index = 0; index < xmlResps.getLength(); index++) {
			Node xmlResp = xmlResps.item(index);

			String points = (String) xpath.evaluate("./setvar[@action=\"Add\"]/text()", xmlResp, XPathConstants.STRING);
			
			String id = (String) xpath.evaluate("./conditionvar/varequal/text()", xmlResp, XPathConstants.STRING);
			if (answerIds.contains(id)) {
				answerPointsPos.put(id,  points);
			}
			
			id = (String) xpath.evaluate("./conditionvar/not/varequal/text()", xmlResp, XPathConstants.STRING);
			if (answerIds.contains(id)) {
				answerPointsNeg.put(id,  points);
			}
		}

		for (String id: answerIds) {
			item.addAnswer(answerText.get(id), answerPointsPos.get(id), answerPointsNeg.get(id));
		}
	}

	
	

	


}
