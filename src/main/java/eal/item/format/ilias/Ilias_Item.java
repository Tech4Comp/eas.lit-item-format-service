package eal.item.format.ilias;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;
import java.util.function.IntFunction;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.Entities.EscapeMode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import eal.item.format.json.Json_Item;
import eal.item.type.Item;
import eal.item.type.Item_FT;
import eal.item.type.Item_MA;
import eal.item.type.Item_MC;
import eal.item.type.Item_OR;
import eal.item.type.Item_ParseException;
import eal.item.type.Item_SC;

public abstract class Ilias_Item {

	public static String DESCRIPTION_QUESTION_SEPARATOR = " EAL ";
	private static String ANNOTATION_PREFIX = "EAL_ANNOTATION:";
	
	
	
	public static Item parseItemElement(Element xmlItem) {

		try {
			XPath xpath = XPathFactory.newInstance().newXPath();
			String type = (String) xpath.evaluate(".//qtimetadatafield[./fieldlabel='QUESTIONTYPE']/fieldentry", xmlItem, XPathConstants.STRING);

			switch (type) {
			case Ilias_Item_SC.type: return Ilias_Item_SC.parseItemElement(xmlItem);
			case Ilias_Item_MC.type: return Ilias_Item_MC.parseItemElement(xmlItem);
			case Ilias_Item_FT.type: return Ilias_Item_FT.parseItemElement(xmlItem);
			case Ilias_Item_OR.type_VERTCIAL: return Ilias_Item_OR.parseItemElement(xmlItem, Item_OR.Direction.VERTICAL);
			case Ilias_Item_OR.type_HORIZONTAL: return Ilias_Item_OR.parseItemElement(xmlItem, Item_OR.Direction.HORIZONTAL);
			case Ilias_Item_MA.type: return Ilias_Item_MA.parseItemElement(xmlItem);
			default: return new Item_ParseException("Ilias_Item.parseItemElement: Unknown type " + type);
			}
		} catch (XPathExpressionException e) {
			return new Item_ParseException("Ilias_Item.parseItemElement: " + e.getMessage());
		}
	}	
	
	
	
	static void parseItemElement (Element xmlNode, Item item) throws XPathExpressionException {

		XPath xpath = XPathFactory.newInstance().newXPath();

		// parse question, description, and annotation from mattext
		String descques = (String) xpath.evaluate("./presentation/flow/material/mattext", xmlNode, XPathConstants.STRING); 
		parseHtmlQuestion (descques, item);
		

		/* for debug 
		Document document = xmlNode.getOwnerDocument();
		DOMImplementationLS domImplLS = (DOMImplementationLS) document
			.getImplementation();
		LSSerializer serializer = domImplLS.createLSSerializer();
		String str = serializer.writeToString(xmlNode);
		*/

		// title
		String s = (String) xpath.evaluate("./@title", xmlNode, XPathConstants.STRING);
		item.setTitle(s);
	}

	/**
	 * Parse HTML question
	 * - may contain JSON data within an HTML comment 
	 * - may be separated into (vignette) description and actual question 
	 * @param htmlQuestion
	 * @param item
	 */
	public static void parseHtmlQuestion (String htmlQuestion, Item item) {
	
		int separatorPosition = 0;
		String[] separatedValues = new String[] { "", ""};
		
		for (Node c: Jsoup.parseBodyFragment(htmlQuestion).body().childNodes()) {
			
			if (c instanceof Comment) {
				String s = ((Comment)c).getData();
				
				if ((s!=null) && (s.length()>ANNOTATION_PREFIX.length()) && (s.startsWith(ANNOTATION_PREFIX))) {
					String jsonString  = s.substring(ANNOTATION_PREFIX.length());
					JSONObject json = new JSONObject(jsonString);
					Json_Item.parse(json, item);
				}
				
				if ((s!=null) && (s.equalsIgnoreCase(DESCRIPTION_QUESTION_SEPARATOR))) {
					separatorPosition = 1;
				}
				continue;
			}
			separatedValues[separatorPosition] += c.outerHtml();
		}
		
		item.setDescription((separatorPosition==1) ? separatedValues[0] : "");
		item.setQuestion((separatorPosition==1) ? separatedValues[1] : separatedValues[0]);
	}
	
	
	public static Element createItemElement(Item item, Document doc, String xmlIdent, IntFunction<String> imgReference, IntFunction<String> imgLabel) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {

		if (item instanceof Item_SC) return Ilias_Item_SC.createItemElement((Item_SC) item, doc, xmlIdent, imgReference, imgLabel);
		if (item instanceof Item_MC) return Ilias_Item_MC.createItemElement((Item_MC) item, doc, xmlIdent, imgReference, imgLabel);
		if (item instanceof Item_FT) return Ilias_Item_FT.createItemElement((Item_FT) item, doc, xmlIdent, imgReference, imgLabel);
		if (item instanceof Item_OR) return Ilias_Item_OR.createItemElement((Item_OR) item, doc, xmlIdent, imgReference, imgLabel);
		if (item instanceof Item_MA) return Ilias_Item_MA.createItemElement((Item_MA) item, doc, xmlIdent, imgReference, imgLabel);
		return null;
	}

	public static Element createItemElement(Item item, String type, Document doc, String xmlIdent, IntFunction<String> imgReference, IntFunction<String> imgLabel) throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {
		return createItemElement(item, type, doc, xmlIdent, new HashMap<String, String>(), imgReference, imgLabel);
	}
	
	public static Element createItemElement(Item item, String type, Document doc, String xmlIdent, Map<String, String> addMetaData, IntFunction<String> imgReference, IntFunction<String> imgLabel) throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {

		Element result = doc.createElement("item");
		result.setAttribute("ident", xmlIdent);
		result.setAttribute("title", item.getTitle());
		result.setAttribute("maxattempts", "1");

		if (item.getEalid() != null) {
			Element c = doc.createElement("qticomment");
			c.setTextContent("[EALID:" + item.getEalid() + "]");
			result.appendChild(c);
		}

		Element d = doc.createElement("duration");
		d.setTextContent("P0Y0M0DT0H1M0S");
		result.appendChild(d);

		Element meta = doc.createElement("qtimetadata");
		Map<String, String> metaData = new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;
			{
				put("ILIAS_VERSION", "5.4.22 2021-05-14"); //  "5.0.8 2015-11-24"
				put("QUESTIONTYPE", type);
				put("AUTHOR", "author");
				put("additional_cont_edit_mode", "default");
//				put("externalId", xmlIdent);
//				if (item.getEalid() != null) {
//					put("ealid", String.valueOf(item.getEalid()));
//				}
				put("thumb_size", "");
				put("feedback_setting", "1");
			}
		};

		for (Entry<String, String> e: addMetaData.entrySet()) {
			if (e.getValue()!=null) {
				metaData.put(e.getKey(), e.getValue());
			} else {
				metaData.remove(e.getKey());
			}
		}

		metaData.forEach((key, value) -> {
			Element l = doc.createElement("fieldlabel");
			l.setTextContent(key);
			Element e = doc.createElement("fieldentry");
			e.setTextContent(value);

			Element x = doc.createElement("qtimetadatafield");
			x.appendChild(l);
			x.appendChild(e);
			meta.appendChild(x);
		});

		Element itemmeta = doc.createElement("itemmetadata");
		itemmeta.appendChild(meta);
		result.appendChild(itemmeta);


		Element material = doc.createElement("material");
		material.appendChild(createMatTextElement(doc, 
				item.getImages().convertImagesToLabel(item.getDescription(), imgLabel),
				item.getImages().convertImagesToLabel(item.getQuestion(), imgLabel),
				Json_Item.toJSON_AnnotationOnly(item)));
		
		/* TODO Image processing ... passt? */
		for (int index=0; index<item.getImages().getNumberOfImages(); index++) {
			Element matimage = doc.createElement("matimage");
			matimage.setAttribute("label", imgLabel.apply(index));
			matimage.setAttribute("uri", imgReference.apply(index));
			material.appendChild(matimage);
		}
		
		
		Element flow = doc.createElement("flow");
		flow.appendChild(material);
		
		Element presentation = doc.createElement("presentation");
		presentation.setAttribute("label", item.getTitle());
		presentation.appendChild(flow);
		
		result.appendChild(presentation);
		return result;
	}


	/**
	 * Creates HTML content that contains EAL annotation data (encoded as HTML comment in JSON format), description, and question
	 * Images are  
	 * @param doc
	 * @param description
	 * @param question
	 * @param annotation
	 * @param imgOffset
	 * @return
	 */
	private static Element createMatTextElement(Document doc, String description, String question, JSONObject annotation) {

		Element mattext = doc.createElement("mattext");
		mattext.setTextContent(createHtmlQuestion (description, question, annotation));
		mattext.setAttribute("texttype", "text/html");
		return mattext;

	}
	

	public static String createHtmlQuestion(String description, String question, JSONObject annotation) {
		
		org.jsoup.nodes.Document html = Jsoup.parseBodyFragment("");
		html.body().appendChild(new Comment(ANNOTATION_PREFIX + annotation.toString()));
		html.body().append(Optional.ofNullable(description).orElse(""));
		html.body().appendChild(new Comment(DESCRIPTION_QUESTION_SEPARATOR));
		html.body().append(Optional.ofNullable(question).orElse(""));
		
		html.outputSettings().syntax(org.jsoup.nodes.Document.OutputSettings.Syntax.xml);    
		html.outputSettings().escapeMode(EscapeMode.xhtml);
		return html.body().html();
	}

	


}
