package eal.item.format.ilias;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eal.item.type.Item_MA;


public class Ilias_Item_MA  {
	
	public final static String type = "MATCHING QUESTION";
	
	
	public static Item_MA parseItemElement (Element xmlNode) throws XPathExpressionException {
		
		Item_MA item = new Item_MA();
		Ilias_Item.parseItemElement(xmlNode, item);
		
		XPath xpath = XPathFactory.newInstance().newXPath();
		
		Map<String, String> definitions = new HashMap<>();
		Map<String, String> terms = new HashMap<>();
		
		NodeList xmlLabels = (NodeList) xpath.evaluate("./presentation/flow//response_label", xmlNode, XPathConstants.NODESET);
		for (int index = 0; index < xmlLabels.getLength(); index++) {
			Node xmlLabel = xmlLabels.item(index);
			String ident = (String) xpath.evaluate("@ident", xmlLabel, XPathConstants.STRING);
			String matchGroup = (String) xpath.evaluate("@match_group", xmlLabel, XPathConstants.STRING);
			String text = (String) xpath.evaluate("./material/mattext/text()", xmlLabel, XPathConstants.STRING);
			if (matchGroup.equals("")) {
				terms.put(ident, text);
				item.addTerm (text);
			} else {
				definitions.put(ident, text);
				item.addDefinition(text);
			}
		}
		
		
		NodeList xmlResps = (NodeList) xpath.evaluate("./resprocessing/respcondition", xmlNode, XPathConstants.NODESET);
		for (int index = 0; index < xmlResps.getLength(); index++) {
			Node xmlResp = xmlResps.item(index);
			String points = (String) xpath.evaluate("./setvar[@action=\"Add\"]/text()", xmlResp, XPathConstants.STRING);
			String[] mapping = ((String) xpath.evaluate("./conditionvar/varsubset/text()", xmlResp, XPathConstants.STRING)).split(",");
			item.addMatch(terms.get(mapping[0]), definitions.get(mapping[1]), points);
		}
		
		return item;
	}
	
	
	public static Element createItemElement(Item_MA item, Document doc, String xmlIdent, IntFunction<String> imgReference, IntFunction<String> imgLabel)	throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {
		
		Element result = Ilias_Item.createItemElement(item, type, doc, xmlIdent, imgReference, imgLabel);
		
		XPath xpath = XPathFactory.newInstance().newXPath();
		
		
		Node flow = (Node) xpath.evaluate("./presentation/flow", result, XPathConstants.NODE);
		flow.appendChild(createResponseElement(doc, xmlIdent, item, "Multiple"));
		
		
		result.appendChild(Ilias_Item_MA.createResprocessingElement(doc, "OQT", item, false));
		
		
		
		/* ident values for odering questions (OQ)
		* OQP: OQ_PICTURES
		* OQNP: OQ_NESTED_PICTURES
		* OQT: OQ_TERMS
		* OQNT: OQ_NESTED_TERMS
		*/
		
		return result;
		
	}
	
	public static Element createResponseElement(Document doc, String xmlIdent, Item_MA item, String rcardinality) {
		
		Element render_choice = doc.createElement("render_choice");
		render_choice.setAttribute("shuffle", "Yes");
		
		
		String match_group = IntStream
		.range(item.getDefinitions().size(), item.getDefinitions().size()+item.getTerms().size())
		.mapToObj(index -> String.valueOf(index))
		.collect(Collectors.joining(","));
		
		int index = 0;
		for (String def: item.getDefinitions()) {
			
			Element mattext = doc.createElement("mattext");
			mattext.setTextContent(def);
			mattext.setAttribute("texttype", "text/plain");
			
			Element material = doc.createElement("material");
			material.appendChild(mattext);
			
			Element response_label = doc.createElement("response_label");
			response_label.setAttribute("ident", String.valueOf(index));
			response_label.setAttribute("match_group", match_group);
			response_label.setAttribute("match_max", "1");
			response_label.appendChild(material);
			
			render_choice.appendChild (response_label);
			
			index++;
		}
		
		for (String def: item.getTerms()) {
			
			Element mattext = doc.createElement("mattext");
			mattext.setTextContent(def);
			mattext.setAttribute("texttype", "text/plain");
			
			Element material = doc.createElement("material");
			material.appendChild(mattext);
			
			Element response_label = doc.createElement("response_label");
			response_label.setAttribute("ident", String.valueOf(index));
			response_label.appendChild(material);
			
			render_choice.appendChild (response_label);
			
			index++;
		}
		
		
		Element response_grp = doc.createElement("response_grp");
		response_grp.setAttribute("ident", "MQ");
		response_grp.setAttribute("rcardinality", "Multiple");
		response_grp.appendChild (render_choice);
		return response_grp;		
		
		
	}
	
	
	public static Element createResprocessingElement (Document doc, String xmlIdent, Item_MA item, boolean includeNOT) {
		
		Element resprocessing = doc.createElement("resprocessing");
		
		Element outcomes = doc.createElement("outcomes");
		Element decvar = doc.createElement("decvar");
		outcomes.appendChild (decvar);
		resprocessing.appendChild(outcomes);
		
		
		
		for (int index=0; index<item.getNumberOfMatches(); index++) {
			
			
			Element respcondition = doc.createElement("respcondition");
			respcondition.setAttribute ("continue", "Yes");
			
			Element conditionvar = doc.createElement("conditionvar");
			Element varsubset = doc.createElement("varsubset");
			varsubset.setTextContent((item.getMatchTermId(index) + item.getDefinitions().size()) + "," + item.getMatchDefinitionId(index));
			varsubset.setAttribute ("respident", "MQ");
			conditionvar.appendChild (varsubset);
			respcondition.appendChild (conditionvar);
			
			Element setvar = doc.createElement("setvar");
			// setvar.setTextContent( String.valueOf (itemWAnsers.getAnswerPoints(index, checked)));
			setvar.setTextContent( String.format("%.2f", item.getMatchPoints(index)));
			setvar.setAttribute("action", "Add");
			
			respcondition.appendChild (setvar);
			resprocessing.appendChild (respcondition);
			
			
		}
		
		return resprocessing;	
	}
	
}
