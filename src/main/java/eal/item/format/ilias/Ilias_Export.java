package eal.item.format.ilias;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.IntFunction;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import eal.item.format.Exporter;
import eal.item.type.Item;

public class Ilias_Export extends Exporter {

	private String name;
	
	private IntFunction<IntFunction<String>> IMAGES_FILENAME = imgOffset -> imgIndex -> String.format("objects/il_0_mob_%d/ealimg_%d", imgOffset+imgIndex, imgIndex);
	private IntFunction<IntFunction<String>> IMAGES_LABEL = imgOffset -> imgIndex -> "il_0_mob_" + (imgOffset+imgIndex);

	
	public Ilias_Export() {
		this.name = System.currentTimeMillis() + "__0__qpl_1";
	}
	
	@Override
	public String getFileName () {
		return this.name;
	}

	@Override
	public int create(Item[] items, OutputStream out) throws Exception {

		int noOfCreatedItems = 0;

		String qplName = name + ".xml";
		String qtiName = name.replace("_qpl_", "_qti_") + ".xml";
		
		ZipOutputStream zip = new ZipOutputStream(out);
		
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document qtiDoc = builder.newDocument();
		qtiDoc.appendChild(qtiDoc.createElement("questestinterop"));
		
		int imgOffset = 0; 
		for (int itemIndex=0; itemIndex<items.length; itemIndex++) {
			
			Element element = Ilias_Item.createItemElement(items[itemIndex], qtiDoc, this.getQuestionIdent(noOfCreatedItems), IMAGES_FILENAME.apply(imgOffset), IMAGES_LABEL.apply(imgOffset));
			if (element == null) continue;
			
			qtiDoc.getDocumentElement().appendChild(element);
			for (int imgIndex=0; imgIndex<items[itemIndex].getImages().getNumberOfImages(); imgIndex++) {
				byte[] c = items[itemIndex].getImages().getImage(imgIndex);
				if (c != null) {
					addFileToZip(zip, name + "/" + IMAGES_FILENAME.apply(imgOffset).apply(imgIndex), new ByteArrayInputStream(c));
				}
			}
			imgOffset += items[itemIndex].getImages().getNumberOfImages();
			noOfCreatedItems++;
		}
		
		addFileToZip (zip, name + "/" + qplName, getStreamFromDocument(createQPLFile(noOfCreatedItems)));
		addFileToZip (zip, name + "/" + qtiName, getStreamFromDocument(qtiDoc));
		zip.close();
		
		return noOfCreatedItems;	// number of exported items
	}

	
	
	private String getQuestionIdent (int index) {
		return "il_0_qst_" + index;
	}
	
	private Document createQPLFile(int noOfItems) throws ParserConfigurationException, SAXException, IOException {

		String xml = String.format("<ContentObject Type=\"Questionpool_Test\"><MetaData><General Structure=\"Hierarchical\"><Identifier Catalog=\"EAL\" Entry=\"%s\" /><Title Language=\"de\">Exported from EALService at %s</Title><Language Language=\"de\" /><Description Language=\"de\" /><Keyword Language=\"en\" /></General></MetaData></ContentObject>",
				this.name,
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())
			);
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document qplDoc = builder.parse(new InputSource(new StringReader(xml)));
		
		for (int index = 0; index < noOfItems; index++) {
			Element xml_PO = qplDoc.createElement("PageObject");
			Element xml_PC = qplDoc.createElement("PageContent");
			Element xml_QU = qplDoc.createElement("Question");
			xml_QU.setAttribute("QRef", this.getQuestionIdent(index));
			xml_PC.appendChild(xml_QU);
			xml_PO.appendChild(xml_PC);
			qplDoc.getDocumentElement().appendChild(xml_PO);
		}

		return qplDoc;
	}


	
	
}
