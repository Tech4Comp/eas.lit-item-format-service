package eal.item.format.ilias;

import java.io.IOException;
import java.io.StringReader;
import java.util.function.IntFunction;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import eal.item.type.Item_FT;

public class Ilias_Item_FT  {

	public final static String type = "TEXT QUESTION";


	public static Item_FT parseItemElement (Element xmlNode) throws XPathExpressionException {

		Item_FT item = new Item_FT();
		Ilias_Item.parseItemElement(xmlNode, item);

		XPath xpath = XPathFactory.newInstance().newXPath();

		item.setPoints((String) xpath.evaluate("./resprocessing/outcomes/decvar/@maxvalue", xmlNode, XPathConstants.STRING));
		return item;
	}



	
	public static Element createItemElement(Item_FT item, Document doc, String xmlIdent, IntFunction<String> imgReference, IntFunction<String> imgLabel)	throws ParserConfigurationException, XPathExpressionException, SAXException, IOException {

		Element result = Ilias_Item.createItemElement(item, Ilias_Item_FT.type, doc, xmlIdent, imgReference, imgLabel);

		XPath xpath = XPathFactory.newInstance().newXPath();
		
		String xml = "<response_str ident=\"TEXT\" rcardinality=\"Ordered\"><render_fib fibtype=\"String\" prompt=\"Box\"><response_label ident=\"A\"/></render_fib></response_str>";
		Element response_str = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(xml))).getDocumentElement();
		Element flow = (Element) xpath.evaluate("./presentation/flow", result, XPathConstants.NODE);
		flow.appendChild(doc.importNode(response_str, true));
		
		
		
		xml = String.format("<resprocessing scoremodel=\"HumanRater\"><outcomes><decvar varname=\"WritingScore\" vartype=\"Integer\" minvalue=\"0\" maxvalue=\"%s\"/></outcomes><respcondition><conditionvar><other>tutor_rated</other></conditionvar></respcondition></resprocessing>",
				item.getPoints());
		Element resprocessing = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(xml))).getDocumentElement();
		
		
		result.appendChild(doc.importNode (resprocessing, true));
		
		return result;
		
	}

}
