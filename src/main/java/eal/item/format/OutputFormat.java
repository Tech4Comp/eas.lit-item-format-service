package eal.item.format;

import eal.item.format.csv.CSV_Export;
import eal.item.format.csv.CSV_Export_Zip;
import eal.item.format.csv.json.CSVJson_Export;
import eal.item.format.csv.xlsx.XLSX_Export;
import eal.item.format.csv.xlsx.XLSX_Export_Zip;
import eal.item.format.ilias.Ilias_Export;
import eal.item.format.json.Json_Export;
import eal.item.format.json.Json_Export_Zip;
import eal.item.format.moodle.Moodle_Export;
import eal.item.format.onyx.Onyx_Export;

public enum OutputFormat { csv, csv_easlit, csv_zip, xlsx_all, xlsx_type, xlsx_all_easlit, xlsx_type_easlit, xlsx_all_zip, xlsx_type_zip, json, json_easlit, json_zip, csvjson_all, csvjson_type, csvjson_all_easlit, csvjson_type_easlit, ilias, onyx, moodle;
	
	public Exporter getExporter() {
		switch (this) {
		case csv: 					return new CSV_Export(false);
		case csv_easlit:			return new CSV_Export(true);
		case csv_zip:				return new CSV_Export_Zip();
		case xlsx_all:				return new XLSX_Export(false, false);
		case xlsx_type:				return new XLSX_Export(true, false);
		case xlsx_all_easlit:		return new XLSX_Export(false, true);
		case xlsx_type_easlit:		return new XLSX_Export(true, true);
		case xlsx_all_zip:			return new XLSX_Export_Zip(false);
		case xlsx_type_zip:			return new XLSX_Export_Zip(true);
		case json:					return new Json_Export(false);
		case json_easlit:			return new Json_Export(true);
		case json_zip:				return new Json_Export_Zip();
		case csvjson_all:			return new CSVJson_Export(false, false);
		case csvjson_type:			return new CSVJson_Export(true, false);
		case csvjson_all_easlit:	return new CSVJson_Export(false, true);
		case csvjson_type_easlit:	return new CSVJson_Export(true, true);
		case ilias:					return new Ilias_Export();
		case onyx:					return new Onyx_Export();
		case moodle:				return new Moodle_Export();
		default:					return null;
		}
	}
	
	public String getContentType() {
		return this.getContentTypeAndDisposition()[0];
	}

	public String getContentDisposition() {
		return this.getContentTypeAndDisposition()[1];
	}
	
	private String[] getContentTypeAndDisposition() {
		switch (this) {
		case csv: 					
		case csv_easlit:			
			return new String[]{"text/csv", "csv" };
		
		case csv_zip:
		case xlsx_all_zip:	
		case xlsx_type_zip:
		case json_zip:
		case ilias:	
		case onyx:
			return new String[]{"application/zip", "zip"};
			
		case xlsx_all:				
		case xlsx_type:				
		case xlsx_all_easlit:		
		case xlsx_type_easlit:		
			return new String[]{"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "xlsx"};
			
		case json:					
		case json_easlit:
			return new String[]{"application/json", "json"};
			
		case csvjson_all:			
		case csvjson_type:			
		case csvjson_all_easlit:	
		case csvjson_type_easlit:		
			return new String[]{"application/json", null};	// no filename/download here
		
		case moodle: 
			return new String[]{"application/xml", "xml"};

		default:					
			return new String[]{null, null};
		}			
	}
	
}