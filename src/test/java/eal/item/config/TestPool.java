package eal.item.config;

import eal.item.format.InputFormat;

public enum TestPool {

    // SC-Fragen von ap.easlit.de
    AP1 ("wordpress ap exports/wp_eal_item_sc.csv", InputFormat.csv),

    // MC-Fragen von ap.easlit.de
	AP2 ("wordpress ap exports/wp_eal_item_mc.csv", InputFormat.csv),
    
    RANG ("rangfolge und co/items-rangfolge-zuordnung-freitext.zip", InputFormat.ilias),
    
    // hier auch unbekannte Item Typen
	DBS_UEB ("dbs ueb ilias/dbs 01_intro bis 08_dk.zip", InputFormat.ilias),
    
    DBS_WIS1 ("dbs wissen ilias/1558036519__0__qpl_97813.zip", InputFormat.ilias),
	DBS_WIS2 ("dbs wissen ilias/1558036511__0__qpl_97809.zip", InputFormat.ilias),
    DBS_WIS3 ("dbs wissen ilias/1558036512__0__qpl_97810.zip", InputFormat.ilias),
    
    // hier fehlerhafte MCs: falsche Antwortoptionen haben [0,0] bei points; Grund: IliasLM-Generator macht das falsch
	ILIAS_LM1 ("von IliasLM generiert/1555150900__0__qpl_123882.zip", InputFormat.ilias),
    
    // korrigiert; falsche haben [pos=-1; neg=0]
	ILIAS_LM2 ("von IliasLM generiert/1558035482__0__qpl_124453.zip", InputFormat.ilias),
    
    JSON_EXP ("jsonexport/2019-06-13-15-21-42_easlit.json", InputFormat.json),
    
    GEN_300 ("itempoolgenerator/300_mc_und_sc.json", InputFormat.json),
    
    // each item is a zip with manifest + xml
    ONYX_1 ("onyxopal/export-2018-06-27_10-53-27.zip", InputFormat.onyx),
	// one manifest with multiple item xmls
    ONYX_2 ("onyxopal/Test.zip", InputFormat.onyx), 
    ONYX_3 ("onyxopal/Neue_Aufgabe_Freitext.zip", InputFormat.onyx),
	ONYX_4 ("onyxopal/Neue_Aufgabe_Einfache_Zuordnung_Drag-and-Drop.zip", InputFormat.onyx),
    ONYX_5 ("onyxopal/drei MC mit unterschiedlichen Bewertungssystemen.zip", InputFormat.onyx),
    
    EXCEL ("demo.xlsx", InputFormat.xlsx);
            

    private final static String DATA_DIR = "data/";
    private String fileName;
    private InputFormat format;

    TestPool (String fileName, InputFormat format) {
        this.fileName = fileName;
        this.format = format;
    }

    public String getFileName() {
		return DATA_DIR + this.fileName;
	}

	public InputFormat getInputFormat() {
		return this.format;
	}

}