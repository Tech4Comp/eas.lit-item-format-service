package eal.item.config;

import java.util.HashSet;
import java.util.function.BiConsumer;

import eal.item.type.Item;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import eal.item.format.InputFormat;
import eal.item.format.OutputFormat;

public enum TestExportFormat {

    CSV(OutputFormat.csv, "_orig.csv", true, InputFormat.csv),
    CSV_EASLIT(OutputFormat.csv_easlit, "_easlit.csv", true, InputFormat.csv),
    CSV_ZIP(OutputFormat.csv_zip, ".csv.zip", true, InputFormat.csv_zip),
    XLSX_ALL(OutputFormat.xlsx_all, "_all_orig.xlsx", true, InputFormat.xlsx),
    XLSX_TYPE(OutputFormat.xlsx_type, "_type_orig.xlsx", false, InputFormat.xlsx),
    XLSX_ALL_EASLIT(OutputFormat.xlsx_all_easlit, "_all_easlit.xlsx", true, InputFormat.xlsx),
    XLSX_TYPE_EASLIT(OutputFormat.xlsx_type_easlit, "_type_easlit.xlsx", false, InputFormat.xlsx),
    XLSX_ALL_ZIP(OutputFormat.xlsx_all_zip, "_all.xlsx.zip", true, InputFormat.xlsx_zip),
    XLSX_TYPE_ZIP(OutputFormat.xlsx_type_zip, "_type.xlsx.zip", false, InputFormat.xlsx_zip),
    JSON(OutputFormat.json, "_orig.json", true, InputFormat.json),
    JSON_EASLIT(OutputFormat.json_easlit, "_easlit.json", true, InputFormat.json),
    JSON_ZIP(OutputFormat.json_zip, ".json.zip", true, InputFormat.json_zip),
    CSVJSON_ALL(OutputFormat.csvjson_all, "_all_orig.csv.json", true, InputFormat.csvjson),
    CSVJOSN_TYPE(OutputFormat.csvjson_type, "_type_orig.csv.json", false, InputFormat.csvjson),
    CSVJSON_ALL_EASLIT(OutputFormat.csvjson_all_easlit, "_all_easlit.csv.json", true, InputFormat.csvjson),
    CSVJSON_TYPE_EASLIT(OutputFormat.csvjson_type_easlit, "_type_easlit.csv.json", false, InputFormat.csvjson),
    ILIAS(OutputFormat.ilias, "_ilias.zip", true, InputFormat.ilias);

    private OutputFormat out;
    private String suffix;
    // CSV/Excel Outputformats "by type" do not remain item order --> we have two assertion functions for item pools
    private boolean isOrdered;
    private InputFormat in;

    TestExportFormat(OutputFormat out, String suffix, boolean isOrdered, InputFormat in) {
        this.out = out;
        this.suffix = suffix;
        this.isOrdered = isOrdered;
        this.in = in;
    }

    public OutputFormat getOutputFormat() {
        return this.out;
    }

    public String getFileSuffix() {
        return this.suffix;
    }

    public BiConsumer<Item[], Item[]> getAssertion() {
        return this.isOrdered 
            ? (items1, items2) -> assertArrayEquals(items1, items2)
            : (items1, items2) -> assertEquals(new HashSet<Item>(Arrays.asList(items1)), new HashSet<Item>(Arrays.asList(items2)));
    }

    public InputFormat getInputFormat() {
        return this.in;
    }

}