package eal.item;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import eal.item.format.InputFormat;
import eal.item.format.OutputFormat;
import eal.item.type.Item;

/**
 * This is the "local equivalent" of the Service Controller that provides createItems and parseItems methods 
 * @author Andreas
 *
 */

public class TestController {

	
	


	/*
	 * see Controller.parseItems
	 */
	public static Item[] parseItemsFromFile (String fileName, InputFormat format)  {
		
		try {
			FileInputStream in = new FileInputStream(new File(fileName));
			Item[] res = format.getImporter().parse(in);
			in.close();
			return res;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	
	/*
	 * see Controller.createItems
	 */
	public static String createItemsToFile (Item[] items, OutputFormat format, String name) {
		
		try {
			String fileName = TestConfig.TEMP_DIR + new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date()) + name;
			File outFile = new File(fileName);
			outFile.createNewFile(); // if file already exists will do nothing
			FileOutputStream out = new FileOutputStream(outFile, false);
			format.getExporter().create(items, out);
			out.close();
			return fileName;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
