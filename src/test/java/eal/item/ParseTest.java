package eal.item;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import eal.item.format.InputFormat;
import eal.item.format.OutputFormat;
import eal.item.store.ItemStore;
import eal.item.store.ItemWithError;
import eal.item.type.Item;
import eal.item.type.Item_MC;

public class ParseTest {

	public static String tmpDir = "tmp/";

	// @Test
	public static void main(String[] args) throws Exception {
		

//		Controller.loadItems("1450");
//		importItems("data/imsglobal/test_package_minfiles.zip", new Onyx_Import());


		checkLoadItems(new String[]{"1", "100"});

		new Scanner(System.in).nextLine();
		
		Item[] items = loadItemsFromFiles();
		
		checkExportFormats(items);

		
		
		ItemWithError[] storedItems = ItemStore.saveItems(items, true, null);
		
		

		
		
		System.out.println("#items="+items.length);
		Item[] input = Arrays.stream(items)
			.filter(item -> /*(item instanceof Item_SC) ||*/ (item instanceof Item_MC))
//			.filter(item -> item.getEalid()==396)
			.map(item -> (Item_MC) item)
			.filter(item -> {
				
				for (int i=0; i<item.getNumberOfAnswers(); i++) {
					if ((item.getAnswerPoints(i, true)==0) && (item.getAnswerPoints(i, false)==0)) return true;
				}
				
				return false;
			})
			.toArray(Item[]::new);
		System.out.println("#items="+input.length);

		
//		checkItemStorage(input, true);
//		checkItemStorage(input, false);

	}

	
	public static void checkLoadItems (String[] idRange) {
		
		
		ItemWithError[] items = ItemStore.loadItemsById(idRange);
		
		
		long noOfLoadedItems = Arrays.stream(items).filter(Objects::nonNull).count();
		
		System.out.println(String.format("Loaded %d of %d items", noOfLoadedItems, items.length));
		
	}
	
	
	public static void checkItemStorage(Item[] items, boolean createNew) throws Exception {
		/*
		ItemWithError[] storedItems = ItemStore.saveItems(items, createNew);
		
		assertEquals(items.length, storedItems.length);

		for (int i=0; i<items.length; i++) {
			if (storedItems[i] != null) {
				items[i].setEalid(storedItems[i].getEalid());
				
//				System.out.println(i);
				
				assertEquals(items[i], storedItems[i]);
			} else {
				System.out.println(String.format("Item #%d could not be saved.", items[i].getEalid()));
			}
			
		}
		*/
	}

	
	



	public static void checkExportFormats(Item[] items) throws Exception {

		System.out.println("CheckExportFormats with " + items.length + " items");

		System.out.println("Test CSV ...");
		assertArrayEquals(items, parseItems (createItems(items, OutputFormat.csv, "_orig.csv"), InputFormat.csv));
		
		System.out.println("Test CSV EASLIT ...");
		assertArrayEquals(items, parseItems (createItems(items, OutputFormat.csv_easlit, "_easlit.csv"), InputFormat.csv));
		
		System.out.println("Test CSV ZIP ...");
		assertArrayEquals(items, parseItems (createItems(items, OutputFormat.csv_zip, ".csv.zip"), InputFormat.csv_zip));
		
		System.out.println("Test Excel overall ...");
		assertArrayEquals(items, parseItems (createItems(items, OutputFormat.xlsx_all, "_all_orig.xlsx"), InputFormat.xlsx));

		// since "by type" may change the item order --> the assertion ignores the order by using sets
		System.out.println("Test Excel by Type ...");
		assertSetEquals(items, parseItems (createItems(items, OutputFormat.xlsx_type, "_type_orig.xlsx"), InputFormat.xlsx));
		
		System.out.println("Test Excel EASLIT overall ...");
		assertArrayEquals(items, parseItems (createItems(items, OutputFormat.xlsx_all_easlit, "_all_easlit.xlsx"), InputFormat.xlsx));

		System.out.println("Test Excel EASLIT by Type ...");
		assertSetEquals(items, parseItems (createItems(items, OutputFormat.xlsx_type_easlit, "_type_easlit.xlsx"), InputFormat.xlsx));
		
		System.out.println("Test Excel ZIP overall ...");
		assertArrayEquals(items, parseItems (createItems(items, OutputFormat.xlsx_all_zip, "_all.xlsx.zip"), InputFormat.xlsx_zip));

		System.out.println("Test Excel ZIP by Type ...");
		assertSetEquals(items, parseItems (createItems(items, OutputFormat.xlsx_type_zip, "_type.xlsx.zip"), InputFormat.xlsx_zip));

		System.out.println("Test Json ...");
		assertArrayEquals(items, parseItems (createItems(items, OutputFormat.json, "_orig.json"), InputFormat.json));

		System.out.println("Test Json EASLIT ...");
		assertArrayEquals(items, parseItems (createItems(items, OutputFormat.json_easlit, "_easlit.json"), InputFormat.json));

		System.out.println("Test Json ZIP ...");
		assertArrayEquals(items, parseItems (createItems(items, OutputFormat.json_zip, ".json.zip"), InputFormat.json_zip));

		System.out.println("Test CSVJson overall ...");
		assertArrayEquals(items, parseItems (createItems(items, OutputFormat.csvjson_all, "_all_orig.csv.json"), InputFormat.csvjson));

		System.out.println("Test CSVJson by Type ...");
		assertSetEquals(items, parseItems (createItems(items, OutputFormat.csvjson_type, "_type_orig.csv.json"), InputFormat.csvjson));

		System.out.println("Test CSVJson EASLIT overall ...");
		assertArrayEquals(items, parseItems (createItems(items, OutputFormat.csvjson_all_easlit, "_all_easlit.csv.json"),InputFormat.csvjson));

		System.out.println("Test CSVJson EASLIT by Type ...");
		assertSetEquals(items, parseItems (createItems(items, OutputFormat.csvjson_type_easlit, "_type_easlit.csv.json"), InputFormat.csvjson));

		System.out.println("Test Ilias ...");
		assertArrayEquals(items, parseItems (createItems(items, OutputFormat.ilias, "_ilias.zip"), InputFormat.ilias));


	}

	private static Item[] loadItemsFromFiles () throws FileNotFoundException, Exception {
		
		System.out.print("Loading items from file ...");
		
		List<Item> items = new ArrayList<Item>();
		items.addAll(Arrays.asList(parseItems("data/wordpress ap exports/wp_eal_item_sc.csv", InputFormat.csv)));
		
		items.addAll(Arrays.asList(parseItems("data/wordpress ap exports/wp_eal_item_mc.csv", InputFormat.csv)));
		items.addAll(Arrays.asList(parseItems("data/rangfolge und co/items-rangfolge-zuordnung-freitext.zip", InputFormat.ilias)));
		items.addAll(Arrays.asList(parseItems("data/dbs ueb ilias/dbs 01_intro bis 08_dk.zip", InputFormat.ilias)));

		items.addAll(Arrays.asList(parseItems("data/dbs wissen ilias/1558036519__0__qpl_97813.zip", InputFormat.ilias)));
		items.addAll(Arrays.asList(parseItems("data/dbs wissen ilias/1558036511__0__qpl_97809.zip", InputFormat.ilias)));
		items.addAll(Arrays.asList(parseItems("data/dbs wissen ilias/1558036512__0__qpl_97810.zip", InputFormat.ilias)));
		
		// hier fehlerhafte MCs: falsche Antwortoptionen haben [0,0] bei points; Grund: IliasLM-Generator macht das falsch
		items.addAll(Arrays.asList(parseItems("data/von IliasLM generiert/1555150900__0__qpl_123882.zip", InputFormat.ilias)));
		
		// korrigiert; falsche haben [pos=-1; neg=0]
		items.addAll(Arrays.asList(parseItems("data/von IliasLM generiert/1558035482__0__qpl_124453.zip", InputFormat.ilias)));
		
		System.out.println(items.size() + " items loaded.");
		return items.toArray(new Item[items.size()]);
	}

	
	
	public static String createItems (Item[] items, OutputFormat format, String name) throws Exception {
		
		String fileName = tmpDir + new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date()) + name;
		File outFile = new File(fileName);
		outFile.createNewFile(); // if file already exists will do nothing
		FileOutputStream out = new FileOutputStream(outFile, false);
		format.getExporter().create(items, out);
		out.close();
		return fileName;
		
	}
	
	private static Item[] parseItems (String fileName, InputFormat format) throws FileNotFoundException, Exception {
		FileInputStream in = new FileInputStream(new File(fileName));
		Item[] res = format.getImporter().parse(in);
		in.close();
		return res;
	}
	
	
	private static void assertSetEquals(Item[] items1, Item[] items2) {
		assertEquals(new HashSet<Item>(Arrays.asList(items1)), new HashSet<Item>(Arrays.asList(items2)));
	}
	
}
