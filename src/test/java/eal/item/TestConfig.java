package eal.item;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashSet;
import java.util.function.BiConsumer;

import eal.item.format.InputFormat;
import eal.item.format.OutputFormat;
import eal.item.type.Item;

public class TestConfig {

	public final static String TEMP_DIR = "tmp/";
	private final static String DATA_DIR = "data/";

	// all available test files from different LMS / systems
	private final static Object[][] ALL_TEST_ITEM_POOLS = new Object[][] {
			// SC-Fragen von ap.easlit.de
			{ "wordpress ap exports/wp_eal_item_sc.csv", InputFormat.csv },
			// MC-Fragen von ap.easlit.de
			{ "wordpress ap exports/wp_eal_item_mc.csv", InputFormat.csv },
			{ "rangfolge und co/items-rangfolge-zuordnung-freitext.zip", InputFormat.ilias },
			// hier auch unbekannte Item Typen
			{ "dbs ueb ilias/dbs 01_intro bis 08_dk.zip", InputFormat.ilias },
			{ "dbs wissen ilias/1558036519__0__qpl_97813.zip", InputFormat.ilias },
			{ "dbs wissen ilias/1558036511__0__qpl_97809.zip", InputFormat.ilias },
			{ "dbs wissen ilias/1558036512__0__qpl_97810.zip", InputFormat.ilias },
			// hier fehlerhafte MCs: falsche Antwortoptionen haben [0,0] bei points; Grund:
			// IliasLM-Generator macht das falsch
			{ "von IliasLM generiert/1555150900__0__qpl_123882.zip", InputFormat.ilias },
			// korrigiert; falsche haben [pos=-1; neg=0]
			{ "von IliasLM generiert/1558035482__0__qpl_124453.zip", InputFormat.ilias },
			{ "jsonexport/2019-06-13-15-21-42_easlit.json", InputFormat.json },
			{ "itempoolgenerator/300_mc_und_sc.json", InputFormat.json },
			// each item is a zip with manifest + xml
			{ "onyxopal/export-2018-06-27_10-53-27.zip", InputFormat.onyx },
			// one manifest with multiple item xmls
			{ "onyxopal/Test.zip", InputFormat.onyx }, { "onyxopal/Neue_Aufgabe_Freitext.zip", InputFormat.onyx },
			{ "onyxopal/Neue_Aufgabe_Einfache_Zuordnung_Drag-and-Drop.zip", InputFormat.onyx },
			{ "onyxopal/drei MC mit unterschiedlichen Bewertungssystemen.zip", InputFormat.onyx },
			{ "demo.xlsx", InputFormat.xlsx }

	};

	public static int getNumberOfTestItemPools() {
		return ALL_TEST_ITEM_POOLS.length;
	}

	public static String getItemPoolFileName(int poolIdx) {
		return DATA_DIR + (String) ALL_TEST_ITEM_POOLS[poolIdx][0];
	}

	public static InputFormat getItemPoolInputFormat(int poolIdx) {
		return (InputFormat) ALL_TEST_ITEM_POOLS[poolIdx][1];
	}

	// CSV/Excel Outputformats "by type" do not remain item order 
	// --> we have two assertion functions for item pools
	private static BiConsumer<Item[], Item[]> assertItemEqualsSet = (items1, items2) -> 
		assertEquals(new HashSet<Item>(Arrays.asList(items1)), new HashSet<Item>(Arrays.asList(items2))); 
	
	private static BiConsumer<Item[], Item[]> assertItemEqualsArray = (items1, items2) -> assertArrayEquals(items1,
			items2);

	// OutputFormat, File-Suffix, assertion, InputFormat
	private final static Object[][] ALL_EXPORT_FORMATS = new Object[][] {
			{ OutputFormat.csv, "_orig.csv", assertItemEqualsArray, InputFormat.csv },
			{ OutputFormat.csv_easlit, "_easlit.csv", assertItemEqualsArray, InputFormat.csv },
			{ OutputFormat.csv_zip, ".csv.zip", assertItemEqualsArray, InputFormat.csv_zip },
			{ OutputFormat.xlsx_all, "_all_orig.xlsx", assertItemEqualsArray, InputFormat.xlsx },
			{ OutputFormat.xlsx_type, "_type_orig.xlsx", assertItemEqualsSet, InputFormat.xlsx },
			{ OutputFormat.xlsx_all_easlit, "_all_easlit.xlsx", assertItemEqualsArray, InputFormat.xlsx },
			{ OutputFormat.xlsx_type_easlit, "_type_easlit.xlsx", assertItemEqualsSet, InputFormat.xlsx },
			{ OutputFormat.xlsx_all_zip, "_all.xlsx.zip", assertItemEqualsArray, InputFormat.xlsx_zip },
			{ OutputFormat.xlsx_type_zip, "_type.xlsx.zip", assertItemEqualsSet, InputFormat.xlsx_zip },
			{ OutputFormat.json, "_orig.json", assertItemEqualsArray, InputFormat.json },
			{ OutputFormat.json_easlit, "_easlit.json", assertItemEqualsArray, InputFormat.json },
			{ OutputFormat.json_zip, ".json.zip", assertItemEqualsArray, InputFormat.json_zip },
			{ OutputFormat.csvjson_all, "_all_orig.csv.json", assertItemEqualsArray, InputFormat.csvjson },
			{ OutputFormat.csvjson_type, "_type_orig.csv.json", assertItemEqualsSet, InputFormat.csvjson },
			{ OutputFormat.csvjson_all_easlit, "_all_easlit.csv.json", assertItemEqualsArray, InputFormat.csvjson },
			{ OutputFormat.csvjson_type_easlit, "_type_easlit.csv.json", assertItemEqualsSet, InputFormat.csvjson },
			{ OutputFormat.ilias, "_ilias.zip", assertItemEqualsArray, InputFormat.ilias } };

	public static int getNumberOfExportFormats() {
		return ALL_EXPORT_FORMATS.length;
	}

	public static OutputFormat getExportOutputFormat(int exportIdx) {
		return (OutputFormat) ALL_EXPORT_FORMATS[exportIdx][0];
	}

	public static String getExportFileSuffix(int exportIdx) {
		return (String) ALL_EXPORT_FORMATS[exportIdx][1];
	}

	@SuppressWarnings("unchecked")
	public static BiConsumer<Item[], Item[]> getExportAssertion(int exportIdx) {
		return (BiConsumer<Item[], Item[]>) ALL_EXPORT_FORMATS[exportIdx][2];
	}

	public static InputFormat getExportInputFormat(int exportIdx) {
		return (InputFormat) ALL_EXPORT_FORMATS[exportIdx][3];
	}

}
