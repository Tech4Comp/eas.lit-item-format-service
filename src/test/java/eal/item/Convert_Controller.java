package eal.item;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Controller.class })
@WebAppConfiguration
public class Convert_Controller {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;
    
    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
    


	@Test
	public void testConvertItems_POST() throws Exception {
		
		for (int poolIdx = 0; poolIdx<TestConfig.getNumberOfTestItemPools(); poolIdx++) {

			for (int exportIdx = 0; exportIdx < TestConfig.getNumberOfExportFormats(); exportIdx++) {
			
		        MockMultipartFile file = new MockMultipartFile("file", null, null, Files.readAllBytes(new File (TestConfig.getItemPoolFileName(poolIdx)).toPath()));
		        
				ResultActions ra = mockMvc.perform(MockMvcRequestBuilders.multipart("/convert")
						.file(file)
						.param("inputFormat", TestConfig.getItemPoolInputFormat(poolIdx).toString())
						.param("outputFormat", TestConfig.getExportOutputFormat(exportIdx).toString())
						);
				
				String fileNameOut = TestConfig.TEMP_DIR + new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date()) + TestConfig.getExportFileSuffix(exportIdx);
				File outFile = new File(fileNameOut);
				outFile.createNewFile(); // if file already exists will do nothing
				FileOutputStream out = new FileOutputStream(outFile, false);
				out.write(ra.andReturn().getResponse().getContentAsByteArray());
				out.close();

				System.out.println(String.format("input file %s to %s", TestConfig.getItemPoolFileName(poolIdx), TestConfig.getExportOutputFormat(exportIdx).toString()));
				System.out.println(ra.andReturn().getResponse().getStatus());
				System.out.println(ra.andReturn().getResponse().getErrorMessage());
			}
			
		}
		
	}
	
	
	@Test
	public void testImportItems_POST() throws Exception {
		
		for (int poolIdx = 0; poolIdx<TestConfig.getNumberOfTestItemPools(); poolIdx++) {

	        MockMultipartFile file = new MockMultipartFile("file", null, null, Files.readAllBytes(new File (TestConfig.getItemPoolFileName(poolIdx)).toPath()));
	        
			ResultActions ra = mockMvc.perform(MockMvcRequestBuilders.multipart("/import")
					.file(file)
					.param("inputFormat", TestConfig.getItemPoolInputFormat(poolIdx).toString())
					);
			
			System.out.println(String.format("input file %s", TestConfig.getItemPoolFileName(poolIdx)));
			System.out.println(ra.andReturn().getResponse().getStatus());
			System.out.println(ra.andReturn().getResponse().getErrorMessage());
			System.out.println(ra.andReturn().getResponse().getContentAsString());
			
		}
		
	}	
}
