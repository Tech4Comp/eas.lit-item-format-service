package eal.item;

import java.io.FileNotFoundException;
import java.util.Arrays;

import org.junit.Test;

import eal.item.config.TestExportFormat;
import eal.item.config.TestPool;
import eal.item.format.InputFormat;
import eal.item.type.Item;
import eal.item.type.Item_ParseException;



public class ConvertTest {


	
	/**
	 * This test checks for the equivalence of all input and output formats 
	 * @throws FileNotFoundException
	 * @throws Exception
	 */
	@Test
	public void testConvertEquivalence() throws FileNotFoundException, Exception {
		
		System.out.println("testConvertEquivalence");

		Arrays.stream (TestPool.values()).filter(p -> p.getInputFormat()!=InputFormat.onyx).toArray(TestPool[]::new);


		// for (TestPool p: TestPool.values()) {

			// if (p.getInputFormat()==InputFormat.onyx) continue;

			TestPool p = TestPool.AP2;
			Item[] loadedItems = loadTestItems(/*TestPool.values()*/ p);	
			System.out.println(String.format ("Loaded %d items.", loadedItems.length));
			System.out.println(p.name());
			
			Item[] items = Arrays.stream(loadedItems).filter(item -> !(item instanceof Item_ParseException)).toArray(Item[]::new);
			System.out.println(String.format ("Checking formats with %d items.", items.length));
			
			Item[] itemsShort = Arrays.copyOfRange(items, 2, 3);

			exportAndTest (itemsShort, /*TestExportFormat.values()*/ TestExportFormat.ILIAS );
		// }


	}
	

	

	// @Test
	// public void testImportItems() throws FileNotFoundException, Exception {
		
	// 	System.out.println("testImportItems");
		
	// 	Item[] items = loadTestItems(IntStream.range(3, 4));
	// 	ItemWithError[] savedItems = ItemStore.saveItems(items, true);

	// }

	
	public void exportAndTest (Item[] items, TestExportFormat... formats)  {
		for (TestExportFormat format: formats) {
			System.out.println(String.format("Test %s", format.getOutputFormat().toString()));
			format.getAssertion().accept(items, 
				TestController.parseItemsFromFile (
					TestController.createItemsToFile(items, format.getOutputFormat(), format.getFileSuffix()), 
					format.getInputFormat()));

		}

	}

	public Item[] loadTestItems(TestPool... pools) {
		System.out.println("Loading items ...");
		return Arrays.stream(pools)
			.map(pool ->  TestController.parseItemsFromFile(pool.getFileName(), pool.getInputFormat()))
			.flatMap(poolItems -> Arrays.stream(poolItems))
			.toArray(Item[]::new);
	}


	

	
	
}
