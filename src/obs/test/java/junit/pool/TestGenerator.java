package junit.pool;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.Test;

import eal.item.pool.Criterion;
import eal.item.pool.Generator;
import eal.item.type.Item;
import eal.item.type.Item_MC;
import eal.item.type.Item_SC;
import junit.Convert;

public class TestGenerator {

	
	@Test
	public void testItemPools () {
		
		Item[] items = Convert.loadTestItems(IntStream.range(0,  2));
		
		
		List<Criterion> c = new ArrayList<Criterion>();
		c.add(new Criterion( 8, 8, it -> true));
		c.add(new Criterion( 3,  8, it -> it instanceof Item_SC));
		c.add(new Criterion( 2,  3, it -> it instanceof Item_MC));
		
		Generator g = new Generator(items, c.stream().toArray(Criterion[]::new));

		System.out.println(g.getNumberOfItemPools());
		
		g.generate(72);

	}
}
