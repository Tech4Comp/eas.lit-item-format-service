package junit.pool;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import eal.item.Controller;
import junit.TestConfig;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Controller.class })
@WebAppConfiguration
public class TestGenerator_Controller {

    private MockMvc mockMvc;

    private final String criteria = "{\"all\":[1,10]}";
    
    @Autowired
    private WebApplicationContext wac;
    
    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
    


	@Test
	public void testItemPool() throws Exception {
	
		/* upload items */
		MockHttpServletRequestBuilder builder =	MockMvcRequestBuilders
			.post("/pool/items")
        	.characterEncoding("UTF-8")
            .content(new String(Files.readAllBytes(Paths.get(TestConfig.getItemPoolFileName(10)))));
		
		String token = mockMvc.perform(builder).andReturn().getResponse().getContentAsString();
		System.out.println(token);
		
		
		builder = MockMvcRequestBuilders
			.put("/pool/count")
	    	.characterEncoding("UTF-8")
	    	.param("token", token)
	        .content(criteria);
		
		
		String count = mockMvc.perform(builder).andReturn().getResponse().getContentAsString();
		System.out.println("COUNT = " + count);
	}
}
