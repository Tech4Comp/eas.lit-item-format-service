package eal.item.pool;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import eal.item.type.Item;

public class Generator {

	
	private Criterion[] criteria;
	private ItemGroupVector[] itemGroups;
	

	
	
	public Generator(Item[] items, Criterion[] criteria) {

		this.criteria = criteria;
		
		itemGroups =
				Arrays.stream(items)
					.collect(Collectors.groupingBy(item -> new ItemVector(item, this.criteria)))
					.entrySet().stream().map(e -> new ItemGroupVector(e.getKey().getVector(), e.getValue(), this.criteria))
					.toArray(ItemGroupVector[]::new);
			
		for (ItemGroupVector ig: itemGroups) {
			ig.computeMin(itemGroups);
		}
		
		
		System.out.println("Generator");
		System.out.println(Arrays.toString(criteria));
		System.out.println(Arrays.toString(itemGroups));
	}
	
	
	
	public long getNumberOfItemPools () {
		
		long res = 0;
		
		ItemGroupPoolIterator iterator = new ItemGroupPoolIterator(itemGroups, criteria);
		
		while (iterator.hasNext()) {
			
			ItemGroupPool igp = iterator.next();
			
			if (igp.getSize() == -1) {
				return -1;
			}
			
			try {
				res = Math.addExact(res, igp.getSize());
			} catch (ArithmeticException e) {
				return -1;
			}
			
		}
		
		return res;
		
	}

	public Item[][] generate(int noOfItemPools) {

		Item[][] res = new Item[noOfItemPools][];
		
		long n = getNumberOfItemPools();
		long maxNoOfItemPools = (n == -1) ? Long.MAX_VALUE : n;
		
		long[] poolIndexs = IntStream.range(0,  noOfItemPools)
			.mapToLong(idx -> (long) (Math.random()*maxNoOfItemPools))
			.sorted()
			.toArray();
		
		
		
		ItemGroupPoolIterator iterator = new ItemGroupPoolIterator(itemGroups, criteria);
		
		
		
		long cuSumOfItemPools = 0; 
		int idx = 0;
		
		while (iterator.hasNext()) {
			
			ItemGroupPool igp = iterator.next();
			
			if (igp.getSize() == -1) {
				cuSumOfItemPools = Long.MAX_VALUE;
			} else {
				try {
					cuSumOfItemPools = Math.addExact(cuSumOfItemPools, igp.getSize());
				} catch (ArithmeticException e) {
					cuSumOfItemPools = Long.MAX_VALUE;
				}
			}
			
			while ((idx<noOfItemPools) && (poolIndexs[idx]<cuSumOfItemPools)) {
				
				res[idx] = igp.getPool();
				System.out.println(Arrays.toString(Arrays.stream(res[idx]).map(Item::getEalid).toArray(String[]::new)));
				
				
				idx++;
			}
			
			if (idx == noOfItemPools) break;
			
		}		
		
		
		return res;
		
//		ItemGroupPoolIterator g = new ItemGroupPoolIterator(itemGroups, criteria);
//		
//		Spliterator<ItemGroupPool> gsplit = Spliterators.spliteratorUnknownSize(g, 0);
//		
//		StreamSupport.stream(gsplit, false)
//			.forEach(x -> System.out.println(x));
		
		
//		
//		
//		// [No_of_items_of_itemGroup0,No_of_items_of_itemGroup1, ...]
//		int[] minGroupPool = Arrays.stream(itemGroups).mapToInt(ig -> ig.getMin()).toArray();
//		int[] maxGroupPool = Arrays.stream(itemGroups).mapToInt(ig -> ig.getMax()).toArray();
//		
//		int[] currentGroupPool = Arrays.copyOf(minGroupPool, minGroupPool.length);
//		int pos = 0;
//		do {
//			
//			System.out.println(Arrays.toString(currentGroupPool));
//
//			// we can increment the current position --> we are done
//			if (currentGroupPool[pos]<maxGroupPool[pos]) {
//				currentGroupPool[pos]++;
//				continue;
//			}
//			
//			// we look for the next position that can be incremented
//			int i;
//			for (i=pos+1; i<=currentGroupPool.length; i++) {
//				if (currentGroupPool[i]<maxGroupPool[i]) {
//					currentGroupPool[i]++;	// increment this and ...
//					for (int k=0; k<i; k++) {
//						currentGroupPool[k] = minGroupPool[k];	// .. set all previous positions two min
//					}
//					pos = 0;
//					break;
//				}
//			}
//			
//			if (i==currentGroupPool.length) {
//				break;
//			}
//			
//		} while (true);
//		
//		
//		
//		int[] currentGroupVector = new int[criteria.length];
		
		
		
		
		
		
		
	}
	
	
//	private boolean testGroupPool (int[] gp) {
//		
//		for (int indexCriteria=0; indexCriteria<this.criteria.length; indexCriteria++) {
//			int v = 0;
//			for (int indexGroup=0; indexGroup<this.itemGroups.length; indexGroup++) {
////				v += gp[indexGroup] * this.criteria[indexCriteria].get
//				
//				
//				
//			}
//			
//		}
//		
//		
//		
//		
//	}
	
}
