package eal.item.pool;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import eal.item.format.json.Json_Item;
import eal.item.format.json.Json_Item_FT;
import eal.item.format.json.Json_Item_MC;
import eal.item.format.json.Json_Item_OR;
import eal.item.format.json.Json_Item_SC;
import eal.item.type.Item;
import eal.item.type.Item_FT;
import eal.item.type.Item_MC;
import eal.item.type.Item_OR;
import eal.item.type.Item_SC;

public class CriteriaParser {

	
	
	public static Criterion[] parse(InputStream in) throws JSONException, IOException    {
		
		List<Criterion> res = new ArrayList<Criterion>();
		
		JSONObject json = new JSONObject(IOUtils.toString(in, "UTF-8"));
		
		System.out.println(json.toString());
		
		JSONArray all = json.optJSONArray("all");
		if (all != null) {
			res.add(new Criterion(all.getInt(0), all.getInt(1), item -> true)); 
		}
		
		JSONObject type = json.optJSONObject(Json_Item.TYPE);
		if (type != null) {
			for (String key: type.keySet()) {
				
				Predicate<Item> pred = null;
				switch (key) {
				case Json_Item_SC.TYPE: pred = item -> item instanceof Item_SC; break;
				case Json_Item_MC.TYPE: pred = item -> item instanceof Item_MC; break;
				case Json_Item_FT.TYPE:	pred = item -> item instanceof Item_FT; break;
				case Json_Item_OR.TYPE:	pred = item -> item instanceof Item_OR; break;
				default: continue;
				}
				
				JSONArray minmax = type.getJSONArray(key);
				res.add(new Criterion(minmax.getInt(0), minmax.getInt(1), pred)); 
			}
		}
		
		
		return res.stream().toArray(Criterion[]::new);
		
	}
	
}
