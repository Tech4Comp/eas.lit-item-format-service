package eal.item.pool;

import java.util.Arrays;
import java.util.Iterator;

public class ItemGroupPoolIterator implements Iterator<ItemGroupPool> {

	private final ItemGroupVector[] itemGroups;
	private final Criterion[] criteria;
	
	
	private int[] groupPool;  
	private int[] criteriaVector;
	
	private boolean isFirst;
	private boolean toCompute;
	
	int pos;
	
	public ItemGroupPoolIterator(ItemGroupVector[] itemGroups, Criterion[] criteria) {

		this.itemGroups = itemGroups;
		this.criteria = criteria;
			
		isFirst = true;
		toCompute = true;
		pos = 0;
	}

	
	@Override
	public boolean hasNext() {
		
		computeNext();
		return !(groupPool == null);
	}

	@Override
	public ItemGroupPool next() {
		computeNext();
		toCompute = true;
		return new ItemGroupPool(groupPool, criteriaVector, itemGroups);
	}

	
	private void computeNext() {
		
		// make computeNext idempotent
		if (!toCompute) {
			return;
		}
		computeNextGroupPool();
	}
	
	private void computeNextGroupPool() {

		toCompute = false;

		// we already reached the end of the iterator
		if ((!isFirst) && (groupPool == null)) {
			return;
		}

		// we compute the first elememt
		if (isFirst) {
			isFirst = false;
			initGroupPool();
			if (!checkIfGroupPoolIsValid()) computeNextGroupPool();
			return;
		}
		

		
		// we can increment the current position --> we are done
		if (groupPool[pos]<itemGroups[pos].getMax()) {
			updateGroupPool (pos, groupPool[pos]+1);
			if (!checkIfGroupPoolIsValid()) computeNextGroupPool();
			return;
		}
		
		// we look for the next position that can be incremented
		int i;
		for (i=pos+1; i<groupPool.length; i++) {
			if (groupPool[i]<itemGroups[i].getMax()) {
				updateGroupPool (i, groupPool[i]+1);	// increment this and ...
				for (int k=0; k<i; k++) {
					updateGroupPool (k, this.itemGroups[k].getMin());	// .. set all previous positions two min
				}
				pos = 0;
				if (!checkIfGroupPoolIsValid()) computeNextGroupPool();
				return;
			}
		}
		
		// we did not find a new element of the iterator
		groupPool = null;
	}
	
	
	private boolean checkIfGroupPoolIsValid () {
		
		for (int criteriaIndex = 0; criteriaIndex < criteriaVector.length; criteriaIndex++) {
			if (criteriaVector[criteriaIndex] < this.criteria[criteriaIndex].getMin()) return false;
			if (criteriaVector[criteriaIndex] > this.criteria[criteriaIndex].getMax()) return false;
		}
		return true;
	}
	
	private void updateGroupPool (int itemGroupIndex, int newValue) {
		
		for (int criteriaIndex = 0; criteriaIndex < criteriaVector.length; criteriaIndex++) {
			criteriaVector[criteriaIndex] += (newValue-groupPool[itemGroupIndex]) * itemGroups[itemGroupIndex].getVector()[criteriaIndex];
		}
		groupPool[itemGroupIndex] = newValue;
	}
	
	
	private void initGroupPool () {
		
		groupPool = Arrays.stream(itemGroups).mapToInt(ItemGroupVector::getMin).toArray();
		criteriaVector = new int[criteria.length];
		
		for (int itemGroupIndex = 0; itemGroupIndex < itemGroups.length; itemGroupIndex++) {
			for (int criteriaIndex = 0; criteriaIndex < criteriaVector.length; criteriaIndex++) {
				criteriaVector [criteriaIndex] += groupPool[itemGroupIndex] * itemGroups[itemGroupIndex].getVector()[criteriaIndex];
			}
		}
	}
	



}
