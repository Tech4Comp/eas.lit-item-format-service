package eal.item.pool;

import java.util.Arrays;

import eal.item.type.Item;

/**
 * Represents what criteria are fulfilled by the item
 * vector[i] == 1 if criterion[i] is fulfilled; 0 otherwise
 * @author Andreas
 *
 */

public class ItemVector {


	private final int[] vector;
	
	public ItemVector(Item item, Criterion[] c) {
		this.vector = Arrays.stream(c).mapToInt(cr -> cr.test(item) ? 1 : 0).toArray();
	}
	
	
	public int[] getVector() {
		return vector;
	}

	@Override
	public boolean equals(Object obj) {
		return Arrays.equals(this.vector, ((ItemVector)obj).vector);
	}
	
	@Override
	public int hashCode() {
		return 0;
	}
}
