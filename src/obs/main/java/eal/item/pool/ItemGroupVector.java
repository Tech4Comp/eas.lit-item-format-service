package eal.item.pool;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import eal.item.type.Item;

/**
 * Represents a list of items that all share the same ItemVector, 
 * i.e., the all fulfill the same criteria
 * @author Andreas
 *
 */
public class ItemGroupVector {

	private final List<Item> items;

	// vector[idx]==1 iff criteria[idx].test()==true
	private final int[] vector;
	private final Criterion[] criteria;
	
	// min/max number of items of this ItemGroup must/can be in a valid solution
	private int min;		
	private final int max;	
	
	
	public ItemGroupVector(int[] vector, List<Item> items, Criterion[] criteria) {
		super();
		
		this.vector = vector;
		this.items = items;
		this.criteria = criteria;
		
		this.min = 0;
		this.max = IntStream.range(0, criteria.length)
			.filter(idx -> vector[idx]>0)
			.map(idx -> criteria[idx].max)
			.min()
			.orElse(items.size());
	}
	
	


	public void computeMin(ItemGroupVector[] itemGroups) {
		
		IntStream.range(0, criteria.length).forEach(index -> { 
		
			if (this.getVector()[index]==0) return;
			
			int sumOtherMax = Arrays.stream(itemGroups)
				.filter(otherIG -> otherIG != this)
				.filter(otherIG -> otherIG.getVector()[index] == 1)
				.mapToInt(otherIG -> otherIG.getMax())
				.sum();
			
			if (sumOtherMax<this.criteria[index].getMin()) {
				this.min = Math.max(this.min, this.criteria[index].getMin()-sumOtherMax);
			}			
				
		});
	}

	
	public Item getItem (int index) {
		return this.items.get(index);
	}
	
	public int getSize() {
		return this.items.size();
	}
	
	public int getMax( ) {
		return this.max;
	}
	
	public int getMin() {
		return this.min;
	}
	
	public int[] getVector() {
		return this.vector;
	}
	
	@Override
	public String toString() {
		return String.format("vector=%s, min=%d, max=%d", Arrays.toString(this.vector), this.min, this.max);
	}
	
}
