package eal.item.pool;

import java.util.function.Predicate;

import eal.item.type.Item;

/**
 * Represents a criterion for an item pool
 * between (inclusive) min and max items in each pool must match the predicate 
 * @author Andreas
 */

public class Criterion {

	
	final int min;
	final int max;
	final Predicate<Item> predicate;
	
	
	public Criterion(int min, int max, Predicate<Item> predicate) {
		super();
		this.min = min;
		this.max = max;
		this.predicate = predicate;
	} 
	
	public boolean test (Item item) {
		return this.predicate.test(item);
	}

	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}
	
	
	@Override
	public String toString() {
		return String.format("Criterion: min=%d, max=%d, predicate=%s", min, max, predicate.toString());
	}
	
	
}
