package eal.item.pool;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

import org.apache.commons.math3.exception.MathArithmeticException;
import org.apache.commons.math3.util.CombinatoricsUtils;

import eal.item.type.Item;


/**
 * An ItemGroupPool is a grouped pool of items that fullfills all criteria
 * for each ItemGroupVector it says how many items are in the pool
 * groupPool[x]=y --> take y items from itemGroups[x] 
 * @author Andreas
 *
 */

public class ItemGroupPool {

	
	private final int[] groupPool;
	private final int[] criteriaVector;
	private final ItemGroupVector[] itemGroups;
	
	private final long size;
	
	public ItemGroupPool(int[] groupPool, int[] criteriaVector, ItemGroupVector[] itemGroups) {
		super();
		this.groupPool = groupPool;
		this.criteriaVector = criteriaVector;
		this.itemGroups = itemGroups;
		this.size = computeSize();
	}
	
	
	public Item[] getPool() {
		
		return IntStream.range(0, groupPool.length).boxed()
			.flatMap(groupIdx ->  
				// pick n=groupPool[groupIdx] items from itemGroup at random 
				new Random().ints(0, itemGroups[groupIdx].getSize())
					.distinct()
					.limit(groupPool[groupIdx])
					.mapToObj(itemIdx -> itemGroups[groupIdx].getItem(itemIdx)))
			.toArray(Item[]::new);
	}
	
	
	public long getSize() {
		return size;
	}
	
	/**
	 * Computes the number of all possible item pools of this group
	 * returns -1 if the number is greater than LONG_MAX_VALUE
	 * @param groupPool
	 * @param sizeGroupPool
	 * @return
	 */
	private long computeSize () {
		
		int[] sizeGroupPool = Arrays.stream(itemGroups).mapToInt(ItemGroupVector::getSize).toArray();
		long size = 1;
		long binCo;
		
		for (int i=0; i<groupPool.length; i++) {
		
			try {
				binCo = CombinatoricsUtils.binomialCoefficient(sizeGroupPool[i], groupPool[i]);
			} catch (MathArithmeticException e) {
				return -1;
			}
			
			try {
				size = Math.multiplyExact(size, binCo);
			} catch (ArithmeticException e) {
				return -1;
			}
		}
		
		return size;
	}
	
	
	
	
	@Override
	public String toString() {
		return Arrays.toString(groupPool) + " => " + size + "\t\t" + Arrays.toString(this.criteriaVector);
	}


	

	
	
}
