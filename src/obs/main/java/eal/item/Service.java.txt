
	public static void setPoolItems(InputStream in, ServletContext context, HttpServletResponse response) throws IOException {
		
		Item[] parsedItems = parseItems(InputFormat.json, in, response);
		if (parsedItems == null) return;
		
		Item[] items = Arrays.stream(parsedItems)
				.filter(item -> !(item instanceof Item_ParseException))
				.filter(item -> item.getEalid() != null)
				.toArray(Item[]::new);
		if (items.length < parsedItems.length) {
			response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, String.format("%d out of %d items could not be processed", parsedItems.length-items.length, parsedItems.length));
			return;
		}		
		
		
		String token = String.valueOf(System.currentTimeMillis());
		context.setAttribute(token, items);
		
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Content-Type", "application/json");
		response.getWriter().write(new JSONObject().put("token", token).toString());
		response.getWriter().flush();
	}


	public static void getPoolCount(String token, InputStream in, ServletContext context, HttpServletResponse response) throws IOException {
	
		Object cachedItems = context.getAttribute(token);
		if ((cachedItems == null) || (!(cachedItems instanceof Item[]))) {
			response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, String.format("Invalid token: %s", token));
			return;
		}
		
		Generator gen = new Generator((Item[]) cachedItems, CriteriaParser.parse(in));
		
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Content-Type", "text");
		response.getWriter().write(String.valueOf(gen.getNumberOfItemPools()));
		response.getWriter().flush();
	}
	
	
	public static void getPools(String token, int numberOfPools, InputStream in, ServletContext context, HttpServletResponse response) throws Exception {
	
		Object cachedItems = context.getAttribute(token);
		if ((cachedItems == null) || (!(cachedItems instanceof Item[]))) {
			response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, String.format("Invalid token: %s", token));
			return;
		}
		
		Generator gen = new Generator((Item[]) cachedItems, CriteriaParser.parse(in));
		Item[][] res = gen.generate(numberOfPools);
		
		
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Content-Type", OutputFormat.json.getContentType());

		String a = Arrays.stream(res)
				.map(pool -> {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					try {
						OutputFormat.json.getExporter().create(pool, baos);
					} catch (Exception e) {
						return "{}";
					}
					return baos.toString();	})
				.collect(Collectors.joining(","));
			
		
		response.getWriter().write(String.format("{\"pools\":[%s]}", a));
		response.getWriter().flush();
	}	
	