
/* old stuff: pool creation */ 

/*	

@PostMapping(value = "/pool/items", consumes={"application/json"}, produces={"application/json"})
@ResponseBody
@ApiOperation(value = "Sets the items for item pool generator.")
public void setPoolItems_POST(
		@ApiParam(value = "JSON that contains the items")
		@RequestBody(required=true) String items, 
		
		HttpServletRequest request,
		HttpServletResponse response) throws IOException, Exception {

	Service.setPoolItems (new ByteArrayInputStream(items.getBytes()), request.getServletContext(), response);		
	
}	



@PutMapping(value = "/pool/count", consumes={"application/json"}, produces={"application/json"} )
@ResponseBody
@ApiOperation(value = "Get the number of item pools")
public void poolCount_PUT(
		
		@ApiParam(required = true, value = "Token that refers to the item set", example = "1563883530197")
		@RequestParam("token") 
		String token,
		
		@ApiParam(value = "Criteria that specify list of valid pools")
		@RequestBody(required=true)  String criteria, 
		
		HttpServletRequest request,
		HttpServletResponse response) throws IOException, Exception {

		
	Service.getPoolCount (token, new ByteArrayInputStream(criteria.getBytes()), request.getServletContext(), response);		
	
}	


@PutMapping(value = "/pool/pools", consumes={"application/json"}, produces={"application/json"} )
@ResponseBody
@ApiOperation(value = "Get random item pools")
public void pools_PUT(
		
		@ApiParam(required = true, value = "Token that refers to the item set", example = "1563883530197")
		@RequestParam("token") 
		String token,
		
		@ApiParam(required = true, value = "Number of pools", example = "10")
		@RequestParam("numberOfPools") 
		int numberOfPools,
		
		@ApiParam(value = "Criteria that specify list of valid pools")
		@RequestBody(required=true)  String criteria, 
		
		HttpServletRequest request,
		HttpServletResponse response) throws IOException, Exception {

		
	Service.getPools (token, numberOfPools, new ByteArrayInputStream(criteria.getBytes()), request.getServletContext(), response);		
	
}


*/