# FROM gradle:jdk11
FROM gradle:6.9.0-jdk11

# RUN git clone https://gitlab.com/Tech4Comp/eas.lit-item-format-service.git

COPY ./ /home/gradle/eas.lit-item-format-service/
USER root
RUN chown -R gradle:gradle ./
USER gradle


WORKDIR /home/gradle/eas.lit-item-format-service/

# wen Performance-Problem: compile in image; start+download dependencies
# RUN gradle bootJar
# RUN java -jar build/libs/eas.lit-item-format-service-0.1.0.jar

# RUN ["gradle", "bootJar"]

CMD ["gradle", "bootRun", "--info"]
